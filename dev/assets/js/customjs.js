/* Navbar scroll */
jQuery(function($) {

    $(window).on('load', function() {
        if ($(this).scrollTop() >= 50) {
            $('.navbar').addClass('pinned');
        } else if ($(this).scrollTop() <= 75) {
            $('.navbar').removeClass('pinned');
        }
    });
    $(window).on('scroll', function() {
        if ($(this).scrollTop() >= 50) {
            $('.navbar').addClass('pinned');
        } else if ($(this).scrollTop() <= 75) {
            $('.navbar').removeClass('pinned');
        }
    });

	toggleMenu();

	function adjustNav() {
		var winWidth = $(window).width(),
			dropdown = $('.dropdown'),
			dropdownMenu = $('.dropdown-menu');
		
		if (winWidth >= 768) {
			dropdown.on('mouseenter', function() {
				$(this).addClass('show')
					.children(dropdownMenu).addClass('show');
			});
			
			dropdown.on('mouseleave', function() {
				$(this).removeClass('show')
					.children(dropdownMenu).removeClass('show');
			});
		} else {
			dropdown.off('mouseenter mouseleave');
		}
	}
	
	$(window).on('resize', adjustNav);

	adjustNav();
});


function toggleMenu() {
    $('.navbar-toggler').unbind('click').bind('click', function() {

       $($(this).data('target')).toggleClass('slide-in');
       $('.navbar-toggler').toggleClass('collapsed');
    });
}