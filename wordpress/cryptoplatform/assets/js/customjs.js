/* Navbar scroll */
jQuery(function($) {

    tickerBar();
	liskSupportAds();
    toggleMenu();

    if($('#floating_social_bar').length > 0) {
        SSKvisibility();
        $(window).on('resize', SSKvisibility);
    }

    adjustNav();
    $(window).on('resize', adjustNav);


    $('.search-toggler').on('click', toggleSearch);

    $(function() {
        if ($(window).scrollTop() >= 50) {
            $('.navbar').addClass('pinned');
        } else if ($(window).scrollTop() <= 75) {
            $('.navbar').removeClass('pinned');
        }
    });

    $(window).on('scroll', function() {
        if ($(this).scrollTop() >= 50) {
            $('.navbar').addClass('pinned');
        } else if ($(this).scrollTop() <= 75) {
            $('.navbar').removeClass('pinned');
        }
    });
});


function toggleMenu() {
    jQuery('.navbar-toggler').unbind('click').bind('click', function() {

        jQuery(jQuery(this).data('target')).toggleClass('slide-in');
        jQuery('.navbar-toggler').toggleClass('collapsed');
    });
}


function SSKvisibility() {

    var start = $('.ssk-group').offset().top - $('.navbar.navbar-dark').innerHeight(),
        stop = $('.page-footer').offset().top,
        element = $('#floating_social_bar');

    $(window).scroll(function () {

        var windowTop = $(window).scrollTop(),
            windowBottom = $(window).scrollTop() + $(window).innerHeight();

        if(windowTop > start) {
            element.addClass('show-ssk');
        } else {
            element.removeClass('show-ssk');
        }
    });
}


function adjustNav() {
    var winWidth = $(window).width(),
        dropdown = $('.dropdown'),
        dropdownMenu = $('.dropdown-menu');

    if (winWidth >= 768) {
        dropdown.on('mouseenter', function() {
            $(this).addClass('show')
                .children(dropdownMenu).addClass('show');
        });

        dropdown.on('mouseleave', function() {
            $(this).removeClass('show')
                .children(dropdownMenu).removeClass('show');
        });
    } else {
        dropdown.off('mouseenter mouseleave');
    }
}

function toggleSearch() {
    var target = $($(this).data('target'));

    target.toggleClass('show-search');

    $(this).find('.close-search').toggleClass('d-none');
    $(this).find('.open-search').toggleClass('d-none');

}

function tickerBar() {

    var keys = ['name', 'symbol', 'price_usd', 'price_btc', 'market_cap_usd', 'percent_change_24h'],
        container = '.ticker-bar',
        identifier = '.ticker-bar-item',
        storedData = JSON.parse( localStorage.getItem( 'tickerBarData' ));

    if(typeof(Storage) !== "undefined" && storedData !== null) {
        updateTickerValues(keys, storedData);
    }

    (function retrieveData() {

        $.ajax({
            url: "//api.coinmarketcap.com/v1/ticker/lisk/?convert=USD",
            method: 'GET',
            success: function(event) {

                var response = event[0];

                if (typeof(Storage) !== "undefined") {
                    localStorage.setItem('tickerBarData', JSON.stringify(response));
                }

                updateTickerValues(keys, response);

            },
            complete: setTimeout(function() {retrieveData()}, 10000),
            timeout: 2000
        });
    })();

    function updateTickerValues(keys, data) {
        $.each(keys, function(key, value) {
            var rule = $(container).find(identifier + '[data-data-key=' + value + '] .ticker-bar-value').data('rule'),
                content = data[value];

            if(rule === 'dollars') {
                content = parseFloat(content, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,")
            }

            $(container).find(identifier + '[data-data-key=' + value + '] .ticker-bar-value').html(content);
        });
    }
}

function kbScroll() {
jQuery(window).scroll(function() {
    var scroll = jQuery(window).scrollTop();

    if (scroll >= 0) {
        jQuery("#kb-quickstart").addClass("active");
	}
});
}



$('document').ready(function () {


		// RESTYLE THE DROPDOWN MENU
    $('#google_translate_element').on("click", function () {

        // Change font family and color
        $("iframe").contents().find(".goog-te-menu2-item div, .goog-te-menu2-item:link div, .goog-te-menu2-item:visited div, .goog-te-menu2-item:active div, .goog-te-menu2 *")
            .css({
                'color': '#544F4B',
                'font-family': '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"',
								'width':'100%'
            });
        // Change menu's padding
        $("iframe").contents().find('.goog-te-menu2-item-selected').css ('display', 'none');

				// Change menu's padding
        $("iframe").contents().find('.goog-te-menu2').css ('padding', '0px');

        // Change the padding of the languages
        $("iframe").contents().find('.goog-te-menu2-item div').css(
			'padding', '20px'
		);

        // Change the width of the languages
        $("iframe").contents().find('.goog-te-menu2-item').css('width', '100%');
        $("iframe").contents().find('td').css('width', '100%');

        // Change hover effects
        $("iframe").contents().find(".goog-te-menu2-item div").hover(function () {
            $(this).css('background-color', '#01c8c7').find('span.text').css('color', 'white');
        }, function () {
            $(this).css('background-color', 'white').find('span.text').css('color', '#544F4B');
        });

        // Change Google's default blue border
        $("iframe").contents().find('.goog-te-menu2').css('border', 'none');

        // Change the iframe's box shadow
        $(".goog-te-menu-frame").css('box-shadow', '0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.3)');



        // Change the iframe's size and position?
        $(".goog-te-menu-frame").css({
            'height': '100%',
            'width': '100%',
			'max-width': '250px',
        });
        // Change iframes's size
        $("iframe").contents().find('.goog-te-menu2').css({
            'height': '100%',
            'width': '100%',
			'max-width': '250px',
			'overflow-y': 'auto'
        });
    });
});

function liskSupportAds() {

    $('.close-ad').on('click', closeAd);

    (function showAds() {
        $.each($('[data-ad-id]'), function(index, el) {

            var e = $(el),
                id = e.data('ad-id'),
                visibility = e.data('ad-visibility');

            if(typeof Storage !== "undefined") {
                if(sessionStorage.getItem('adIsViewed[' + id + ']') == '1' ) {
                    e.remove();
                } else {
                    e.addClass(visibility);
                }
            }
        });
    })();

    function closeAd() {
        var e = $(this),
            target = e.data('ad-target'),
            ad = $('[data-ad-id=' + target + ']');

        sessionStorage.setItem('adIsViewed[' + target + ']', '1');
        ad.remove();
    }

}