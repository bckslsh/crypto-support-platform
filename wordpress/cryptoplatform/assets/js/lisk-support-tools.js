var delegate_chart;
var overall_chart;
var overallDataContainer;
var overallChartNames = [];
var overallChartTotals = [];

var overallChartTotal30Days = [];
var overallChartTotal60Days = [];
var overallChartTotal90Days = [];
var overallChartTotal180Days = [];
var overallChartTotal365Days = [];
var overallChartNames30Days = [];
var overallChartNames60Days = [];
var overallChartNames90Days = [];
var overallChartNames180Days = [];
var overallChartNames365Days = [];

var overallIndChartNames = [];
var overallIndChartTotals = [];

var overallIndChartTotal30Days = [];
var overallIndChartTotal60Days = [];
var overallIndChartTotal90Days = [];
var overallIndChartTotal180Days = [];
var overallIndChartTotal365Days = [];
var overallIndChartNames30Days = [];
var overallIndChartNames60Days = [];
var overallIndChartNames90Days = [];
var overallIndChartNames180Days = [];
var overallIndChartNames365Days = [];

var overallDelChartNames = [];
var overallDelChartTotals = [];

var overallDelChartTotal30Days = [];
var overallDelChartTotal60Days = [];
var overallDelChartTotal90Days = [];
var overallDelChartTotal180Days = [];
var overallDelChartTotal365Days = [];
var overallDelChartNames30Days = [];
var overallDelChartNames60Days = [];
var overallDelChartNames90Days = [];
var overallDelChartNames180Days = [];
var overallDelChartNames365Days = [];

var overallDelPChartNames = [];
var overallDelPChartTotals = [];

var overallDelPChartTotal30Days = [];
var overallDelPChartTotal60Days = [];
var overallDelPChartTotal90Days = [];
var overallDelPChartTotal180Days = [];
var overallDelPChartTotal365Days = [];
var overallDelPChartNames30Days = [];
var overallDelPChartNames60Days = [];
var overallDelPChartNames90Days = [];
var overallDelPChartNames180Days = [];
var overallDelPChartNames365Days = [];

//Voting Overall
var overallVotingChartNames = [];
var overallVotingChartTotals = [];

var overallVotingChartTotal30Days = [];
var overallVotingChartTotal60Days = [];
var overallVotingChartTotal90Days = [];
var overallVotingChartTotal180Days = [];
var overallVotingChartTotal365Days = [];
var overallVotingChartNames30Days = [];
var overallVotingChartNames60Days = [];
var overallVotingChartNames90Days = [];
var overallVotingChartNames180Days = [];
var overallVotingChartNames365Days = [];

var overallVotingIndChartNames = [];
var overallVotingIndChartTotals = [];

var overallVotingIndChartTotal30Days = [];
var overallVotingIndChartTotal60Days = [];
var overallVotingIndChartTotal90Days = [];
var overallVotingIndChartTotal180Days = [];
var overallVotingIndChartTotal365Days = [];
var overallVotingIndChartNames30Days = [];
var overallVotingIndChartNames60Days = [];
var overallVotingIndChartNames90Days = [];
var overallVotingIndChartNames180Days = [];
var overallVotingIndChartNames365Days = [];

var overallVotingDelChartNames = [];
var overallVotingDelChartTotals = [];

var overallVotingDelChartTotal30Days = [];
var overallVotingDelChartTotal60Days = [];
var overallVotingDelChartTotal90Days = [];
var overallVotingDelChartTotal180Days = [];
var overallVotingDelChartTotal365Days = [];
var overallVotingDelChartNames30Days = [];
var overallVotingDelChartNames60Days = [];
var overallVotingDelChartNames90Days = [];
var overallVotingDelChartNames180Days = [];
var overallVotingDelChartNames365Days = [];

var overallVotingDelPChartNames = [];
var overallVotingDelPChartTotals = [];

var overallVotingDelPChartTotal30Days = [];
var overallVotingDelPChartTotal60Days = [];
var overallVotingDelPChartTotal90Days = [];
var overallVotingDelPChartTotal180Days = [];
var overallVotingDelPChartTotal365Days = [];
var overallVotingDelPChartNames30Days = [];
var overallVotingDelPChartNames60Days = [];
var overallVotingDelPChartNames90Days = [];
var overallVotingDelPChartNames180Days = [];
var overallVotingDelPChartNames365Days = [];

//NonVoting Overall
var overallNonVotingChartNames = [];
var overallNonVotingChartTotals = [];

var overallNonVotingChartTotal30Days = [];
var overallNonVotingChartTotal60Days = [];
var overallNonVotingChartTotal90Days = [];
var overallNonVotingChartTotal180Days = [];
var overallNonVotingChartTotal365Days = [];
var overallNonVotingChartNames30Days = [];
var overallNonVotingChartNames60Days = [];
var overallNonVotingChartNames90Days = [];
var overallNonVotingChartNames180Days = [];
var overallNonVotingChartNames365Days = [];

var overallNonVotingIndChartNames = [];
var overallNonVotingIndChartTotals = [];

var overallNonVotingIndChartTotal30Days = [];
var overallNonVotingIndChartTotal60Days = [];
var overallNonVotingIndChartTotal90Days = [];
var overallNonVotingIndChartTotal180Days = [];
var overallNonVotingIndChartTotal365Days = [];
var overallNonVotingIndChartNames30Days = [];
var overallNonVotingIndChartNames60Days = [];
var overallNonVotingIndChartNames90Days = [];
var overallNonVotingIndChartNames180Days = [];
var overallNonVotingIndChartNames365Days = [];

var overallNonVotingDelChartNames = [];
var overallNonVotingDelChartTotals = [];

var overallNonVotingDelChartTotal30Days = [];
var overallNonVotingDelChartTotal60Days = [];
var overallNonVotingDelChartTotal90Days = [];
var overallNonVotingDelChartTotal180Days = [];
var overallNonVotingDelChartTotal365Days = [];
var overallNonVotingDelChartNames30Days = [];
var overallNonVotingDelChartNames60Days = [];
var overallNonVotingDelChartNames90Days = [];
var overallNonVotingDelChartNames180Days = [];
var overallNonVotingDelChartNames365Days = [];

var overallNonVotingDelPChartNames = [];
var overallNonVotingDelPChartTotals = [];

var overallNonVotingDelPChartTotal30Days = [];
var overallNonVotingDelPChartTotal60Days = [];
var overallNonVotingDelPChartTotal90Days = [];
var overallNonVotingDelPChartTotal180Days = [];
var overallNonVotingDelPChartTotal365Days = [];
var overallNonVotingDelPChartNames30Days = [];
var overallNonVotingDelPChartNames60Days = [];
var overallNonVotingDelPChartNames90Days = [];
var overallNonVotingDelPChartNames180Days = [];
var overallNonVotingDelPChartNames365Days = [];


var monthlyDataContainer;
//var overall_monthly_chart;

var overallMonthlyData = {};
overallMonthlyData.labels = [];
overallMonthlyData.datasets = [];
var overallMonthlyColorIndex = -1;
var overallMonthlyColors = ['#e57373', '#BA68C8', '#7986CB', '#4FC3F7', '#4DB6AC', '#AED581', '#FFF176', '#FFB74D', '#A1887F', '#90A4AE',
'#F06292', '#9575CD', '#64B5F6', '#4DD0E1', '#81C784', '#DCE775', '#FFD54F', '#FF8A65',
'#e53935', '#8E24AA', '#3949AB', '#039BE5', '#00897B', '#7CB342', '#FDD835', '#FB8C00', '#6D4C41', '#546E7A',
'#D81B60', '#5E35B1', '#1E88E5', '#00ACC1', '#43A047', '#C0CA33', '#FFB300', '#F4511E', '#757575'];

var overallMonthlyDelData = {};
overallMonthlyDelData.labels = [];
overallMonthlyDelData.datasets = [];
var overallMonthlyDelColorIndex = -1;
var overallVotingMonthlyDelColorIndex = -1;
var overallNonVotingMonthlyDelColorIndex = -1;
var overallMonthlyDelColors = ['#e57373', '#BA68C8', '#7986CB', '#4FC3F7', '#4DB6AC', '#AED581', '#FFF176', '#FFB74D', '#A1887F', '#90A4AE',
'#F06292', '#9575CD', '#64B5F6', '#4DD0E1', '#81C784', '#DCE775', '#FFD54F', '#FF8A65',
'#e53935', '#8E24AA', '#3949AB', '#039BE5', '#00897B', '#7CB342', '#FDD835', '#FB8C00', '#6D4C41', '#546E7A',
'#D81B60', '#5E35B1', '#1E88E5', '#00ACC1', '#43A047', '#C0CA33', '#FFB300', '#F4511E', '#757575'];

var overallMonthlyDelPData = {};
overallMonthlyDelPData.labels = [];
overallMonthlyDelPData.datasets = [];
var overallMonthlyDelPColorIndex = -1;
var overallVotingMonthlyDelPColorIndex = -1;
var overallNonVotingMonthlyDelPColorIndex = -1;
var overallMonthlyDelPColors = ['#e57373', '#BA68C8', '#7986CB', '#4FC3F7', '#4DB6AC', '#AED581', '#FFF176', '#FFB74D', '#A1887F', '#90A4AE',
'#F06292', '#9575CD', '#64B5F6', '#4DD0E1', '#81C784', '#DCE775', '#FFD54F', '#FF8A65',
'#e53935', '#8E24AA', '#3949AB', '#039BE5', '#00897B', '#7CB342', '#FDD835', '#FB8C00', '#6D4C41', '#546E7A',
'#D81B60', '#5E35B1', '#1E88E5', '#00ACC1', '#43A047', '#C0CA33', '#FFB300', '#F4511E', '#757575'];

var overallMonthlyIndData = {};
overallMonthlyIndData.labels = [];
overallMonthlyIndData.datasets = [];
var overallMonthlyIndColorIndex = -1;
var overallVotingMonthlyIndColorIndex = -1;
var overallNonVotingMonthlyIndColorIndex = -1;
var overallMonthlyIndColors = ['#e57373', '#BA68C8', '#7986CB', '#4FC3F7', '#4DB6AC', '#AED581', '#FFF176', '#FFB74D', '#A1887F', '#90A4AE',
'#F06292', '#9575CD', '#64B5F6', '#4DD0E1', '#81C784', '#DCE775', '#FFD54F', '#FF8A65',
'#e53935', '#8E24AA', '#3949AB', '#039BE5', '#00897B', '#7CB342', '#FDD835', '#FB8C00', '#6D4C41', '#546E7A',
'#D81B60', '#5E35B1', '#1E88E5', '#00ACC1', '#43A047', '#C0CA33', '#FFB300', '#F4511E', '#757575'];

var overallMonthlyLast3MonthsData = {};
overallMonthlyLast3MonthsData.labels = [];
overallMonthlyLast3MonthsData.datasets = [];

var overallMonthlyLast3MonthsIndData = {};
overallMonthlyLast3MonthsIndData.labels = [];
overallMonthlyLast3MonthsIndData.datasets = [];
var overallMonthlyLast3MonthsDelData = {};
overallMonthlyLast3MonthsDelData.labels = [];
overallMonthlyLast3MonthsDelData.datasets = [];
var overallMonthlyLast3MonthsDelPData = {};
overallMonthlyLast3MonthsDelPData.labels = [];
overallMonthlyLast3MonthsDelPData.datasets = [];

var overallMonthlyLast6MonthsData = {};
overallMonthlyLast6MonthsData.labels = [];
overallMonthlyLast6MonthsData.datasets = [];

var overallMonthlyLast6MonthsIndData = {};
overallMonthlyLast6MonthsIndData.labels = [];
overallMonthlyLast6MonthsIndData.datasets = [];
var overallMonthlyLast6MonthsDelData = {};
overallMonthlyLast6MonthsDelData.labels = [];
overallMonthlyLast6MonthsDelData.datasets = [];
var overallMonthlyLast6MonthsDelPData = {};
overallMonthlyLast6MonthsDelPData.labels = [];
overallMonthlyLast6MonthsDelPData.datasets = [];

var overallMonthlyLast12MonthsData = {};
overallMonthlyLast12MonthsData.labels = [];
overallMonthlyLast12MonthsData.datasets = [];

var overallMonthlyLast12MonthsIndData = {};
overallMonthlyLast12MonthsIndData.labels = [];
overallMonthlyLast12MonthsIndData.datasets = [];
var overallMonthlyLast12MonthsDelData = {};
overallMonthlyLast12MonthsDelData.labels = [];
overallMonthlyLast12MonthsDelData.datasets = [];
var overallMonthlyLast12MonthsDelPData = {};
overallMonthlyLast12MonthsDelPData.labels = [];
overallMonthlyLast12MonthsDelPData.datasets = [];

//Voting Delegates Data
var overallVotingMonthlyData = {};
overallVotingMonthlyData.labels = [];
overallVotingMonthlyData.datasets = [];
var overallVotingMonthlyColorIndex = -1;

var overallVotingMonthlyDelData = {};
overallVotingMonthlyDelData.labels = [];
overallVotingMonthlyDelData.datasets = [];

var overallVotingMonthlyDelPData = {};
overallVotingMonthlyDelPData.labels = [];
overallVotingMonthlyDelPData.datasets = [];

var overallVotingMonthlyIndData = {};
overallVotingMonthlyIndData.labels = [];
overallVotingMonthlyIndData.datasets = [];

var overallVotingMonthlyLast3MonthsData = {};
overallVotingMonthlyLast3MonthsData.labels = [];
overallVotingMonthlyLast3MonthsData.datasets = [];

var overallVotingMonthlyLast3MonthsIndData = {};
overallVotingMonthlyLast3MonthsIndData.labels = [];
overallVotingMonthlyLast3MonthsIndData.datasets = [];
var overallVotingMonthlyLast3MonthsDelData = {};
overallVotingMonthlyLast3MonthsDelData.labels = [];
overallVotingMonthlyLast3MonthsDelData.datasets = [];
var overallVotingMonthlyLast3MonthsDelPData = {};
overallVotingMonthlyLast3MonthsDelPData.labels = [];
overallVotingMonthlyLast3MonthsDelPData.datasets = [];

var overallVotingMonthlyLast6MonthsData = {};
overallVotingMonthlyLast6MonthsData.labels = [];
overallVotingMonthlyLast6MonthsData.datasets = [];

var overallVotingMonthlyLast6MonthsIndData = {};
overallVotingMonthlyLast6MonthsIndData.labels = [];
overallVotingMonthlyLast6MonthsIndData.datasets = [];
var overallVotingMonthlyLast6MonthsDelData = {};
overallVotingMonthlyLast6MonthsDelData.labels = [];
overallVotingMonthlyLast6MonthsDelData.datasets = [];
var overallVotingMonthlyLast6MonthsDelPData = {};
overallVotingMonthlyLast6MonthsDelPData.labels = [];
overallVotingMonthlyLast6MonthsDelPData.datasets = [];

var overallVotingMonthlyLast12MonthsData = {};
overallVotingMonthlyLast12MonthsData.labels = [];
overallVotingMonthlyLast12MonthsData.datasets = [];

var overallVotingMonthlyLast12MonthsIndData = {};
overallVotingMonthlyLast12MonthsIndData.labels = [];
overallVotingMonthlyLast12MonthsIndData.datasets = [];
var overallVotingMonthlyLast12MonthsDelData = {};
overallVotingMonthlyLast12MonthsDelData.labels = [];
overallVotingMonthlyLast12MonthsDelData.datasets = [];
var overallVotingMonthlyLast12MonthsDelPData = {};
overallVotingMonthlyLast12MonthsDelPData.labels = [];
overallVotingMonthlyLast12MonthsDelPData.datasets = [];

//Voting Delegates Data
var overallNonVotingMonthlyData = {};
overallNonVotingMonthlyData.labels = [];
overallNonVotingMonthlyData.datasets = [];
var overallNonVotingMonthlyColorIndex = -1;

var overallNonVotingMonthlyDelData = {};
overallNonVotingMonthlyDelData.labels = [];
overallNonVotingMonthlyDelData.datasets = [];

var overallNonVotingMonthlyDelPData = {};
overallNonVotingMonthlyDelPData.labels = [];
overallNonVotingMonthlyDelPData.datasets = [];

var overallNonVotingMonthlyIndData = {};
overallNonVotingMonthlyIndData.labels = [];
overallNonVotingMonthlyIndData.datasets = [];

var overallNonVotingMonthlyLast3MonthsData = {};
overallNonVotingMonthlyLast3MonthsData.labels = [];
overallNonVotingMonthlyLast3MonthsData.datasets = [];

var overallNonVotingMonthlyLast3MonthsIndData = {};
overallNonVotingMonthlyLast3MonthsIndData.labels = [];
overallNonVotingMonthlyLast3MonthsIndData.datasets = [];
var overallNonVotingMonthlyLast3MonthsDelData = {};
overallNonVotingMonthlyLast3MonthsDelData.labels = [];
overallNonVotingMonthlyLast3MonthsDelData.datasets = [];
var overallNonVotingMonthlyLast3MonthsDelPData = {};
overallNonVotingMonthlyLast3MonthsDelPData.labels = [];
overallNonVotingMonthlyLast3MonthsDelPData.datasets = [];

var overallNonVotingMonthlyLast6MonthsData = {};
overallNonVotingMonthlyLast6MonthsData.labels = [];
overallNonVotingMonthlyLast6MonthsData.datasets = [];

var overallNonVotingMonthlyLast6MonthsIndData = {};
overallNonVotingMonthlyLast6MonthsIndData.labels = [];
overallNonVotingMonthlyLast6MonthsIndData.datasets = [];
var overallNonVotingMonthlyLast6MonthsDelData = {};
overallNonVotingMonthlyLast6MonthsDelData.labels = [];
overallNonVotingMonthlyLast6MonthsDelData.datasets = [];
var overallNonVotingMonthlyLast6MonthsDelPData = {};
overallNonVotingMonthlyLast6MonthsDelPData.labels = [];
overallNonVotingMonthlyLast6MonthsDelPData.datasets = [];

var overallNonVotingMonthlyLast12MonthsData = {};
overallNonVotingMonthlyLast12MonthsData.labels = [];
overallNonVotingMonthlyLast12MonthsData.datasets = [];

var overallNonVotingMonthlyLast12MonthsIndData = {};
overallNonVotingMonthlyLast12MonthsIndData.labels = [];
overallNonVotingMonthlyLast12MonthsIndData.datasets = [];
var overallNonVotingMonthlyLast12MonthsDelData = {};
overallNonVotingMonthlyLast12MonthsDelData.labels = [];
overallNonVotingMonthlyLast12MonthsDelData.datasets = [];
var overallNonVotingMonthlyLast12MonthsDelPData = {};
overallNonVotingMonthlyLast12MonthsDelPData.labels = [];
overallNonVotingMonthlyLast12MonthsDelPData.datasets = [];

var isStacked = true;
var chartType = 'bar';
var poolSwitch = 'All'; //GroupWInd, GroupWOInd, Ind
var votingSwitch = 'Both'; //Yes, No
var dataRange = 0; //3, 6, 12
var votingDelegates = [];

var overallRangeSwitch = 0;
var overallPoolSwitch = 'All';
var overallVotingSwitch = 'Both';

//This array does not include gdtpool delegate, as they are already added
var GDTMembers = ['bioly', 'corsaro', 'dakk', 'forrest', 'hagie', 'joel', 'joo5ty', 'kushed.delegate', 'liskit', 'ntelo', 'ondin',
'philhellmuth', 'redsn0w', 'sgdias', 'slasheks', 'splatters', 'tembo', 'vekexasia', 'vi1son'];

function GetOverallMonthlyColor()
{
    overallMonthlyColorIndex++;
    return overallMonthlyColors[overallMonthlyColorIndex];
}
function GetOverallMonthlyDelColor()
{
    overallMonthlyDelColorIndex++;
    return overallMonthlyDelColors[overallMonthlyDelColorIndex];
}
function GetOverallMonthlyDelPColor()
{
    overallMonthlyDelPColorIndex++;
    return overallMonthlyDelPColors[overallMonthlyDelPColorIndex];
}
function GetOverallMonthlyIndColor()
{
    overallMonthlyIndColorIndex++;
    return overallMonthlyIndColors[overallMonthlyIndColorIndex];
}

//Voting
function GetOverallVotingMonthlyColor()
{
    overallVotingMonthlyColorIndex++;
    return overallMonthlyColors[overallVotingMonthlyColorIndex];
}
function GetOverallVotingMonthlyDelColor()
{
    overallVotingMonthlyDelColorIndex++;
    return overallMonthlyDelColors[overallVotingMonthlyDelColorIndex];
}
function GetOverallVotingMonthlyDelPColor()
{
    overallVotingMonthlyDelPColorIndex++;
    return overallMonthlyDelPColors[overallVotingMonthlyDelPColorIndex];
}
function GetOverallVotingMonthlyIndColor()
{
    overallVotingMonthlyIndColorIndex++;
    return overallMonthlyIndColors[overallVotingMonthlyIndColorIndex];
}

//NonVoting
function GetOverallNonVotingMonthlyColor()
{
    overallNonVotingMonthlyColorIndex++;
    return overallMonthlyColors[overallNonVotingMonthlyColorIndex];
}
function GetOverallNonVotingMonthlyDelColor()
{
    overallNonVotingMonthlyDelColorIndex++;
    return overallMonthlyDelColors[overallNonVotingMonthlyDelColorIndex];
}
function GetOverallNonVotingMonthlyDelPColor()
{
    overallNonVotingMonthlyDelPColorIndex++;
    return overallMonthlyDelPColors[overallNonVotingMonthlyDelPColorIndex];
}
function GetOverallNonVotingMonthlyIndColor()
{
    overallNonVotingMonthlyIndColorIndex++;
    return overallMonthlyIndColors[overallNonVotingMonthlyIndColorIndex];
}

function overallMonthlyLabels(transactions)
{
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

    //get timestamp of first transaction
    let startTimestamp = transactions.transactions[0].timestamp;

    //convert first timestamp to UTC
    let startUTC = timeConverter(startTimestamp + 1464109200);

    //get Month and Year of first timestamp
    let startSplit = startUTC.split(' ');
    //let startDate = startSplit[1] + ' ' + startSplit[2];

    //get timestamp of last transaction
    let endTimestamp = transactions.transactions[transactions.transactions.length - 1].timestamp;

    //convert last timestamp to UTC
    let endUTC = timeConverter(endTimestamp + 1464109200);

    //get Month and Year of last timestamp
    let endSplit = endUTC.split(' ');
    //let endDate = endSplit[1] + ' ' + endSplit[2];

    //get index of starting Month
    let monthIndex = -1;
    for(let m = 0; m < months.length; m++)
    {
        if(startSplit[1] === months[m])
        {
            monthIndex = m;
        }
    }

    //populate labels - needs loops
    //outside loop - year
    for(let y = startSplit[2]; y <= endSplit[2]; y++)
    {
        if(monthIndex === 12)
        {
            monthIndex = 0;
        }

        //inside loop - month
        while(monthIndex < 12){
            overallMonthlyData.labels.push(months[monthIndex] + ' ' + y);
            overallMonthlyDelData.labels.push(months[monthIndex] + ' ' + y);
            overallMonthlyDelPData.labels.push(months[monthIndex] + ' ' + y);
            overallMonthlyIndData.labels.push(months[monthIndex] + ' ' + y);
            overallVotingMonthlyData.labels.push(months[monthIndex] + ' ' + y);
            overallVotingMonthlyDelData.labels.push(months[monthIndex] + ' ' + y);
            overallVotingMonthlyDelPData.labels.push(months[monthIndex] + ' ' + y);
            overallVotingMonthlyIndData.labels.push(months[monthIndex] + ' ' + y);
            overallNonVotingMonthlyData.labels.push(months[monthIndex] + ' ' + y);
            overallNonVotingMonthlyDelData.labels.push(months[monthIndex] + ' ' + y);
            overallNonVotingMonthlyDelPData.labels.push(months[monthIndex] + ' ' + y);
            overallNonVotingMonthlyIndData.labels.push(months[monthIndex] + ' ' + y);
            
            if((months[monthIndex] == endSplit[1] && y == endSplit[2]))
            {
                break;
            }
            monthIndex++;
        }
    } //End outside (year) loop
}

function addToOverallMonthlyData (dName, dMonth, dAmount)
{
    //check if delegate already exists in dataset
    let indexOfDelegate = -1;
    for (let i = 0; i < overallMonthlyData.datasets.length; i++)
    {
        if(overallMonthlyData.datasets[i].label === dName) 
        {
            indexOfDelegate = i;
        }
    }

    let ddMonthYear = -1;
    for(let ii = 0; ii < overallMonthlyData.labels.length; ii++)
    {
        if(overallMonthlyData.labels[ii] == dMonth)
        {
            ddMonthYear = ii;
        }
    }

    if(indexOfDelegate !== -1)
    {
        overallMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallMonthlyData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
    }
    else //delegate not in overallMonthlyData yet
    {
        overallMonthlyData.datasets.push({label:dName,data:[],backgroundColor: GetOverallMonthlyColor()});
        for(let ii = 0; ii < overallMonthlyData.labels.length; ii++)
        {
            overallMonthlyData.datasets[overallMonthlyData.datasets.length - 1].data[ii] = 0;
        }
        overallMonthlyData.datasets[overallMonthlyData.datasets.length - 1].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallMonthlyData.datasets[overallMonthlyData.datasets.length - 1].data[ddMonthYear] = Math.round(overallMonthlyData.datasets[overallMonthlyData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
    }

    //Add to Ind
    if(dName !== 'GDT Pool' && dName !== 'Elite + Supporters' && dName !== 'Sherwood')
    {
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallMonthlyIndData.datasets.length; i++)
        {
            if(overallMonthlyIndData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallMonthlyIndData yet
        {
            overallMonthlyIndData.datasets.push({label:dName,data:[],backgroundColor: GetOverallMonthlyIndColor()});
            //for(let ii = 0; ii < overallMonthlyIndData.labels.length; ii++)
            for(let ii = 0; ii < overallMonthlyIndData.labels.length; ii++)
            {
                overallMonthlyIndData.datasets[overallMonthlyIndData.datasets.length - 1].data[ii] = 0;
            }
            overallMonthlyIndData.datasets[overallMonthlyIndData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyIndData.datasets[overallMonthlyIndData.datasets.length - 1].data[ddMonthYear] = Math.round(overallMonthlyIndData.datasets[overallMonthlyIndData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //Ind DelP Add
        if(GDTMembers.indexOf(dName) > -1)
        {
            indexOfDelegate = -1;
            for (let i = 0; i < overallMonthlyDelPData.datasets.length; i++)
            {
                if(overallMonthlyDelPData.datasets[i].label === "GDT Pool") 
                {
                    indexOfDelegate = i;
                }
            }
            
            if(indexOfDelegate !== -1)
            {
                overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
            }
            else //delegate not in overallMonthlyDelPData yet
            {
                overallMonthlyDelPData.datasets.push({label:'GDT Pool',data:[],backgroundColor: GetOverallMonthlyDelPColor()});
                //for(let ii = 0; ii < overallMonthlyDelPData.labels.length; ii++)
                for(let ii = 0; ii < overallMonthlyDelPData.labels.length; ii++)
                {
                    overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ii] = 0;
                }
                overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
            }
        }
    }
    else //Add to Del & Delp
    {
        //Del
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallMonthlyDelData.datasets.length; i++)
        {
            if(overallMonthlyDelData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallMonthlyIndData yet
        {
            overallMonthlyDelData.datasets.push({label:dName,data:[],backgroundColor: GetOverallMonthlyDelColor()});
            //for(let ii = 0; ii < overallMonthlyDelData.labels.length; ii++)
            for(let ii = 0; ii < overallMonthlyDelData.labels.length; ii++)
            {
                overallMonthlyDelData.datasets[overallMonthlyDelData.datasets.length - 1].data[ii] = 0;
            }
            overallMonthlyDelData.datasets[overallMonthlyDelData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyDelData.datasets[overallMonthlyDelData.datasets.length - 1].data[ddMonthYear] = Math.round(overallMonthlyDelData.datasets[overallMonthlyDelData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //DelP
        indexOfDelegate = -1;
        for (let i = 0; i < overallMonthlyDelPData.datasets.length; i++)
        {
            if(overallMonthlyDelPData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallMonthlyDelPData yet
        {
            overallMonthlyDelPData.datasets.push({label:dName,data:[],backgroundColor: GetOverallMonthlyDelPColor()});
            //for(let ii = 0; ii < overallMonthlyDelPData.labels.length; ii++)
            for(let ii = 0; ii < overallMonthlyDelPData.labels.length; ii++)
            {
                overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ii] = 0;
            }
            overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallMonthlyDelPData.datasets[overallMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }
    }
}

function addToOverallVotingMonthlyData (dName, dMonth, dAmount)
{
    //check if delegate already exists in dataset
    let indexOfDelegate = -1;
    for (let i = 0; i < overallVotingMonthlyData.datasets.length; i++)
    {
        if(overallVotingMonthlyData.datasets[i].label === dName) 
        {
            indexOfDelegate = i;
        }
    }

    let ddMonthYear = -1;
    for(let ii = 0; ii < overallVotingMonthlyData.labels.length; ii++)
    {
        if(overallVotingMonthlyData.labels[ii] == dMonth)
        {
            ddMonthYear = ii;
        }
    }

    if(indexOfDelegate !== -1)
    {
        overallVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
    }
    else //delegate not in overallVotingMonthlyData yet
    {
        overallVotingMonthlyData.datasets.push({label:dName,data:[],backgroundColor: GetOverallVotingMonthlyColor()});
        for(let ii = 0; ii < overallVotingMonthlyData.labels.length; ii++)
        {
            overallVotingMonthlyData.datasets[overallVotingMonthlyData.datasets.length - 1].data[ii] = 0;
        }
        overallVotingMonthlyData.datasets[overallVotingMonthlyData.datasets.length - 1].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallVotingMonthlyData.datasets[overallVotingMonthlyData.datasets.length - 1].data[ddMonthYear] = Math.round(overallVotingMonthlyData.datasets[overallVotingMonthlyData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
    }

    //Add to Ind
    if(dName !== 'GDT Pool' && dName !== 'Elite + Supporters' && dName !== 'Sherwood')
    {
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallVotingMonthlyIndData.datasets.length; i++)
        {
            if(overallVotingMonthlyIndData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallMonthlyIndData yet
        {
            overallVotingMonthlyIndData.datasets.push({label:dName,data:[],backgroundColor: GetOverallVotingMonthlyIndColor()});
            //for(let ii = 0; ii < overallVotingMonthlyIndData.labels.length; ii++)
            for(let ii = 0; ii < overallVotingMonthlyIndData.labels.length; ii++)
            {
                overallVotingMonthlyIndData.datasets[overallVotingMonthlyIndData.datasets.length - 1].data[ii] = 0;
            }
            overallVotingMonthlyIndData.datasets[overallVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyIndData.datasets[overallVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear] = Math.round(overallVotingMonthlyIndData.datasets[overallVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //Ind DelP Add
        if(GDTMembers.indexOf(dName) > -1)
        {
            indexOfDelegate = -1;
            for (let i = 0; i < overallVotingMonthlyDelPData.datasets.length; i++)
            {
                if(overallVotingMonthlyDelPData.datasets[i].label === "GDT Pool") 
                {
                    indexOfDelegate = i;
                }
            }
            
            if(indexOfDelegate !== -1)
            {
                overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
            }
            else //delegate not in overallMonthlyDelPData yet
            {
                overallVotingMonthlyDelPData.datasets.push({label:'GDT Pool',data:[],backgroundColor: GetOverallVotingMonthlyDelPColor()});
                //for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
                for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
                {
                    overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ii] = 0;
                }
                overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
            }
        }
    }
    else //Add to Del & Delp
    {
        //Del
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallVotingMonthlyDelData.datasets.length; i++)
        {
            if(overallVotingMonthlyDelData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallVotingMonthlyIndData yet
        {
            overallVotingMonthlyDelData.datasets.push({label:dName,data:[],backgroundColor: GetOverallVotingMonthlyDelColor()});
            //for(let ii = 0; ii < overallVotingMonthlyDelData.labels.length; ii++)
            for(let ii = 0; ii < overallVotingMonthlyDelData.labels.length; ii++)
            {
                overallVotingMonthlyDelData.datasets[overallVotingMonthlyDelData.datasets.length - 1].data[ii] = 0;
            }
            overallVotingMonthlyDelData.datasets[overallVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyDelData.datasets[overallVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear] = Math.round(overallVotingMonthlyDelData.datasets[overallVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //DelP
        indexOfDelegate = -1;
        for (let i = 0; i < overallVotingMonthlyDelPData.datasets.length; i++)
        {
            if(overallVotingMonthlyDelPData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallVotingMonthlyDelPData yet
        {
            overallVotingMonthlyDelPData.datasets.push({label:dName,data:[],backgroundColor: GetOverallVotingMonthlyDelPColor()});
            //for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
            for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
            {
                overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ii] = 0;
            }
            overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallVotingMonthlyDelPData.datasets[overallVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }
    }
}

function addToOverallNonVotingMonthlyData (dName, dMonth, dAmount)
{
    //check if delegate already exists in dataset
    let indexOfDelegate = -1;
    for (let i = 0; i < overallNonVotingMonthlyData.datasets.length; i++)
    {
        if(overallNonVotingMonthlyData.datasets[i].label === dName) 
        {
            indexOfDelegate = i;
        }
    }

    let ddMonthYear = -1;
    for(let ii = 0; ii < overallNonVotingMonthlyData.labels.length; ii++)
    {
        if(overallNonVotingMonthlyData.labels[ii] == dMonth)
        {
            ddMonthYear = ii;
        }
    }

    if(indexOfDelegate !== -1)
    {
        overallNonVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallNonVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallNonVotingMonthlyData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
    }
    else //delegate not in overallNonVotingMonthlyData yet
    {
        overallNonVotingMonthlyData.datasets.push({label:dName,data:[],backgroundColor: GetOverallNonVotingMonthlyColor()});
        for(let ii = 0; ii < overallNonVotingMonthlyData.labels.length; ii++)
        {
            overallNonVotingMonthlyData.datasets[overallNonVotingMonthlyData.datasets.length - 1].data[ii] = 0;
        }
        overallNonVotingMonthlyData.datasets[overallNonVotingMonthlyData.datasets.length - 1].data[ddMonthYear] += dAmount;
        //correct rounding error
        overallNonVotingMonthlyData.datasets[overallNonVotingMonthlyData.datasets.length - 1].data[ddMonthYear] = Math.round(overallNonVotingMonthlyData.datasets[overallNonVotingMonthlyData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
    }

    //Add to Ind
    if(dName !== 'GDT Pool' && dName !== 'Elite + Supporters' && dName !== 'Sherwood')
    {
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallNonVotingMonthlyIndData.datasets.length; i++)
        {
            if(overallNonVotingMonthlyIndData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallNonVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallNonVotingMonthlyIndData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallMonthlyIndData yet
        {
            overallNonVotingMonthlyIndData.datasets.push({label:dName,data:[],backgroundColor: GetOverallNonVotingMonthlyIndColor()});
            //for(let ii = 0; ii < overallVotingMonthlyIndData.labels.length; ii++)
            for(let ii = 0; ii < overallNonVotingMonthlyIndData.labels.length; ii++)
            {
                overallNonVotingMonthlyIndData.datasets[overallNonVotingMonthlyIndData.datasets.length - 1].data[ii] = 0;
            }
            overallNonVotingMonthlyIndData.datasets[overallNonVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyIndData.datasets[overallNonVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear] = Math.round(overallNonVotingMonthlyIndData.datasets[overallNonVotingMonthlyIndData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //Ind DelP Add
        if(GDTMembers.indexOf(dName) > -1)
        {
            indexOfDelegate = -1;
            for (let i = 0; i < overallNonVotingMonthlyDelPData.datasets.length; i++)
            {
                if(overallNonVotingMonthlyDelPData.datasets[i].label === "GDT Pool") 
                {
                    indexOfDelegate = i;
                }
            }
            
            if(indexOfDelegate !== -1)
            {
                overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
            }
            else //delegate not in overallMonthlyDelPData yet
            {
                overallNonVotingMonthlyDelPData.datasets.push({label:'GDT Pool',data:[],backgroundColor: GetOverallNonVotingMonthlyDelPColor()});
                //for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
                for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
                {
                    overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ii] = 0;
                }
                overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
                //correct rounding error
                overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
            }
        }
    }
    else //Add to Del & Delp
    {
        //Del
        //check if delegate already exists in dataset
        indexOfDelegate = -1;
        for (let i = 0; i < overallNonVotingMonthlyDelData.datasets.length; i++)
        {
            if(overallNonVotingMonthlyDelData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallNonVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallNonVotingMonthlyIndData yet
        {
            overallNonVotingMonthlyDelData.datasets.push({label:dName,data:[],backgroundColor: GetOverallNonVotingMonthlyDelColor()});
            //for(let ii = 0; ii < overallNonVotingMonthlyDelData.labels.length; ii++)
            for(let ii = 0; ii < overallNonVotingMonthlyDelData.labels.length; ii++)
            {
                overallNonVotingMonthlyDelData.datasets[overallNonVotingMonthlyDelData.datasets.length - 1].data[ii] = 0;
            }
            overallNonVotingMonthlyDelData.datasets[overallNonVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyDelData.datasets[overallNonVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelData.datasets[overallNonVotingMonthlyDelData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }

        //DelP
        indexOfDelegate = -1;
        for (let i = 0; i < overallNonVotingMonthlyDelPData.datasets.length; i++)
        {
            if(overallNonVotingMonthlyDelPData.datasets[i].label === dName) 
            {
                indexOfDelegate = i;
            }
        }
        
        if(indexOfDelegate !== -1)
        {
            overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelPData.datasets[indexOfDelegate].data[ddMonthYear]*100000000)/100000000;
        }
        else //delegate not in overallNonVotingMonthlyDelPData yet
        {
            overallNonVotingMonthlyDelPData.datasets.push({label:dName,data:[],backgroundColor: GetOverallNonVotingMonthlyDelPColor()});
            //for(let ii = 0; ii < overallVotingMonthlyDelPData.labels.length; ii++)
            for(let ii = 0; ii < overallNonVotingMonthlyDelPData.labels.length; ii++)
            {
                overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ii] = 0;
            }
            overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] += dAmount;
            //correct rounding error
            overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear] = Math.round(overallNonVotingMonthlyDelPData.datasets[overallNonVotingMonthlyDelPData.datasets.length - 1].data[ddMonthYear]*100000000)/100000000;
        }
    }
}

function reload() {
    location.reload();
}

function isJson(str) {
    try {
        JSON.parse(str); 
    } catch (e) {
        return false;
    }
    return true;
}

var allVotes = new Array();
function SetAllVotes(transactions)
{
    for(var i = 0; i < transactions.transactions.length; i++)
    {
        if(transactions.transactions[i].type === 3) //delegate vote
        {
            fetch('https://wallet.lisknode.io/api/transactions/get?id=' + transactions.transactions[i].id)
            .then(res => res.json())
            .then((out) => {
                SetAllVotesHelper(out);
            }).catch(err => console.error(err));
        }
    }
}

function SetAllVotesHelper(votes)
{
    for(var i = 0; i < votes.transaction.votes.added.length; i++)
    {
        if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
        {
            allVotes.push(votes.transaction.votes.added[i]);
        }
    }
}

var allTrans = new Object();
function AddTransactions(transactions, address)
{
    for(var i = 1000; i < transactions.count; i+=1000)
    {
        fetch('https://wallet.lisknode.io/api/transactions?recipientId=' + address+'&limit=1000&offset='+i)
        .then(res => res.json())
        .then((out) => {
            AddTransactionsHelper(out);
        }).catch(err => console.error(err));
    }
}

function AddTransactionsHelper(transactions)
{
    for(let i = 0; i < transactions.transactions.length; i++)
    {
        allTrans.transactions.push(transactions.transactions[i]);
    }
}

function isSorted(arr) {
    const limit = arr.length - 1;
    return arr.every((_, i) => (i < limit ? arr[i] <= arr[i + 1] : true));
}

function Request(myaddress, mydele)
{
    var req = {address:myaddress, dele:mydele};
    return req;
}

var listOfDelegates;
var total = 0.0;
var firstNoTool = true;
var processingAddress = null;
var index = 0;

function connect() {
    var LiskAddress = document.getElementById("LSKAddressBox").value;
    var validAddress = CheckLSKAddress(LiskAddress);
    if(validAddress)
    {
        document.getElementById("LSKAddress").style.display = 'none';
        document.getElementById("startButton").style.display = 'none';
        document.getElementById("InvalidAddress").style.display = 'none';
        document.getElementById("Calculating").style.display = 'block';
        document.getElementById("percentPara").style.display = 'block';
        var percentText = document.createTextNode('0% Done');
        document.getElementById('percentPara').innerHTML = '';
        percentPara.appendChild(percentText);
	    var ws = new WebSocket("wss://www.liskpending.nl:1616/service");
        ws.onopen = function () {
            ws.send(JSON.stringify(Request(LiskAddress, processingAddress)));
	    //ws.send('Test123');
        };

        ws.onmessage = function (evt) {
            var received_msg;
            received_msg = JSON.parse(evt.data);
            
            if(!!received_msg)
            {
                if(typeof received_msg === 'object' && received_msg.hasOwnProperty('name') && received_msg.hasOwnProperty('amount') && received_msg.hasOwnProperty('source'))
                {
                    //Add to table
                    if(!!received_msg.source)
                    {
                        var tr = tbl.insertRow();
                        var td1 = tr.insertCell();
                        td1.appendChild(document.createTextNode(received_msg.name));
                        var td2 = tr.insertCell();
                        td2.appendChild(document.createTextNode(received_msg.amount.toFixed(8)));
            
                        total += received_msg.amount;

                        var td4 = tr.insertCell();
                        if(received_msg.name === 'hoop' || received_msg.name === 'index' || received_msg.name === 'corsaro' || received_msg.name === 'dakk')
                        {
                            td4.appendChild(document.createTextNode('0.5 LSK'));
                        }
                        else if(received_msg.name === 'forrest')
                        {
                            td4.appendChild(document.createTextNode('2 LSK'));
                        }
						else if(received_msg.name === 'communitypool')
                        {
                            td4.appendChild(document.createTextNode('3 LSK'));
                        }
                        else if(received_msg.name === 'shinekami')
                        {
                            td4.appendChild(document.createTextNode('4 LSK'));
                        }
                        else if(received_msg.name === 'liskpool.top')
                        {
                            td4.appendChild(document.createTextNode('5 LSK'));
                        }
                        else if(received_msg.name === 'liskpool_com_01')
                        {
                            td4.appendChild(document.createTextNode('6 LSK'));
                        }
                        else
                        {
                            td4.appendChild(document.createTextNode('1 LSK'));
                        }
            
                        var td3 = tr.insertCell();
            
                        var a = document.createElement('a');
                        var linkText = document.createTextNode(received_msg.source);
                        a.appendChild(linkText);
                        a.title = received_msg.source;
                        a.href = received_msg.source;
                        td3.appendChild(a);
                    }
                    else
                    {
                        var delegateName = "";
                        if(firstNoTool)
                        {
                            firstNoTool = false;
                            delegateName = document.createTextNode(received_msg.name);
                        }
                        else
                        {
                            delegateName = document.createTextNode(', ' + received_msg.name);
                        }
                        noToolList.appendChild(delegateName);
                    }

                    //Find where in listOFDelegates the delegate is
                    index = listOfDelegates.indexOf(received_msg.name);

                    //Display % done
                    var percentText = document.createTextNode(Math.round(((index+1)/listOfDelegates.length) * 100)+'% Done');
                    document.getElementById('percentPara').innerHTML = '';
                    percentPara.appendChild(percentText);

                    //Find out if it is the end of the list
                    if(index >= listOfDelegates.length - 1) //If it is, end connection
                    {
                        //Total Row
                        var trtotal = tbl.insertRow();
                        var tdtotal1 = trtotal.insertCell();
                        tdtotal1.appendChild(document.createTextNode('Total'));
                        var tdtotal2 = trtotal.insertCell();
                        tdtotal2.appendChild(document.createTextNode(total.toFixed(8)));
                        var tdtotal3 = trtotal.insertCell();
                        tdtotal3.appendChild(document.createTextNode(''));
                    
                        var totalText = document.createTextNode('Total Pending: ' + total.toFixed(8));
                        totalPendingPara.appendChild(totalText);
                        document.getElementById("totalPendingPara").style.display = 'block';
                        document.getElementById("Calculating").style.display = 'none';
                        document.getElementById("percentPara").style.display = 'none';
                        document.getElementById("reloadButton").style.display = 'block';

                        ws.close();
                    }
                    else //If not, send next elements
                    {
                        processingAddress = listOfDelegates[index + 1];
                        ws.send(JSON.stringify(Request(LiskAddress, listOfDelegates[index + 1])));
                    }
                }
                else //Receive list of delegates the address entered is voting for + have pools
                {
                    //Table for delegates with online tool
                    var trheader1 = tbl.insertRow();
                    var tdheader1 = trheader1.insertCell();
                    tdheader1.appendChild(document.createTextNode('Name'));
                    var tdheader2 = trheader1.insertCell();
                    tdheader2.appendChild(document.createTextNode('Amount'));
                    var tdheader4 = trheader1.insertCell();
                    tdheader4.appendChild(document.createTextNode('Min'));
                    var tdheader3 = trheader1.insertCell();
                    tdheader3.appendChild(document.createTextNode('Source'));

                    document.getElementById("tbl").style.display = 'block'; 
                    document.getElementById("noToolPara").style.display = 'block';
                    document.getElementById("noToolList").style.display = 'block';

                    var noToolText = document.createTextNode('Forgers who give individual rewards, but have no online tool we can use to calculate pending payouts: ');
                    noToolPara.appendChild(noToolText);

                    listOfDelegates = received_msg;

                    if(listOfDelegates.length > 0)
                    {
                        processingDelegate = listOfDelegates[0];
                        ws.send(JSON.stringify(Request(LiskAddress, listOfDelegates[0])));
                    }
                    else
                    {
                        var trtotal = tbl.insertRow();
                        var tdtotal1 = trtotal.insertCell();
                        tdtotal1.appendChild(document.createTextNode('Total'));
                        var tdtotal2 = trtotal.insertCell();
                        tdtotal2.appendChild(document.createTextNode(total.toFixed(8)));
                        var tdtotal3 = trtotal.insertCell();
                        tdtotal3.appendChild(document.createTextNode(''));
                    
                        var totalText = document.createTextNode('Total Pending: ' + total.toFixed(8));
                        totalPendingPara.appendChild(totalText);
                        document.getElementById("totalPendingPara").style.display = 'block';
                        document.getElementById("Calculating").style.display = 'none';
                        document.getElementById("percentPara").style.display = 'none';
                        document.getElementById("reloadButton").style.display = 'block';

                        ws.close();
                    }
                }
            }
        };

        ws.onclose = function () {
            // websocket is closed.
            //alert("Connection is closed...");
            if(index < listOfDelegates.length - 1)
            {
                setTimeout(function () {
                    connect();
                }, 7500);
            }
        };
    }
    else //invalid address was entered
    {
        document.getElementById("InvalidAddress").style.display = 'block';
    }
};

function CheckLSKAddress(address)
{
    //ends in L
    if (!(address.length >= 7 && address.length <= 21))
    {
        //alert(address + " is not proper length: " + address.length);
        return false;
    }
    
    //check if address is numeric
    var test = isNaN(address.substring(0, address.length-1));
    if (test)
    {
        //alert(address + " is not a number: " + address.substring(0, address.length-1))
        return false;
    }
    else if(address.charAt(address.length-1) !== "L") //Does the address end in L?
    {
        //alert(address + " does not end in L");
        return false;
    }
    else //Valid address
    {
        //alert(address + " is valid");
        return true;
    }
}

function connectnow() {
    //ResetButtonBG('button');
    var LiskAddress = document.getElementById("LSKAddressBox").value;
    var validAddress = CheckLSKAddress(LiskAddress);
    if(validAddress)
    {
      GetPayouts(LiskAddress);
    }
    jQuery( "#startButton" ).remove();
	jQuery( "#LSKAddress" ).remove();
    jQuery( "#reloadButton" ).css({ 'display': "block" });
    
}

function timeConverter(UNIX_timestamp){
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    if(hour < 10)
    {
        hour = '0' + hour;
    }
    var min = a.getMinutes();
    if(min < 10)
    {
        min = '0' + min;
    }
    var sec = a.getSeconds();
    if(sec < 10)
    {
        sec = '0' + sec;
    }

    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}

function unixConverter(dateStr){
    dateStr = dateStr.split(" ");
    //0 = day
    //1 = month
    //2 = year
    //3 = hr:min:sec

    var myTime = dateStr[3].split(":");
    //0 = hr
    //1 = min
    //2 = sec

    //var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    if(dateStr[1] == 'Jan'){ dateStr[1] = 0;}
    else if(dateStr[1] == 'Feb'){ dateStr[1] = 1;}
    else if(dateStr[1] == 'Mar'){ dateStr[1] = 2;}
    else if(dateStr[1] == 'Apr'){ dateStr[1] = 3;}
    else if(dateStr[1] == 'May'){ dateStr[1] = 4;}
    else if(dateStr[1] == 'Jun'){ dateStr[1] = 5;}
    else if(dateStr[1] == 'Jul'){ dateStr[1] = 6;}
    else if(dateStr[1] == 'Aug'){ dateStr[1] = 7;}
    else if(dateStr[1] == 'Sep'){ dateStr[1] = 8;}
    else if(dateStr[1] == 'Oct'){ dateStr[1] = 9;}
    else if(dateStr[1] == 'Nov'){ dateStr[1] = 10;}
    else if(dateStr[1] == 'Dec'){ dateStr[1] = 11;}

    var datum = new Date(Date.UTC(dateStr[2],dateStr[1],dateStr[0],myTime[0],myTime[1],myTime[2]));
	return (datum.getTime()/1000)+14400;
}

function inElite(delegates){
    var isElite = true;

    var votingList = new Array();
    for (var i = 0; i < delegates.delegates.length; i++) {
        votingList[i] = delegates.delegates[i].username;
    }
    var eliteList = ['carbonara', 'luiz', 'iii.element.iii', 'spacetrucker', 'acheng', 'badman0316', 'leo', 'rooney', 'bigfisher', 'liskjp',
        'will', 'panzer', 'xujian', 'eastwind_ja', 'grajsondelegate', 'mrgr', 'adrianhunter', 'luxiang7890', 'crodam', 'honeybee',
        'phinx', 'seven', 'liskroad', 'chamberlain', 'someonesomeone', 'savetheworld', 'lwyrup', 'luukas', 'carolina', 'hua', 'mac',
        'augurproject', 'forger_of_lisk', 'crolisk', 'veriform', 'goodtimes', 'zy1349', 'yuandian', 'hong', 'bilibili', 'dakini',
        'loveforever', 'cai', 'bigtom', 'jixie', 'blackswan', 'khitan', 'jiandan', 'menfei', 'china', 'kaystar', 'kc', 'catstar', 
        'threelittlepig', 'elonhan'];
        for (var j = 0; j < eliteList.length; j++) {
            if (votingList.indexOf(eliteList[j]) <= -1)
            {
                isElite = false;
            }
        }

    return isElite;
}

function GetPayouts(address){
    jQuery.getJSON('https://wallet.lisknode.io/api/accounts/delegates/?address=' + address, function(delegates) {
        jQuery.getJSON('https://wallet.lisknode.io/api/transactions?recipientId=' + address+'&limit=1000', function(transactionObj) {

            SetAllVotes(transactionObj);
            allTrans = transactionObj; 
            if(transactionObj.count > 1000)
            {
                AddTransactions(transactionObj, address);
            }
            setTimeout(function(){
                transactionObj = allTrans;
                //Setup delegate chart
                var ctx = document.getElementById("delegateChart");

                var config = {
                    type: 'bar',
                    data: {
                        labels: null,
                        datasets: [{
                            type: 'line',
                            label: "Total",
                            fill: false,
                            data: null,
                            backgroundColor:
                                'rgba(255, 159, 64, 0.2)',
                            borderColor: [
                                'rgba(255, 159, 64, 1)'
                            ],
                        }, {
                            type: 'bar',
                            label: "Individual",
                            data: null,
                            backgroundColor:
                            'rgba(0, 159, 100, 0.2)',
                        borderColor: [
                            'rgba(0, 159, 100, 1)'
                        ],
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            labels:{boxWidth:10}
                        },
                        layout: {
                            padding: {
                               top: 25  //set that fits the best
                            }
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                autoSkip: false
                                }
                                }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true,
                                    min: 0
                                }
                            }]
                        },
                        tooltips: {
                            //enabled: false,
                            //mode: 'index',
                            //position: 'nearest',
                            //custom: customTooltips
                            yAlign: 'bottom'
                        }
                    }
                };

                delegate_chart = new Chart(ctx, config);

                var delegateAddresses = new Array();
                delegateAddresses[0] = {name:'5an1ty', address:['14871479939815286293L'], publicKey:'bdbd213cd29238e63abd0ab2f81cb270b7caf1e8ef78161c3a20ea37d1f7b035'};
                delegateAddresses[1] = {name:'bioly', address:['5223701961271242511L'], publicKey:'1681920f9cb83ff2590a8e5c502a7015d4834f5365cf5ed17392c9c78147f94d'};
                delegateAddresses[2] = {name:'bitbanksy', address:['10220254067074498243L'], publicKey:'63db2063e7760b241b0fe69436834fa2b759746b8237e1aafd2e099a38fc64d6'};
                delegateAddresses[3] = {name:'communitypool', address:['10804763751453501689L'], publicKey:'f8fa9e01047c19102133d2af06aab6cc377d5665bede412f04f81bcdc368d00e'};
                delegateAddresses[4] = {name:'corsaro', address:['3026381832248350807L'], publicKey:'ac09bc40c889f688f9158cca1fcfcdf6320f501242e0f7088d52a5077084ccba'};
                delegateAddresses[5] = {name:'dakk', address:['2324852447570841050L'], publicKey:'120d1c3847bd272237ee712ae83de59bbeae127263196fc0f16934bcfa82d8a4'};
                delegateAddresses[6] = {name:'devasive', address:['17438085793668748066L'], publicKey:'a2fc2420262f081d0f6426364301ef40597756e163f6b1fd813eff9b03594125'};
                delegateAddresses[7] = {name:'forrest', address:['11416406146107258985L'], publicKey:'393f73238941510379d930e674e21ca4c00ba30c0877cd3728b5bd5874588671'};
                delegateAddresses[8] = {name:'GDT Pool', address:['1251806989552612274L'], publicKey:'380b952cd92f11257b71cce73f51df5e0a258e54f60bb82bccd2ba8b4dff2ec9'};
                delegateAddresses[9] = {name:'hagie', address:['9012953217698940676L'], publicKey:'27f7950c552f9ffa8c871940167e92257cf90625443a0183aa3f7e05e1f6cb21'};
                delegateAddresses[10] = {name:'hoop', address:['17301165310246494286L'], publicKey:'465085ba003a03d1fed0cfd35b7f3c07927c9db41d32194d273f8fe2fa238faa'};
                delegateAddresses[11] = {name:'index', address:['4318457725971796647L'], publicKey:'d12a6aef4b165b0197adb82d0d544202897b95300ff1fff93c339cf866defb0d'};
                delegateAddresses[12] = {name:'joel', address:['7621048351401864465L'], publicKey:'b6de69ebd1ba0bfe2d37ea6733c64b7e3eb262bee6c9cee05034b0b4465e2678'};
                delegateAddresses[13] = {name:'joo5ty', address:['12654423644480926777L'], publicKey:'3cbef15f5e95cdd1e0d8ba4a3c23cac513a42716a9e540818f42146922674678'};
                delegateAddresses[14] = {name:'kushed.delegate', address:['4225472009432714154L'], publicKey:'f88b86d0a104bda71b2ff4d8234fef4e184ee771a9c2d3a298280790c185231b'};
                delegateAddresses[15] = {name:'Elite + Supporters', address:['9104439788933259758L'], publicKey:'f3cff760a964eaef72d7a6b69086b9aaab0dfbe7e87597f93a391ec47e60c93b'};
                delegateAddresses[16] = {name:'liskit', address:['10310263204519541551L'], publicKey:'e0f1c6cca365cd61bbb01cfb454828a698fa4b7170e85a597dde510567f9dda5'};
                delegateAddresses[17] = {name:'liskpool_com_01', address:['16534946301332208841L'], publicKey:'ec111c8ad482445cfe83d811a7edd1f1d2765079c99d7d958cca1354740b7614'};
                delegateAddresses[18] = {name:'liskpool.top', address:['4034636149257692063L'], publicKey:'1a99630b0ca1642b232888a3119e68b000b6194eced51e7fe3231bbe476f7c10'};
                delegateAddresses[19] = {name:'ntelo', address:['6674162710313805686L'], publicKey:'ca1285393e1848ee41ba0c5e47789e5e0c570a7b51d8e2f7f6db37417b892cf9'};
                delegateAddresses[20] = {name:'ondin', address:['14063591492121689829L'], publicKey:'7f6b150814ab9683db10d4fa20901c555cc789b7d901478819ca7deb630e7d48'};
                delegateAddresses[21] = {name:'philhellmuth', address:['3706844901938026254L'], publicKey:'226e78386cb6e79aa5005c847b806da1d1d7dc995e3b76945d174b87de3b6099'};
                delegateAddresses[22] = {name:'phoenix1969', address:['8668463642800443353L'], publicKey:'ab086300d5d1e366d56ff2b4919ee718d3d6b72a862bafec0f1d42c9812af30b'};
                delegateAddresses[23] = {name:'redsn0w', address:['15383473069235952958L'], publicKey:'b73fa499a7794c111fcd011cdc7dcc426341a28c6c2d6a32b8d7d028dcb8493f'};
                delegateAddresses[24] = {name:'samuray', address:['6876894875185466854L'], publicKey:'fbac76743fad9448ed0b45fb4c97a62f81a358908aa14f6a2c76d2a8dc207141'};
                delegateAddresses[25] = {name:'sgdias', address:['10934306629196932988L'], publicKey:'faf9f863e704f9cf560bc7a5718a25d851666d38195cba3cacd360cd5fa96fd3'};
                delegateAddresses[26] = {name:'Sherwood', address:['17621212458226596154L'], publicKey:'32f20bee855238630b0f791560c02cf93014977b4b25c19ef93cd92220390276'};
                delegateAddresses[27] = {name:'shinekami', address:['16152155423726476379L'], publicKey:'253e674789632f72c98d47a650f1ca5ece0dbb82f591080471129d57ed88fb8a'};
                delegateAddresses[28] = {name:'slasheks', address:['11833062258953092213L'], publicKey:'e41c426b0b79983f6f568f5fd0f0ee018aac76a48b10d06e6cde8c4c62e6f278'};
                delegateAddresses[29] = {name:'splatters', address:['12475282220702488645L'], publicKey:'613e4178a65c1194192eaa29910f0ecca3737f92587dd05d58c6435da41220f6'};
                delegateAddresses[30] = {name:'stellardynamic', address:['7292106026137978431L'], publicKey:'a26710a6bd9de7a328d73446713c1082b4dbf5bd76d2a37caebf212220a9c0f3'};
                delegateAddresses[31] = {name:'techbytes', address:['8325313365683223493L'], publicKey:'33a2abcd627a4cf13c34bd1281519b5d2f8513de434958943576556fae62af80'};
                delegateAddresses[32] = {name:'tembo', address:['2433857930558702776L'], publicKey:'326bff18531703385d4037e5585b001e732c4a68afb8f82efe2b46c27dcf05aa'};
                delegateAddresses[33] = {name:'thepool', address:['10839494368003872009L', '469226551L', '682098L'], publicKey:'b002f58531c074c7190714523eec08c48db8c7cfc0c943097db1a2e82ed87f84'};
                delegateAddresses[34] = {name:'vega', address:['2695109181643074042L'], publicKey:'47226b469031d48f215973a11876c3f03a6d74360b40a55192b2ba9e5a74ede5'};
                delegateAddresses[35] = {name:'vekexasia', address:['9102643396261850794L'], publicKey:'2fc8f8048d2573529b7f37037a49724202a28a0fbee8741702bb4d96c09fcbbf'};
                delegateAddresses[36] = {name:'vi1son', address:['16564709774155482395L'], publicKey:'5386c93dbc76fce1e3a5ae5436ba98bb39e6a0929d038ee2118af54afd45614a'};
                delegateAddresses[37] = {name:'vipertkd', address:['4980451641598555896L'], publicKey:'45ab8f54edff6b802335dc3ea5cd5bc5324e4031c0598a2cdcae79402e4941f8'};
                delegateAddresses[38] = {name:'vrlc92', address:['17059886363552285239L'], publicKey:'eaa5ccb65e635e9ad3ebd98c2b7402b3a7c048fcd300c2d8aed8864f621ee6b2'};                

                var redTotal = 0;
                var greenTotal = 0;

                //Sort transaction by timestamp
                transactionObj.transactions.sort(function(a, b) {
                    return parseInt(a.timestamp) - parseInt(b.timestamp);
                });
                overallMonthlyLabels(transactionObj);

                //arrays for overall chart
                let overallTableData = new Array();

                for (let j = 0; j < delegateAddresses.length; j++) {
                    var delegateChartNames = new Array();
                    var delegateChartTotals = new Array();

                    var total = 0;
                    var total30Days = 0;
                    var total60Days = 0;
                    var total90Days = 0;
                    var total180Days = 0;
                    var total365Days = 0;

                    let ids = new Array();
                    let fees = new Array();
                    let labels = new Array();
                    let indAmounts = new Array();
                    let totalAmounts = new Array();

                    let totalPara = document.createElement('p');
                    totalPara.id = 'para' + j;
                    totalPara.className = 'totalPara';

                    var isVote = false;
                    if(delegateAddresses[j].name === 'Elite + Supporters')
                    {
                        isVote = inElite(delegates);
                    }
                    else
                    {
                        for(var k = 0; k < delegates.delegates.length; k++)
                        {
                            if(delegateAddresses[j].name === 'GDT Pool')
                            {
                                if(delegates.delegates[k].username === 'gdtpool')
                                {
                                    isVote = true;
                                }
                            }
                            else if(delegateAddresses[j].name === 'Sherwood')
                            {
                                if(delegates.delegates[k].username === 'robinhood')
                                {
                                    isVote = true;
                                }
                            }
                            else
                            {
                                if(delegates.delegates[k].username === delegateAddresses[j].name)
                                {
                                    isVote = true;
                                }
                            }
                        }
                    }

                    var neverVoted = true;
                    if(isVote)
                    {
                        neverVoted = false;
                    }
                    else
                    {
                        if (allVotes.indexOf(delegateAddresses[j].publicKey) > -1)
                        {
                            neverVoted = false;
                        }
                    }

                    //Test123
                    let currentTime = Date.now();
                    let days30 = (currentTime / 1000) - (60 * 60 * 24 * 30);
                    let days60 = (currentTime / 1000) - (60 * 60 * 24 * 60);
                    let days90 = (currentTime / 1000) - (60 * 60 * 24 * 90);
                    let days180 = (currentTime / 1000) - (60 * 60 * 24 * 180);
                    let days365 = (currentTime / 1000) - (60 * 60 * 24 * 365);
                    
                    for (var i = transactionObj.transactions.length - 1; i >= 0; i--) {
                        for(let ad = 0; ad < delegateAddresses[j].address.length; ad++)
                        {
                            if(transactionObj.transactions[i].senderId === delegateAddresses[j].address[ad])
                            {
                                //Push data to overallTableData here
                                overallTableData.push({name:delegateAddresses[j].name, id:transactionObj.transactions[i].id, amount:transactionObj.transactions[i].amount / 100000000, fee:transactionObj.transactions[i].fee / 100000000, timestamp:transactionObj.transactions[i].timestamp + 1464109200}); 

                                let time = timeConverter(transactionObj.transactions[i].timestamp + 1464109200);
                                time = time.split(' ');
                                //Add to overallMonthlyChart data
                                addToOverallMonthlyData(delegateAddresses[j].name, time[1] + ' ' + time[2], transactionObj.transactions[i].amount / 100000000);
                                if(isVote)
                                {
                                    addToOverallVotingMonthlyData(delegateAddresses[j].name, time[1] + ' ' + time[2], transactionObj.transactions[i].amount / 100000000);
                                }
                                else
                                {
                                    addToOverallNonVotingMonthlyData(delegateAddresses[j].name, time[1] + ' ' + time[2], transactionObj.transactions[i].amount / 100000000);
                                }

                                labels.unshift(timeConverter(transactionObj.transactions[i].timestamp + 1464109200));
                                indAmounts.unshift(transactionObj.transactions[i].amount / 100000000);
                                ids.unshift(transactionObj.transactions[i].id);
                                fees.unshift(transactionObj.transactions[i].fee / 100000000);

                                delegateChartNames.push(timeConverter(transactionObj.transactions[i].timestamp + 1464109200));
                                delegateChartTotals.push(transactionObj.transactions[i].amount / 100000000);

                                total += transactionObj.transactions[i].amount;
                                if((transactionObj.transactions[i].timestamp + 1464109200) >= days30) //Within last 30 days
                                {
                                    total30Days += transactionObj.transactions[i].amount;
                                }
                                if((transactionObj.transactions[i].timestamp + 1464109200) >= days60) //Within last 60 days
                                {
                                    total60Days += transactionObj.transactions[i].amount;
                                }
                                if((transactionObj.transactions[i].timestamp + 1464109200) >= days90) //Within last 90 days
                                {
                                    total90Days += transactionObj.transactions[i].amount;
                                }
                                if((transactionObj.transactions[i].timestamp + 1464109200) >= days180) //Within last 180 days
                                {
                                    total180Days += transactionObj.transactions[i].amount;
                                }
                                if((transactionObj.transactions[i].timestamp + 1464109200) >= days365) //Within last 365 days
                                {
                                    total365Days += transactionObj.transactions[i].amount;
                                }
                            }
                        }
                    }

                    //Moved here
                    if(indAmounts.length > 0)
                    {
                        totalAmounts[0] = indAmounts[0];
                    
                        for(let q = 1; q < indAmounts.length; q++)
                        {
                            let temptotal = 0;
                            for(let u = 0; u <= q; u++)
                            {
                                temptotal += indAmounts[u];
                                //correct roundoff error
                                temptotal = Math.round(temptotal*100000000)/100000000;
                            }
                            totalAmounts.push(temptotal);
                        }
                    }

                    if(!neverVoted)
                    {
                        let delegateButton = document.createElement('button');
                        let buttonPlacement = '';
                        delegateButton.id = 'button' + j;
                        delegateButton.className = 'buttonClass button delegateButton';
                        delegateButton.textContent = delegateAddresses[j].name + ' - ' + (total / 100000000) + ' LSK';
                        if(isVote)
                        {
                            //delegateButton.style.backgroundColor = "#A4FCB1";
                            buttonPlacement = 'votingDelegates';
                            votingDelegates.push(delegateAddresses[j].name);
                        }
                        else
                        {
                            //delegateButton.style.backgroundColor = "#FCA8A4";
                            buttonPlacement = 'nonvotingDelegates';
                        }
                        delegateButton.onclick = function () {
                            ResetButtonBG('delegateButton');
                            ActivateButtonBG('button' + j);
                            //chart
                            var chart_labels = labels;
                            var ind_dataset = indAmounts;
                            var total_dataset = totalAmounts;
                            var data = delegate_chart.config.data;
                            data.datasets[0].data = total_dataset;
                            data.datasets[1].data = ind_dataset;
                            data.labels = chart_labels;
                            delegate_chart.update();
                            document.getElementById('delegateChartDiv').style.display = "block";
                            document.getElementById('delegateTable').style.display = "block";
                            document.getElementById('overallChartDiv').style.display = "none";
                            document.getElementById('overallMonthlyChartDiv').style.display = "none";
                            document.getElementById('overallTableDiv').style.display = "none";

                            //table
                            var delegateTable = document.getElementById('delegateTable');
                            //Erase table
                            delegateTable.innerHTML = "";

                            //Add header rows
                            var trheader1 = delegateTable.insertRow();
                            var tdheader1 = trheader1.insertCell();
                            tdheader1.appendChild(document.createTextNode('tx id'));
                            var tdheader2 = trheader1.insertCell();
                            tdheader2.appendChild(document.createTextNode('Amount'));
                            var tdheader3 = trheader1.insertCell();
                            tdheader3.appendChild(document.createTextNode('Fee'));
                            var tdheader4 = trheader1.insertCell();
                            tdheader4.appendChild(document.createTextNode('Date/Time'));

                            let totalAmount = 0;
                            let totalFees = 0;
                            //Add data rows
                            for(var p = ids.length - 1; p >= 0; p--)
                            {
                                var trheader2 = delegateTable.insertRow();
                                var tdheader5 = trheader2.insertCell();
                                
                                var a = document.createElement('a');
                                var linkText = document.createTextNode(ids[p]);
                                a.appendChild(linkText);
                                a.title = 'https://explorer.lisk.io/tx/' + ids[p];
                                a.href = 'https://explorer.lisk.io/tx/' + ids[p];
                                tdheader5.appendChild(a);
                                var tdheader6 = trheader2.insertCell();
                                tdheader6.appendChild(document.createTextNode(indAmounts[p]));
                                totalAmount += indAmounts[p];

                                var tdheader7 = trheader2.insertCell();
                                tdheader7.appendChild(document.createTextNode(fees[p]));
                                totalFees += fees[p];

                                var tdheader8 = trheader2.insertCell();
                                tdheader8.appendChild(document.createTextNode(labels[p]));
                            }

                            var trheader3 = delegateTable.insertRow();
                            var tdheader9 = trheader3.insertCell();
                            tdheader9.appendChild(document.createTextNode('Total'));

                            var tdheader10 = trheader3.insertCell();
                            //Math.round(num*100)/100
                            //tdheader10.appendChild(document.createTextNode(totalAmount.toFixed(8)));
                            tdheader10.appendChild(document.createTextNode(Math.round(totalAmount*100000000)/100000000));

                            var tdheader11 = trheader3.insertCell();
                            //tdheader11.appendChild(document.createTextNode(totalFees.toFixed(8)));
                            tdheader11.appendChild(document.createTextNode(Math.round(totalFees*100000000)/100000000));
                        };

                        //var buttons = document.getElementById('delegateButtons');
                        var buttons = document.getElementById(buttonPlacement);
                        buttons.appendChild(delegateButton);

                        //123
                        overallChartNames.push(delegateAddresses[j].name);
                        overallChartTotals.push(total / 100000000);
                        overallChartNames30Days.push(delegateAddresses[j].name);
                        overallChartTotal30Days.push(total30Days / 100000000);
                        overallChartNames60Days.push(delegateAddresses[j].name);
                        overallChartTotal60Days.push(total60Days / 100000000);
                        overallChartNames90Days.push(delegateAddresses[j].name);
                        overallChartTotal90Days.push(total90Days / 100000000);
                        overallChartNames180Days.push(delegateAddresses[j].name);
                        overallChartTotal180Days.push(total180Days / 100000000);
                        overallChartNames365Days.push(delegateAddresses[j].name);
                        overallChartTotal365Days.push(total365Days / 100000000);
                        if(isVote)
                        {
                            overallVotingChartNames.push(delegateAddresses[j].name);
                            overallVotingChartTotals.push(total / 100000000);
                            overallVotingChartNames30Days.push(delegateAddresses[j].name);
                            overallVotingChartTotal30Days.push(total30Days / 100000000);
                            overallVotingChartNames60Days.push(delegateAddresses[j].name);
                            overallVotingChartTotal60Days.push(total60Days / 100000000);
                            overallVotingChartNames90Days.push(delegateAddresses[j].name);
                            overallVotingChartTotal90Days.push(total90Days / 100000000);
                            overallVotingChartNames180Days.push(delegateAddresses[j].name);
                            overallVotingChartTotal180Days.push(total180Days / 100000000);
                            overallVotingChartNames365Days.push(delegateAddresses[j].name);
                            overallVotingChartTotal365Days.push(total365Days / 100000000);
                        }
                        else
                        {
                            overallNonVotingChartNames.push(delegateAddresses[j].name);
                            overallNonVotingChartTotals.push(total / 100000000);
                            overallNonVotingChartNames30Days.push(delegateAddresses[j].name);
                            overallNonVotingChartTotal30Days.push(total30Days / 100000000);
                            overallNonVotingChartNames60Days.push(delegateAddresses[j].name);
                            overallNonVotingChartTotal60Days.push(total60Days / 100000000);
                            overallNonVotingChartNames90Days.push(delegateAddresses[j].name);
                            overallNonVotingChartTotal90Days.push(total90Days / 100000000);
                            overallNonVotingChartNames180Days.push(delegateAddresses[j].name);
                            overallNonVotingChartTotal180Days.push(total180Days / 100000000);
                            overallNonVotingChartNames365Days.push(delegateAddresses[j].name);
                            overallNonVotingChartTotal365Days.push(total365Days / 100000000);
                        }
                        var totalText = document.createTextNode(delegateAddresses[j].name + ': ' + (total / 100000000));
                        totalPara.appendChild(totalText);
                    }

                    if(!neverVoted)
                    {
                        if(isVote)
                        {
                            greenTotal += total;
                        }
                        else
                        {
                            redTotal += total;
                        }
                    }
                } //end 1234

                calculateMonthlyRanges();
                calculateOverallData();
                
                ResetButtonBG('button');
                
                ActivateButtonBG('OverallAllMonthlyChart');
                ActivateButtonBG('OverallStackedMonthlyChart');
                ActivateButtonBG('OverallBarMonthlyChart');
                ActivateButtonBG('OverallRangeAll');
                ActivateButtonBG('MonthlyBothVoting');
                ActivateButtonBG('OverallAllChart');
                ActivateButtonBG('OverallHBarChart');
                ActivateButtonBG('overallMonthlyBtn');
                ActivateButtonBG('OverallRangeAll2');
                ActivateButtonBG('OverallBothVoting');

                //Overall Table
                //Sort data by timestamp
                function ByTimestamp(a,b) {
                    if (a.timestamp < b.timestamp)
                      return 1;
                    if (a.timestamp > b.timestamp)
                      return -1;
                    return 0;
                  }
                  
                  overallTableData.sort(ByTimestamp);

                //Table headers
                //table
                var overallTable = document.getElementById('overallTable');
                //Erase table
                overallTable.innerHTML = "";

                //Add header rows
                var trheader1 = overallTable.insertRow();
                var tdheader0 = trheader1.insertCell();
                tdheader0.appendChild(document.createTextNode('Delegate'));
                var tdheader1 = trheader1.insertCell();
                tdheader1.appendChild(document.createTextNode('tx id'));
                var tdheader2 = trheader1.insertCell();
                tdheader2.appendChild(document.createTextNode('Amount'));
                var tdheader3 = trheader1.insertCell();
                tdheader3.appendChild(document.createTextNode('Fee'));
                var tdheader4 = trheader1.insertCell();
                tdheader4.appendChild(document.createTextNode('Date/Time'));

                //Table data Loop
                //Add data rows
                for(var d = 0; d < overallTableData.length; d++)
                {
                    var trheader2 = overallTable.insertRow();

                    var tdheader5 = trheader2.insertCell();
                    tdheader5.appendChild(document.createTextNode(overallTableData[d].name));

                    var tdheader6 = trheader2.insertCell();
                    
                    var a = document.createElement('a');
                    var linkText = document.createTextNode(overallTableData[d].id);
                    a.appendChild(linkText);
                    a.title = 'https://explorer.lisk.io/tx/' + overallTableData[d].id;
                    a.href = 'https://explorer.lisk.io/tx/' + overallTableData[d].id;
                    tdheader6.appendChild(a);
                    var tdheader7 = trheader2.insertCell();
                    tdheader7.appendChild(document.createTextNode(overallTableData[d].amount));
                    //totalAmount += indAmounts[p];

                    var tdheader8 = trheader2.insertCell();
                    tdheader8.appendChild(document.createTextNode(overallTableData[d].fee));
                    //totalFees += fees[p];

                    var tdheader9 = trheader2.insertCell();
                    tdheader9.appendChild(document.createTextNode(timeConverter(overallTableData[d].timestamp)));
                    
                    //Limit to 20 transactions
                    if(d >= 19)
                    {
                        break;
                    }
                }

                createChart(overallChartNames, overallChartTotals, "overallChart", 'horizontalBar');
                var totalPayoutText = document.createTextNode('Total from all pools: ' + ((greenTotal + redTotal) / 100000000).toFixed(8));
                alldelegates.appendChild(totalPayoutText);
                var greenTotalText = document.createTextNode('Total from pools voting for: ' + (greenTotal / 100000000).toFixed(8));
                voted.appendChild(greenTotalText);
                var redTotalText = document.createTextNode('Total from pools NOT voting for: ' + (redTotal / 100000000).toFixed(8));
                notvoted.appendChild(redTotalText);
                document.getElementById('totalPayoutInfo').style.display = "block";
                //firstChartLoad();
                overallMonthlyChart();
            }, 500);
        });
    });
}

function firstChartLoad(){

    /*if(typeof monthlyDataContainer !== 'undefined')
    {
        OverallAllMonthlyChart();
    }
    else
    {
        setTimeout(function () {
            firstChartLoad();
        }, 400);
    }*/
}

function createChart(names, liskData, elementName, chartType){
    var ctx2 = document.getElementById(elementName);
    //var myChart = new Chart(ctx2, {
    overall_chart = new Chart(ctx2, {
    type: chartType,
    data: {
        labels: names,
        datasets: [{
            label: 'Lisk Paid Out',
            data: liskData,
            backgroundColor: 
                overallMonthlyColors
            ,
            //borderColor: [
            //    'rgba(255, 159, 64, 1)'
            //],
            borderWidth: 1
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false,
            labels:{boxWidth:10}
         },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
                }],
            yAxes: [{
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
            }]
        },
        tooltips: {
            //enabled: false,
            //mode: 'index',
            //position: 'nearest',
            //custom: customTooltips
            //yAlign: 'bottom'
        }
    }
}); 
}

function overallChart(){
    ResetButtonBG('DataTypes');
    ActivateButtonBG("overallBtn");
    document.getElementById('delegateChartDiv').style.display = "none";
    document.getElementById('delegateTable').style.display = "none";
    document.getElementById('overallChartDiv').style.display = "block";
    document.getElementById('overallMonthlyChartDiv').style.display = "none";
    document.getElementById('overallTableDiv').style.display = "block";

    document.getElementById('MonthlyButtons').style.display = "none";
    document.getElementById('OverallButtons').style.display = "block";
    document.getElementById('delegateButtons').style.display = "none";
}
//var myOverallMonthlyChart;
var isFirst = true;
function overallMonthlyChart(){
    ResetButtonBG('DataTypes');
    ActivateButtonBG("overallMonthlyBtn");
    document.getElementById('delegateTable').style.display = "none";
    document.getElementById('delegateChartDiv').style.display = "none";
    document.getElementById('overallChartDiv').style.display = "none";
    document.getElementById('overallMonthlyChartDiv').style.display = "block";
    document.getElementById('overallTableDiv').style.display = "block";

    document.getElementById('MonthlyButtons').style.display = "block";
    document.getElementById('OverallButtons').style.display = "none";
    document.getElementById('delegateButtons').style.display = "none";

    
    var ctx3 = document.getElementById('overallMonthlyChart');
    var overall_monthly_chart = new Chart(ctx3, {
        type: chartType,
        data: overallMonthlyData,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                labels:{boxWidth:10}
            },
            tooltips: {
                enabled: false,
                mode: 'index',
                //intersect: false,
                //yAlign: 'bottom',
                position: 'nearest',
                custom: customTooltips2,
            },
            scales: {
                xAxes: [{
                    stacked: isStacked,
                    ticks: {
                    autoSkip: false
                    }
                    }],
                yAxes: [{
                    stacked: isStacked,
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
monthlyDataContainer = overall_monthly_chart;
}

//Old Table Tooltip
//Keep for legacy sakes
/*
var customTooltips = function(tooltip) {
    // Tooltip Element

    var tooltipEl = document.getElementById('chartjs-tooltip');
    if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chartjs-tooltip';
        tooltipEl.innerHTML = "<table></table>"
        this._chart.canvas.parentNode.appendChild(tooltipEl);
    }
    // Hide if no tooltip
    if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }
    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltip.yAlign) {
        tooltipEl.classList.add(tooltip.yAlign);
    } else {
        tooltipEl.classList.add('no-transform');
    }
    function getBody(bodyItem) {
        return bodyItem.lines;
    }
    // Set Text
    if (tooltip.body) {
        var titleLines = tooltip.title || [];
        var bodyLines = tooltip.body.map(getBody);
        var innerHtml = '<thead>';
        titleLines.forEach(function(title) {
            innerHtml += '<tr><th>' + title + '</th></tr>';
        });
        innerHtml += '</thead><tbody>';
        let totalMonth = 0;
        if(tooltip.labelColors[0].backgroundColor !== 'rgb(242, 128, 115)')
        {
            bodyLines.forEach(function(body, i) {
                var colors = tooltip.labelColors[i];
                //var style = 'background:' + colors.backgroundColor;
                //style += '; border-color:' + colors.borderColor;
                var style = 'border-color:' + colors.borderColor;
                style += '; border-width: 2px'; 
                var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                innerHtml += "<tr><td bgcolor='" + tooltip.labelColors[i].backgroundColor + "'>" + span + body + '</td></tr>';
                let temp = body[0].split(": ");
                totalMonth += Number(temp[1]);
            });
            //correct rounding error
            totalMonth = Math.round(totalMonth*100000000)/100000000;
            innerHtml += "<tr><td>Total: " + totalMonth + '</td></tr>';
        }
        else //This else is only for an error caused by the tooltip rendering incorrectly. A fix will need to found in the future
        {
            bodyLines.forEach(function(body, i) {
                var colors = tooltip.labelColors[i];
                //var style = 'background:' + colors.backgroundColor;
                //style += '; border-color:' + colors.borderColor;
                var style = 'border-color:' + colors.borderColor;
                style += '; border-width: 2px'; 
                var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                innerHtml += "<tr><td>" + span + body + '</td></tr>';
                let temp = body[0].split(": ");
                totalMonth += Number(temp[1]);
            });
            //correct rounding error
            totalMonth = Math.round(totalMonth*100000000)/100000000;
            innerHtml += "<tr><td>Total: " + totalMonth + '</td></tr>';
        }
        innerHtml += '</tbody>';
        var tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHTML = innerHtml;
    }
    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft;
    // Display, position, and set styles for font
    //tooltipEl.style.opacity = 1;
    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
    tooltipEl.style.top = positionY + tooltip.caretY + 'px';
    tooltipEl.style.fontFamily = tooltip._fontFamily;
    tooltipEl.style.fontSize = tooltip.fontSize;
    tooltipEl.style.fontStyle = tooltip._fontStyle;
    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
};
*/

var customTooltips2 = function(tooltip) {
    // Tooltip Element
    var tooltipEl = document.getElementById('chartjs-tooltip2');
    let tooltipFontColor = 'Black';
    let tooltipFontSize = '10px';
    let tooltipFontFamily = 'verdana';
    let tooltipStyle = "'color:"+tooltipFontColor+";font-size:"+tooltipFontSize+";font-family:"+tooltipFontFamily+"'";

    if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chartjs-tooltip2';
        var tooltipID = 'MonthlyCustomTooltip';
        var tooltipClass = 'CustomTooltips';
        //tooltipEl.innerHTML = "<table style=" + tooltipStyle + "></table>";
        tooltipEl.innerHTML = '<table id="' + tooltipID + '" class="' + tooltipClass + '"></table>';
        this._chart.canvas.parentNode.appendChild(tooltipEl);
    }

    // Hide if no tooltip
    if (tooltip.opacity === 0) {
        tooltipEl.style.opacity = 0;
        return;
    }

    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltip.yAlign) {
        tooltipEl.classList.add(tooltip.yAlign);
    } else {
        tooltipEl.classList.add('no-transform');
    }

    function getBody(bodyItem) {
        return bodyItem.lines;
    }

    // Set Text
    if (tooltip.body) {
        var titleLines = tooltip.title || [];
        var bodyLines = tooltip.body.map(getBody);

        var innerHtml = '<thead>';
        let totalMonth = 0;
        titleLines.forEach(function(title) {
            //innerHtml += '<tr><th colspan="2" style=color:' + tooltipFontColor + '>' + title + '</th></tr>';
            innerHtml += '<tr><th class="CustomTooltipsTD" colspan="2">' + title + '</th></tr>';
        });
        innerHtml += '</thead><tbody>';

        //Order tooltip by LSK amounts, not names 
        for(let j=0; j<bodyLines.length - 1; j++)
        {
            for(let i=0; i<bodyLines.length - 1; i++)
            {
                let temp = (bodyLines[i] + '').split(": ");
                let temp2 = (bodyLines[i + 1] + '').split(": ");

                if(Number(temp[1]) < Number(temp2[1]))
                {
                    let temp3 = bodyLines[i];
                    bodyLines[i] = bodyLines[i + 1];
                    bodyLines[i + 1] = temp3;

                    temp3 = tooltip.labelColors[i];
                    tooltip.labelColors[i] = tooltip.labelColors[i + 1];
                    tooltip.labelColors[i + 1] = temp3;
                }
            }
        }

        for(let i=0; i<bodyLines.length/2;i++)
        {
            var colors = tooltip.labelColors[i];
            var style = 'background:' + colors.backgroundColor;
            style += '; border-color:' + colors.borderColor;
            style += '; border-width: 2px'; 
            var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
            let temp = (bodyLines[i] + '').split(": ");
            temp[1] = Math.round(Number(temp[1]) *10000) / 10000;
            totalMonth += Number(temp[1]);

            if(i+(Math.ceil(bodyLines.length/2)) < bodyLines.length)
            {
                var colors2 = tooltip.labelColors[i+(Math.ceil(bodyLines.length/2))];
                var style2 = 'background:' + colors2.backgroundColor;
                style2 += '; border-color:' + colors2.borderColor;
                style2 += '; border-width: 2px'; 
                var span2 = '<span class="chartjs-tooltip-key" style="' + style2 + '"></span>';
                let temp2 = (bodyLines[i+(Math.ceil(bodyLines.length/2))] + '').split(": ");
                temp2[1] = Math.round(Number(temp2[1]) *10000) / 10000;
                totalMonth += Number(temp2[1]);

                //innerHtml += '<tr><td class="CustomTooltipsTD">' + span + bodyLines[i] + '</td><td class="CustomTooltipsTD">' + span2 + bodyLines[i+(Math.ceil(bodyLines.length/2))] + '</td></tr>';
                innerHtml += '<tr><td class="CustomTooltipsTD">' + span + temp[0] + ': ' + temp[1] + '</td><td class="CustomTooltipsTD">' + span2 + temp2[0] + ': ' + temp2[1] + '</td></tr>';
            }
            else
            {
                //innerHtml += '<tr><td class="CustomTooltipsTD">' + span + bodyLines[i] + '</td></tr>';
                innerHtml += '<tr><td class="CustomTooltipsTD">' + span + temp[0] + ': ' + temp[1] + '</td></tr>';
            }
            
        }
        //end test

        //correct rounding error
        totalMonth = Math.round(totalMonth*100000000)/100000000;
        innerHtml += '<tr><td class="CustomTooltipsTD"><b>Total: ' + totalMonth + '</b></td></tr>';
        innerHtml += '</tbody>';

        var tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHTML = innerHtml;
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft;

    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1;
    tooltipEl.style.left = positionX + 50 + tooltip.caretX + 'px';
    //tooltipEl.style.top = positionY + tooltip.caretY + 'px';
    tooltipEl.style.top = 50 + 'px';
    tooltipEl.style.fontFamily = tooltip._fontFamily;
    tooltipEl.style.fontSize = tooltip.fontSize;
    tooltipEl.style.fontStyle = tooltip._fontStyle;
    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
};

function calculateOverallData(){
    //4321
    for(let i = 0; i < overallChartNames.length; i++)
    {
        if(overallChartNames[i] !== 'GDT Pool' && overallChartNames[i] !== 'Elite + Supporters' && overallChartNames[i] !== 'Sherwood')
        {
            overallIndChartNames.push(overallChartNames[i]);
            overallIndChartTotals.push(overallChartTotals[i]);
        }
        else
        {
            overallDelChartNames.push(overallChartNames[i]);
            overallDelChartTotals.push(overallChartTotals[i]);

            overallDelPChartNames.push(overallChartNames[i]);

            if(overallChartNames[i] === 'Elite + Supporters' || overallChartNames[i] === 'Sherwood')
            {
                
                overallDelPChartTotals.push(overallChartTotals[i]);
            }
            else if(overallChartNames[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotals[i];
               
                for(let d = 0; d < overallChartNames.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames[d]) > -1)
                    {
                        GDTTotal += overallChartTotals[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotals.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallChartNames30Days.length; i++)
    {
        if(overallChartNames30Days[i] !== 'GDT Pool' && overallChartNames30Days[i] !== 'Elite + Supporters' && overallChartNames30Days[i] !== 'Sherwood')
        {
            overallIndChartNames30Days.push(overallChartNames30Days[i]);
            overallIndChartTotal30Days.push(overallChartTotal30Days[i]);
        }
        else
        {
            overallDelChartNames30Days.push(overallChartNames30Days[i]);
            overallDelChartTotal30Days.push(overallChartTotal30Days[i]);

            overallDelPChartNames30Days.push(overallChartNames30Days[i]);

            if(overallChartNames30Days[i] === 'Elite + Supporters' || overallChartNames30Days[i] === 'Sherwood')
            {
                
                overallDelPChartTotal30Days.push(overallChartTotal30Days[i]);
            }
            else if(overallChartNames30Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotal30Days[i];
               
                for(let d = 0; d < overallChartNames30Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames30Days[d]) > -1)
                    {
                        GDTTotal += overallChartTotal30Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotal30Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallChartNames60Days.length; i++)
    {
        if(overallChartNames60Days[i] !== 'GDT Pool' && overallChartNames60Days[i] !== 'Elite + Supporters' && overallChartNames60Days[i] !== 'Sherwood')
        {
            overallIndChartNames60Days.push(overallChartNames60Days[i]);
            overallIndChartTotal60Days.push(overallChartTotal60Days[i]);
        }
        else
        {
            overallDelChartNames60Days.push(overallChartNames60Days[i]);
            overallDelChartTotal60Days.push(overallChartTotal60Days[i]);

            overallDelPChartNames60Days.push(overallChartNames60Days[i]);

            if(overallChartNames60Days[i] === 'Elite + Supporters' || overallChartNames60Days[i] === 'Sherwood')
            {
                
                overallDelPChartTotal60Days.push(overallChartTotal60Days[i]);
            }
            else if(overallChartNames60Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotal60Days[i];
               
                for(let d = 0; d < overallChartNames60Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames60Days[d]) > -1)
                    {
                        GDTTotal += overallChartTotal60Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotal60Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallChartNames90Days.length; i++)
    {
        if(overallChartNames90Days[i] !== 'GDT Pool' && overallChartNames90Days[i] !== 'Elite + Supporters' && overallChartNames90Days[i] !== 'Sherwood')
        {
            overallIndChartNames90Days.push(overallChartNames90Days[i]);
            overallIndChartTotal90Days.push(overallChartTotal90Days[i]);
        }
        else
        {
            overallDelChartNames90Days.push(overallChartNames90Days[i]);
            overallDelChartTotal90Days.push(overallChartTotal90Days[i]);

            overallDelPChartNames90Days.push(overallChartNames90Days[i]);

            if(overallChartNames90Days[i] === 'Elite + Supporters' || overallChartNames90Days[i] === 'Sherwood')
            {
                
                overallDelPChartTotal90Days.push(overallChartTotal90Days[i]);
            }
            else if(overallChartNames90Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotal90Days[i];
               
                for(let d = 0; d < overallChartNames90Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames90Days[d]) > -1)
                    {
                        GDTTotal += overallChartTotal90Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotal90Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallChartNames180Days.length; i++)
    {
        if(overallChartNames180Days[i] !== 'GDT Pool' && overallChartNames180Days[i] !== 'Elite + Supporters' && overallChartNames180Days[i] !== 'Sherwood')
        {
            overallIndChartNames180Days.push(overallChartNames180Days[i]);
            overallIndChartTotal180Days.push(overallChartTotal180Days[i]);
        }
        else
        {
            overallDelChartNames180Days.push(overallChartNames180Days[i]);
            overallDelChartTotal180Days.push(overallChartTotal180Days[i]);

            overallDelPChartNames180Days.push(overallChartNames180Days[i]);

            if(overallChartNames180Days[i] === 'Elite + Supporters' || overallChartNames180Days[i] === 'Sherwood')
            {
                
                overallDelPChartTotal180Days.push(overallChartTotal180Days[i]);
            }
            else if(overallChartNames180Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotal180Days[i];
               
                for(let d = 0; d < overallChartNames180Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames180Days[d]) > -1)
                    {
                        GDTTotal += overallChartTotal180Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotal180Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallChartNames365Days.length; i++)
    {
        if(overallChartNames365Days[i] !== 'GDT Pool' && overallChartNames365Days[i] !== 'Elite + Supporters' && overallChartNames365Days[i] !== 'Sherwood')
        {
            overallIndChartNames365Days.push(overallChartNames365Days[i]);
            overallIndChartTotal365Days.push(overallChartTotal365Days[i]);
        }
        else
        {
            overallDelChartNames365Days.push(overallChartNames365Days[i]);
            overallDelChartTotal365Days.push(overallChartTotal365Days[i]);

            overallDelPChartNames365Days.push(overallChartNames365Days[i]);

            if(overallChartNames365Days[i] === 'Elite + Supporters' || overallChartNames365Days[i] === 'Sherwood')
            {
                
                overallDelPChartTotal365Days.push(overallChartTotal365Days[i]);
            }
            else if(overallChartNames365Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallChartTotal365Days[i];
               
                for(let d = 0; d < overallChartNames365Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallChartNames365Days[d]) > -1)
                    {
                        GDTTotal += overallChartTotal365Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallDelPChartTotal365Days.push(GDTTotal);
            }
        }
    }
    //Voting
    for(let i = 0; i < overallVotingChartNames.length; i++)
    {
        if(overallVotingChartNames[i] !== 'GDT Pool' && overallVotingChartNames[i] !== 'Elite + Supporters' && overallVotingChartNames[i] !== 'Sherwood')
        {
            overallVotingIndChartNames.push(overallVotingChartNames[i]);
            overallVotingIndChartTotals.push(overallVotingChartTotals[i]);
        }
        else
        {
            overallVotingDelChartNames.push(overallVotingChartNames[i]);
            overallVotingDelChartTotals.push(overallVotingChartTotals[i]);

            overallVotingDelPChartNames.push(overallVotingChartNames[i]);

            if(overallVotingChartNames[i] === 'Elite + Supporters' || overallVotingChartNames[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotals.push(overallVotingChartTotals[i]);
            }
            else if(overallVotingChartNames[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotals[i];
               
                for(let d = 0; d < overallVotingChartNames.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotals[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotals.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallVotingChartNames30Days.length; i++)
    {
        if(overallVotingChartNames30Days[i] !== 'GDT Pool' && overallVotingChartNames30Days[i] !== 'Elite + Supporters' && overallVotingChartNames30Days[i] !== 'Sherwood')
        {
            overallVotingIndChartNames30Days.push(overallVotingChartNames30Days[i]);
            overallVotingIndChartTotal30Days.push(overallVotingChartTotal30Days[i]);
        }
        else
        {
            overallVotingDelChartNames30Days.push(overallVotingChartNames30Days[i]);
            overallVotingDelChartTotal30Days.push(overallVotingChartTotal30Days[i]);

            overallVotingDelPChartNames30Days.push(overallVotingChartNames30Days[i]);

            if(overallVotingChartNames30Days[i] === 'Elite + Supporters' || overallVotingChartNames30Days[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotal30Days.push(overallVotingChartTotal30Days[i]);
            }
            else if(overallVotingChartNames30Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotal30Days[i];
               
                for(let d = 0; d < overallVotingChartNames30Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames30Days[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotal30Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotal30Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallVotingChartNames60Days.length; i++)
    {
        if(overallVotingChartNames60Days[i] !== 'GDT Pool' && overallVotingChartNames60Days[i] !== 'Elite + Supporters' && overallVotingChartNames60Days[i] !== 'Sherwood')
        {
            overallVotingIndChartNames60Days.push(overallVotingChartNames60Days[i]);
            overallVotingIndChartTotal60Days.push(overallVotingChartTotal60Days[i]);
        }
        else
        {
            overallVotingDelChartNames60Days.push(overallVotingChartNames60Days[i]);
            overallVotingDelChartTotal60Days.push(overallVotingChartTotal60Days[i]);

            overallVotingDelPChartNames60Days.push(overallVotingChartNames60Days[i]);

            if(overallVotingChartNames60Days[i] === 'Elite + Supporters' || overallVotingChartNames60Days[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotal60Days.push(overallVotingChartTotal60Days[i]);
            }
            else if(overallVotingChartNames60Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotal60Days[i];
               
                for(let d = 0; d < overallVotingChartNames60Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames60Days[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotal60Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotal60Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallVotingChartNames90Days.length; i++)
    {
        if(overallVotingChartNames90Days[i] !== 'GDT Pool' && overallVotingChartNames90Days[i] !== 'Elite + Supporters' && overallVotingChartNames90Days[i] !== 'Sherwood')
        {
            overallVotingIndChartNames90Days.push(overallVotingChartNames90Days[i]);
            overallVotingIndChartTotal90Days.push(overallVotingChartTotal90Days[i]);
        }
        else
        {
            overallVotingDelChartNames90Days.push(overallVotingChartNames90Days[i]);
            overallVotingDelChartTotal90Days.push(overallVotingChartTotal90Days[i]);

            overallVotingDelPChartNames90Days.push(overallVotingChartNames90Days[i]);

            if(overallVotingChartNames90Days[i] === 'Elite + Supporters' || overallVotingChartNames90Days[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotal90Days.push(overallVotingChartTotal90Days[i]);
            }
            else if(overallVotingChartNames90Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotal90Days[i];
               
                for(let d = 0; d < overallVotingChartNames90Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames90Days[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotal90Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotal90Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallVotingChartNames180Days.length; i++)
    {
        if(overallVotingChartNames180Days[i] !== 'GDT Pool' && overallVotingChartNames180Days[i] !== 'Elite + Supporters' && overallVotingChartNames180Days[i] !== 'Sherwood')
        {
            overallVotingIndChartNames180Days.push(overallVotingChartNames180Days[i]);
            overallVotingIndChartTotal180Days.push(overallVotingChartTotal180Days[i]);
        }
        else
        {
            overallVotingDelChartNames180Days.push(overallVotingChartNames180Days[i]);
            overallVotingDelChartTotal180Days.push(overallVotingChartTotal180Days[i]);

            overallVotingDelPChartNames180Days.push(overallVotingChartNames180Days[i]);

            if(overallVotingChartNames180Days[i] === 'Elite + Supporters' || overallVotingChartNames180Days[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotal180Days.push(overallVotingChartTotal180Days[i]);
            }
            else if(overallVotingChartNames180Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotal180Days[i];
               
                for(let d = 0; d < overallVotingChartNames180Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames180Days[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotal180Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotal180Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallVotingChartNames365Days.length; i++)
    {
        if(overallVotingChartNames365Days[i] !== 'GDT Pool' && overallVotingChartNames365Days[i] !== 'Elite + Supporters' && overallVotingChartNames365Days[i] !== 'Sherwood')
        {
            overallVotingIndChartNames365Days.push(overallVotingChartNames365Days[i]);
            overallVotingIndChartTotal365Days.push(overallVotingChartTotal365Days[i]);
        }
        else
        {
            overallVotingDelChartNames365Days.push(overallVotingChartNames365Days[i]);
            overallVotingDelChartTotal365Days.push(overallVotingChartTotal365Days[i]);

            overallVotingDelPChartNames365Days.push(overallVotingChartNames365Days[i]);

            if(overallVotingChartNames365Days[i] === 'Elite + Supporters' || overallVotingChartNames365Days[i] === 'Sherwood')
            {
                
                overallVotingDelPChartTotal365Days.push(overallVotingChartTotal365Days[i]);
            }
            else if(overallVotingChartNames365Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallVotingChartTotal365Days[i];
               
                for(let d = 0; d < overallVotingChartNames365Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallVotingChartNames365Days[d]) > -1)
                    {
                        GDTTotal += overallVotingChartTotal365Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallVotingDelPChartTotal365Days.push(GDTTotal);
            }
        }
    }
    //NonVoting
    for(let i = 0; i < overallNonVotingChartNames.length; i++)
    {
        if(overallNonVotingChartNames[i] !== 'GDT Pool' && overallNonVotingChartNames[i] !== 'Elite + Supporters' && overallNonVotingChartNames[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames.push(overallNonVotingChartNames[i]);
            overallNonVotingIndChartTotals.push(overallNonVotingChartTotals[i]);
        }
        else
        {
            overallNonVotingDelChartNames.push(overallNonVotingChartNames[i]);
            overallNonVotingDelChartTotals.push(overallNonVotingChartTotals[i]);

            overallNonVotingDelPChartNames.push(overallNonVotingChartNames[i]);

            if(overallNonVotingChartNames[i] === 'Elite + Supporters' || overallNonVotingChartNames[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotals.push(overallNonVotingChartTotals[i]);
            }
            else if(overallNonVotingChartNames[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotals[i];
               
                for(let d = 0; d < overallNonVotingChartNames.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotals[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotals.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallNonVotingChartNames30Days.length; i++)
    {
        if(overallNonVotingChartNames30Days[i] !== 'GDT Pool' && overallNonVotingChartNames30Days[i] !== 'Elite + Supporters' && overallNonVotingChartNames30Days[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames30Days.push(overallNonVotingChartNames30Days[i]);
            overallNonVotingIndChartTotal30Days.push(overallNonVotingChartTotal30Days[i]);
        }
        else
        {
            overallNonVotingDelChartNames30Days.push(overallNonVotingChartNames30Days[i]);
            overallNonVotingDelChartTotal30Days.push(overallNonVotingChartTotal30Days[i]);

            overallNonVotingDelPChartNames30Days.push(overallNonVotingChartNames30Days[i]);

            if(overallNonVotingChartNames30Days[i] === 'Elite + Supporters' || overallNonVotingChartNames30Days[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotal30Days.push(overallNonVotingChartTotal30Days[i]);
            }
            else if(overallNonVotingChartNames30Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotal30Days[i];
               
                for(let d = 0; d < overallNonVotingChartNames30Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames30Days[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotal30Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotal30Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallNonVotingChartNames60Days.length; i++)
    {
        if(overallNonVotingChartNames60Days[i] !== 'GDT Pool' && overallNonVotingChartNames60Days[i] !== 'Elite + Supporters' && overallNonVotingChartNames60Days[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames60Days.push(overallNonVotingChartNames60Days[i]);
            overallNonVotingIndChartTotal60Days.push(overallNonVotingChartTotal60Days[i]);
        }
        else
        {
            overallNonVotingDelChartNames60Days.push(overallNonVotingChartNames60Days[i]);
            overallNonVotingDelChartTotal60Days.push(overallNonVotingChartTotal60Days[i]);

            overallNonVotingDelPChartNames60Days.push(overallNonVotingChartNames60Days[i]);

            if(overallNonVotingChartNames60Days[i] === 'Elite + Supporters' || overallNonVotingChartNames60Days[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotal60Days.push(overallNonVotingChartTotal60Days[i]);
            }
            else if(overallNonVotingChartNames60Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotal60Days[i];
               
                for(let d = 0; d < overallNonVotingChartNames60Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames60Days[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotal60Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotal60Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallNonVotingChartNames90Days.length; i++)
    {
        if(overallNonVotingChartNames90Days[i] !== 'GDT Pool' && overallNonVotingChartNames90Days[i] !== 'Elite + Supporters' && overallNonVotingChartNames90Days[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames90Days.push(overallNonVotingChartNames90Days[i]);
            overallNonVotingIndChartTotal90Days.push(overallNonVotingChartTotal90Days[i]);
        }
        else
        {
            overallNonVotingDelChartNames90Days.push(overallNonVotingChartNames90Days[i]);
            overallNonVotingDelChartTotal90Days.push(overallNonVotingChartTotal90Days[i]);

            overallNonVotingDelPChartNames90Days.push(overallNonVotingChartNames90Days[i]);

            if(overallNonVotingChartNames90Days[i] === 'Elite + Supporters' || overallNonVotingChartNames90Days[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotal90Days.push(overallNonVotingChartTotal90Days[i]);
            }
            else if(overallNonVotingChartNames90Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotal90Days[i];
               
                for(let d = 0; d < overallNonVotingChartNames90Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames90Days[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotal90Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotal90Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallNonVotingChartNames180Days.length; i++)
    {
        if(overallNonVotingChartNames180Days[i] !== 'GDT Pool' && overallNonVotingChartNames180Days[i] !== 'Elite + Supporters' && overallNonVotingChartNames180Days[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames180Days.push(overallNonVotingChartNames180Days[i]);
            overallNonVotingIndChartTotal180Days.push(overallNonVotingChartTotal180Days[i]);
        }
        else
        {
            overallNonVotingDelChartNames180Days.push(overallNonVotingChartNames180Days[i]);
            overallNonVotingDelChartTotal180Days.push(overallNonVotingChartTotal180Days[i]);

            overallNonVotingDelPChartNames180Days.push(overallNonVotingChartNames180Days[i]);

            if(overallNonVotingChartNames180Days[i] === 'Elite + Supporters' || overallNonVotingChartNames180Days[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotal180Days.push(overallNonVotingChartTotal180Days[i]);
            }
            else if(overallNonVotingChartNames180Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotal180Days[i];
               
                for(let d = 0; d < overallNonVotingChartNames180Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames180Days[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotal180Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotal180Days.push(GDTTotal);
            }
        }
    }
    for(let i = 0; i < overallNonVotingChartNames365Days.length; i++)
    {
        if(overallNonVotingChartNames365Days[i] !== 'GDT Pool' && overallNonVotingChartNames365Days[i] !== 'Elite + Supporters' && overallNonVotingChartNames365Days[i] !== 'Sherwood')
        {
            overallNonVotingIndChartNames365Days.push(overallNonVotingChartNames365Days[i]);
            overallNonVotingIndChartTotal365Days.push(overallNonVotingChartTotal365Days[i]);
        }
        else
        {
            overallNonVotingDelChartNames365Days.push(overallNonVotingChartNames365Days[i]);
            overallNonVotingDelChartTotal365Days.push(overallNonVotingChartTotal365Days[i]);

            overallNonVotingDelPChartNames365Days.push(overallNonVotingChartNames365Days[i]);

            if(overallNonVotingChartNames365Days[i] === 'Elite + Supporters' || overallNonVotingChartNames365Days[i] === 'Sherwood')
            {
                
                overallNonVotingDelPChartTotal365Days.push(overallNonVotingChartTotal365Days[i]);
            }
            else if(overallNonVotingChartNames365Days[i] === 'GDT Pool')
            {
                //Add every individual GDT member's contributions
                let GDTTotal = overallNonVotingChartTotal365Days[i];
               
                for(let d = 0; d < overallNonVotingChartNames365Days.length; d++)
                {
                    //if (allVotes.indexOf(votes.transaction.votes.added[i]) <= -1)
                    if(GDTMembers.indexOf(overallNonVotingChartNames365Days[d]) > -1)
                    {
                        GDTTotal += overallNonVotingChartTotal365Days[d];
    
                        //Correct round-off error
                        GDTTotal = Math.round(GDTTotal*100000000)/100000000;
                    }
                }
                overallNonVotingDelPChartTotal365Days.push(GDTTotal);
            }
        }
    }
}

function getOverallData(){
    var data = overall_chart.config.data;
    if(overallVotingSwitch == 'Both')
    {
        if(overallRangeSwitch == 0)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames;
                data.datasets[0].data = overallChartTotals;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames;
                data.datasets[0].data = overallIndChartTotals;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames;
                data.datasets[0].data = overallDelChartTotals;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames;
                data.datasets[0].data = overallDelPChartTotals;
            }
        }
        else if(overallRangeSwitch == 30)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames30Days;
                data.datasets[0].data = overallChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames30Days;
                data.datasets[0].data = overallIndChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames30Days;
                data.datasets[0].data = overallDelChartTotal30Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames30Days;
                data.datasets[0].data = overallDelPChartTotal30Days;
            }
        }
        else if(overallRangeSwitch == 60)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames60Days;
                data.datasets[0].data = overallChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames60Days;
                data.datasets[0].data = overallIndChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames60Days;
                data.datasets[0].data = overallDelChartTotal60Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames60Days;
                data.datasets[0].data = overallDelPChartTotal60Days;
            }
        }
        else if(overallRangeSwitch == 90)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames90Days;
                data.datasets[0].data = overallChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames90Days;
                data.datasets[0].data = overallIndChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames90Days;
                data.datasets[0].data = overallDelChartTotal90Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames90Days;
                data.datasets[0].data = overallDelPChartTotal90Days;
            }
        }
        else if(overallRangeSwitch == 180)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames180Days;
                data.datasets[0].data = overallChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames180Days;
                data.datasets[0].data = overallIndChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames180Days;
                data.datasets[0].data = overallDelChartTotal180Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames180Days;
                data.datasets[0].data = overallDelPChartTotal180Days;
            }
        }
        else if(overallRangeSwitch == 365)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallChartNames365Days;
                data.datasets[0].data = overallChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallIndChartNames365Days;
                data.datasets[0].data = overallIndChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallDelChartNames365Days;
                data.datasets[0].data = overallDelChartTotal365Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallDelPChartNames365Days;
                data.datasets[0].data = overallDelPChartTotal365Days;
            }
        }
    }
    else if(overallVotingSwitch == 'Yes')
    {
        if(overallRangeSwitch == 0)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames;
                data.datasets[0].data = overallVotingChartTotals;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames;
                data.datasets[0].data = overallVotingIndChartTotals;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames;
                data.datasets[0].data = overallVotingDelChartTotals;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames;
                data.datasets[0].data = overallVotingDelPChartTotals;
            }
        }
        else if(overallRangeSwitch == 30)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames30Days;
                data.datasets[0].data = overallVotingChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames30Days;
                data.datasets[0].data = overallVotingIndChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames30Days;
                data.datasets[0].data = overallVotingDelChartTotal30Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames30Days;
                data.datasets[0].data = overallVotingDelPChartTotal30Days;
            }
        }
        else if(overallRangeSwitch == 60)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames60Days;
                data.datasets[0].data = overallVotingChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames60Days;
                data.datasets[0].data = overallVotingIndChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames60Days;
                data.datasets[0].data = overallVotingDelChartTotal60Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames60Days;
                data.datasets[0].data = overallVotingDelPChartTotal60Days;
            }
        }
        else if(overallRangeSwitch == 90)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames90Days;
                data.datasets[0].data = overallVotingChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames90Days;
                data.datasets[0].data = overallVotingIndChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames90Days;
                data.datasets[0].data = overallVotingDelChartTotal90Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames90Days;
                data.datasets[0].data = overallVotingDelPChartTotal90Days;
            }
        }
        else if(overallRangeSwitch == 180)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames180Days;
                data.datasets[0].data = overallVotingChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames180Days;
                data.datasets[0].data = overallVotingIndChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames180Days;
                data.datasets[0].data = overallVotingDelChartTotal180Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames180Days;
                data.datasets[0].data = overallVotingDelPChartTotal180Days;
            }
        }
        else if(overallRangeSwitch == 365)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallVotingChartNames365Days;
                data.datasets[0].data = overallVotingChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallVotingIndChartNames365Days;
                data.datasets[0].data = overallVotingIndChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallVotingDelChartNames365Days;
                data.datasets[0].data = overallVotingDelChartTotal365Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallVotingDelPChartNames365Days;
                data.datasets[0].data = overallVotingDelPChartTotal365Days;
            }
        }
    }
    else if(overallVotingSwitch == 'No')
    {
        if(overallRangeSwitch == 0)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames;
                data.datasets[0].data = overallNonVotingChartTotals;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames;
                data.datasets[0].data = overallNonVotingIndChartTotals;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames;
                data.datasets[0].data = overallNonVotingDelChartTotals;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames;
                data.datasets[0].data = overallNonVotingDelPChartTotals;
            }
        }
        else if(overallRangeSwitch == 30)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames30Days;
                data.datasets[0].data = overallNonVotingChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames30Days;
                data.datasets[0].data = overallNonVotingIndChartTotal30Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames30Days;
                data.datasets[0].data = overallNonVotingDelChartTotal30Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames30Days;
                data.datasets[0].data = overallNonVotingDelPChartTotal30Days;
            }
        }
        else if(overallRangeSwitch == 60)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames60Days;
                data.datasets[0].data = overallNonVotingChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames60Days;
                data.datasets[0].data = overallNonVotingIndChartTotal60Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames60Days;
                data.datasets[0].data = overallNonVotingDelChartTotal60Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames60Days;
                data.datasets[0].data = overallNonVotingDelPChartTotal60Days;
            }
        }
        else if(overallRangeSwitch == 90)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames90Days;
                data.datasets[0].data = overallNonVotingChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames90Days;
                data.datasets[0].data = overallNonVotingIndChartTotal90Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames90Days;
                data.datasets[0].data = overallNonVotingDelChartTotal90Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames90Days;
                data.datasets[0].data = overallNonVotingDelPChartTotal90Days;
            }
        }
        else if(overallRangeSwitch == 180)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames180Days;
                data.datasets[0].data = overallNonVotingChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames180Days;
                data.datasets[0].data = overallNonVotingIndChartTotal180Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames180Days;
                data.datasets[0].data = overallNonVotingDelChartTotal180Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames180Days;
                data.datasets[0].data = overallNonVotingDelPChartTotal180Days;
            }
        }
        else if(overallRangeSwitch == 365)
        {
            if(overallPoolSwitch == 'All')
            {
                data.labels = overallNonVotingChartNames365Days;
                data.datasets[0].data = overallNonVotingChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Ind')
            {
                data.labels = overallNonVotingIndChartNames365Days;
                data.datasets[0].data = overallNonVotingIndChartTotal365Days;
            }
            else if(overallPoolSwitch == 'Del')
            {
                data.labels = overallNonVotingDelChartNames365Days;
                data.datasets[0].data = overallNonVotingDelChartTotal365Days;
            }
            else if(overallPoolSwitch == 'DelP')
            {
                data.labels = overallNonVotingDelPChartNames365Days;
                data.datasets[0].data = overallNonVotingDelPChartTotal365Days;
            }
        }
    }
    overall_chart.update();
}

function IndDelegates(){
    ResetButtonBG('DataTypes');
    ActivateButtonBG("indDelegatesBtn");

    document.getElementById('delegateChartDiv').style.display = "block";
    document.getElementById('delegateTable').style.display = "block";
    document.getElementById('overallChartDiv').style.display = "none";
    document.getElementById('overallMonthlyChartDiv').style.display = "none";
    document.getElementById('overallTableDiv').style.display = "none";

    document.getElementById('MonthlyButtons').style.display = "none";
    document.getElementById('OverallButtons').style.display = "none";
    document.getElementById('delegateButtons').style.display = "block";
}

function OverallIndChart(){
    ResetButtonBG('Pools');
    ActivateButtonBG("OverallIndChart");
    overallPoolSwitch = 'Ind';
    getOverallData();
}

function OverallDelChart(){
    ResetButtonBG('Pools');
    ActivateButtonBG("OverallDelChart");
    overallPoolSwitch = 'Del';
    getOverallData();
}

function OverallDelPChart(){
    ResetButtonBG('Pools');
    ActivateButtonBG("OverallDelPChart");
    overallPoolSwitch = 'DelP';
    getOverallData();

}

function OverallAllChart(){
    ResetButtonBG('Pools');
    ActivateButtonBG("OverallAllChart");
    overallPoolSwitch = 'All';
    getOverallData();
}

function OverallPieChart(){
    ResetButtonBG('ChartTypes');
    ActivateButtonBG("OverallPieChart");
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'pie',
      data: data
    });
}

function OverallDonChart(){
    ResetButtonBG('ChartTypes');
    ActivateButtonBG("OverallDonChart");
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'doughnut',
      data: data
    });
}

function OverallBarChart(){
    ResetButtonBG('ChartTypes');
    ActivateButtonBG("OverallBarChart");
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'bar',
      data: data,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false,
            labels:{boxWidth:10}
        },
        scales: {
            xAxes: [{
                ticks: {
                autoSkip: false,
                beginAtZero:true
                }
                }],
            yAxes: [{
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
            }]
        },
        tooltips: {
        }
    }
    });
}

function OverallHBarChart(){
    ResetButtonBG('ChartTypes');
    ActivateButtonBG("OverallHBarChart");
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'horizontalBar',
      data: data,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            display: false,
            labels:{boxWidth:10}
         },
        scales: {
            xAxes: [{
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
                }],
            yAxes: [{
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
            }]
        },
        tooltips: {
        }
    }
    });
}

function OverallRangeAll2(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeAll2");
    overallRangeSwitch = 0;
    getOverallData();
}

function OverallRangeLast365Days(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeLast365Days");
    overallRangeSwitch = 365;
    getOverallData();
}

function OverallRangeLast180Days(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeLast180Days");
    overallRangeSwitch = 180;
    getOverallData();
}

function OverallRangeLast90Days(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeLast90Days");
    overallRangeSwitch = 90;
    getOverallData();
}

function OverallRangeLast60Days(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeLast60Days");
    overallRangeSwitch = 60;
    getOverallData();
}

function OverallRangeLast30Days(){
    ResetButtonBG('DataRange');
    ActivateButtonBG("OverallRangeLast30Days");
    overallRangeSwitch = 30;
    getOverallData();
}

function OverallBothVoting(){
    ResetButtonBG('OverallVoting');
    ActivateButtonBG("OverallBothVoting");
    overallVotingSwitch = 'Both';
    getOverallData();
}

function OverallOnlyVoting(){
    ResetButtonBG('OverallVoting');
    ActivateButtonBG("OverallOnlyVoting");
    overallVotingSwitch = 'Yes';
    getOverallData();
}

function OverallNotVoting(){
    ResetButtonBG('OverallVoting');
    ActivateButtonBG("OverallNotVoting");
    overallVotingSwitch = 'No';
    getOverallData();
}

function dataSelector()
{
    if(typeof monthlyDataContainer !== 'undefined')
    {
        //var votingSwitch = 'Both'; //Yes, No
        if(votingSwitch === 'Both')
        {
            if(poolSwitch === 'All')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallMonthlyData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallMonthlyLast3MonthsData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallMonthlyLast6MonthsData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallMonthlyLast12MonthsData;
                }
            }
            else if(poolSwitch === 'Ind')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallMonthlyIndData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallMonthlyLast3MonthsIndData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallMonthlyLast6MonthsIndData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallMonthlyLast12MonthsIndData;
                }
            }
            else if(poolSwitch === 'Del')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallMonthlyDelData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallMonthlyLast3MonthsDelData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallMonthlyLast6MonthsDelData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallMonthlyLast12MonthsDelData;
                }
            }
            else if(poolSwitch === 'DelP')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallMonthlyDelPData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallMonthlyLast3MonthsDelPData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallMonthlyLast6MonthsDelPData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallMonthlyLast12MonthsDelPData;
                }
            }
        }
        else if(votingSwitch === 'Yes')
        {
            if(poolSwitch === 'All')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallVotingMonthlyData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast3MonthsData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast6MonthsData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast12MonthsData;
                }
            }
            else if(poolSwitch === 'Ind')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallVotingMonthlyIndData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast3MonthsIndData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast6MonthsIndData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast12MonthsIndData;
                }
            }
            else if(poolSwitch === 'Del')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallVotingMonthlyDelData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast3MonthsDelData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast6MonthsDelData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast12MonthsDelData;
                }
            }
            else if(poolSwitch === 'DelP')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallVotingMonthlyDelPData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast3MonthsDelPData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast6MonthsDelPData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallVotingMonthlyLast12MonthsDelPData;
                }
            }
        }
        else if(votingSwitch === 'No')
        {
            if(poolSwitch === 'All')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast3MonthsData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast6MonthsData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast12MonthsData;
                }
            }
            else if(poolSwitch === 'Ind')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyIndData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast3MonthsIndData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast6MonthsIndData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast12MonthsIndData;
                }
            }
            else if(poolSwitch === 'Del')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyDelData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast3MonthsDelData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast6MonthsDelData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast12MonthsDelData;
                }
            }
            else if(poolSwitch === 'DelP')
            {
                if(dataRange === 0)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyDelPData;
                }
                else if(dataRange === 3)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast3MonthsDelPData;
                }
                else if(dataRange === 6)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast6MonthsDelPData;
                }
                else if(dataRange === 12)
                {
                    monthlyDataContainer.data = overallNonVotingMonthlyLast12MonthsDelPData;
                }
            }
        }
    }
}

function ResetButtonBG(className){
    var elements = document.getElementsByClassName(className);
    for (var i = 0; i < elements.length; i++) {
        elements[i].style.backgroundColor="#00b8b2";
    }
}

function ActivateButtonBG(idName){
    document.getElementById(idName).style.backgroundColor="#517bd2";
}

//Start of monthly functions
function OverallIndMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyPools');
    ActivateButtonBG("OverallIndMonthlyChart");
    poolSwitch = 'Ind';
    dataSelector();
    RefreshMonthlyChart();
}

function OverallDelMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyPools');
    ActivateButtonBG("OverallDelMonthlyChart");
    poolSwitch = 'Del';
    dataSelector();
    RefreshMonthlyChart();
}

function OverallDelPMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyPools');
    ActivateButtonBG("OverallDelPMonthlyChart");
    poolSwitch = 'DelP';
    dataSelector();
    RefreshMonthlyChart();
}

function OverallAllMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyPools');
    ActivateButtonBG("OverallAllMonthlyChart");
    poolSwitch = 'All';
    dataSelector();
    RefreshMonthlyChart();
}

function OverallRangeAll(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyDataRange');
    ActivateButtonBG("OverallRangeAll");
    dataRange = 0;
    dataSelector();
    RefreshMonthlyChart();
}

function OverallRange3Months(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyDataRange');
    ActivateButtonBG("OverallRange3Months");
    dataRange = 3;
    dataSelector();
    RefreshMonthlyChart();
}

function OverallRange6Months(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyDataRange');
    ActivateButtonBG("OverallRange6Months");
    dataRange = 6;
    dataSelector();
    RefreshMonthlyChart();
}

function OverallRange12Months(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyDataRange');
    ActivateButtonBG("OverallRange12Months");
    dataRange = 12;
    dataSelector();
    RefreshMonthlyChart();
}

function MonthlyBothVoting(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyVoting');
    ActivateButtonBG("MonthlyBothVoting");
    votingSwitch = 'Both';
    dataSelector();
    RefreshMonthlyChart();
}

function MonthlyOnlyVoting(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyVoting');
    ActivateButtonBG("MonthlyOnlyVoting");
    votingSwitch = 'Yes';
    dataSelector();
    RefreshMonthlyChart();
}

function MonthlyNotVoting(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyVoting');
    ActivateButtonBG("MonthlyNotVoting");
    votingSwitch = 'No';
    dataSelector();
    RefreshMonthlyChart();
}

/*
function OverallPieMonthlyChart(){
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'pie',
      data: data
    });
}

function OverallDonMonthlyChart(){
    var ctx4 = document.getElementById("overallChart");
    var data = overall_chart.config.data;
    overall_chart.destroy();
    overall_chart = new Chart(ctx4, {
      type: 'doughnut',
      data: data
    });
}
*/

function OverallStackedMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('Stacked');
    ActivateButtonBG("OverallStackedMonthlyChart");
    isStacked = true;
    RefreshMonthlyChart();
}

function OverallNonStackedMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('Stacked');
    ActivateButtonBG("OverallNonStackedMonthlyChart");
    isStacked = false;
    RefreshMonthlyChart();
}

function OverallBarMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyType');
    ActivateButtonBG("OverallBarMonthlyChart");
    chartType = 'bar';
    RefreshMonthlyChart();
}

function OverallHBarMonthlyChart(){
    disableThenEnable('MonthlyButton');
    ResetButtonBG('MonthlyType');
    ActivateButtonBG("OverallHBarMonthlyChart");
    chartType = 'horizontalBar';
    RefreshMonthlyChart();
}

function RefreshMonthlyChart(){
    var data = monthlyDataContainer.config.data;
    document.getElementById("omcd").innerHTML = '&nbsp;';
    document.getElementById("omcd").innerHTML = '<canvas id="overallMonthlyChart"></canvas>';
    var ctx4 = document.getElementById("overallMonthlyChart");
    monthlyDataContainer = new Chart(ctx4, {
      type: chartType,
      data: data,
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
            labels:{boxWidth:10}
        },
        tooltips: {
            enabled: false,
            mode: 'index',
            position: 'nearest',
            custom: customTooltips2,
        },
        scales: {
            xAxes: [{
                stacked: isStacked,
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
                }],
            yAxes: [{
                stacked: isStacked,
                ticks: {
                    autoSkip: false,
                    beginAtZero:true
                }
            }]
        }
    }
    });
}

function calculateMonthlyRanges()
{
    calculate3Months();
    calculate6Months();
    calculate12Months();
    calculateVoting3Months();
    calculateVoting6Months();
    calculateVoting12Months();
    calculateNonVoting3Months();
    calculateNonVoting6Months();
    calculateNonVoting12Months();
}

function calculate3Months()
{
    let numMonths = 3;
    //All Delegates
    for(let h = 0; h < overallMonthlyData.datasets.length; h++)
    {
        overallMonthlyLast3MonthsData.datasets[h] = {label:overallMonthlyData.datasets[h].label, data:[], backgroundColor: overallMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast3MonthsData.datasets[h].data[hh] = overallMonthlyData.datasets[h].data[overallMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyData.labels.length - 1; iii > overallMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast3MonthsData.labels[iii - (overallMonthlyData.labels.length - numMonths)] = overallMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast3MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast3MonthsData.labels[h] === undefined)
        {
            overallMonthlyLast3MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast3MonthsData.datasets.length; hh++)
            {
                overallMonthlyLast3MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallMonthlyIndData.datasets.length; h++)
    {
        overallMonthlyLast3MonthsIndData.datasets[h] = {label:overallMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast3MonthsIndData.datasets[h].data[hh] = overallMonthlyIndData.datasets[h].data[overallMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyIndData.labels.length - 1; iii > overallMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast3MonthsIndData.labels[iii - (overallMonthlyIndData.labels.length - numMonths)] = overallMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast3MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast3MonthsIndData.labels[h] === undefined)
        {
            overallMonthlyLast3MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast3MonthsIndData.datasets.length; hh++)
            {
                overallMonthlyLast3MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallMonthlyDelData.datasets.length; h++)
    {
        overallMonthlyLast3MonthsDelData.datasets[h] = {label:overallMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast3MonthsDelData.datasets[h].data[hh] = overallMonthlyDelData.datasets[h].data[overallMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelData.labels.length - 1; iii > overallMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast3MonthsDelData.labels[iii - (overallMonthlyDelData.labels.length - numMonths)] = overallMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast3MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast3MonthsDelData.labels[h] === undefined)
        {
            overallMonthlyLast3MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast3MonthsDelData.datasets.length; hh++)
            {
                overallMonthlyLast3MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallMonthlyDelPData.datasets.length; h++)
    {
        overallMonthlyLast3MonthsDelPData.datasets[h] = {label:overallMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast3MonthsDelPData.datasets[h].data[hh] = overallMonthlyDelPData.datasets[h].data[overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelPData.labels.length - 1; iii > overallMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast3MonthsDelPData.labels[iii - (overallMonthlyDelPData.labels.length - numMonths)] = overallMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast3MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast3MonthsDelPData.labels[h] === undefined)
        {
            overallMonthlyLast3MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast3MonthsDelPData.datasets.length; hh++)
            {
                overallMonthlyLast3MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculate6Months()
{
    let numMonths = 6;
    for(let h = 0; h < overallMonthlyData.datasets.length; h++)
    {
        overallMonthlyLast6MonthsData.datasets[h] = {label:overallMonthlyData.datasets[h].label, data:[], backgroundColor: overallMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast6MonthsData.datasets[h].data[hh] = overallMonthlyData.datasets[h].data[overallMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = (overallMonthlyData.labels.length - 1); iii > (overallMonthlyData.labels.length - (numMonths + 1)); iii--)
    {
        overallMonthlyLast6MonthsData.labels[iii - (overallMonthlyData.labels.length - numMonths)] = overallMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast6MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast6MonthsData.labels[h] === undefined)
        {
            overallMonthlyLast6MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast6MonthsData.datasets.length; hh++)
            {
                overallMonthlyLast6MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallMonthlyIndData.datasets.length; h++)
    {
        overallMonthlyLast6MonthsIndData.datasets[h] = {label:overallMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast6MonthsIndData.datasets[h].data[hh] = overallMonthlyIndData.datasets[h].data[overallMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyIndData.labels.length - 1; iii > overallMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast6MonthsIndData.labels[iii - (overallMonthlyIndData.labels.length - numMonths)] = overallMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast6MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast6MonthsIndData.labels[h] === undefined)
        {
            overallMonthlyLast6MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast6MonthsIndData.datasets.length; hh++)
            {
                overallMonthlyLast6MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallMonthlyDelData.datasets.length; h++)
    {
        overallMonthlyLast6MonthsDelData.datasets[h] = {label:overallMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast6MonthsDelData.datasets[h].data[hh] = overallMonthlyDelData.datasets[h].data[overallMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelData.labels.length - 1; iii > overallMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast6MonthsDelData.labels[iii - (overallMonthlyDelData.labels.length - numMonths)] = overallMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast6MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast6MonthsDelData.labels[h] === undefined)
        {
            overallMonthlyLast6MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast6MonthsDelData.datasets.length; hh++)
            {
                overallMonthlyLast6MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove


    //DelP Delgates
    for(let h = 0; h < overallMonthlyDelPData.datasets.length; h++)
    {
        overallMonthlyLast6MonthsDelPData.datasets[h] = {label:overallMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast6MonthsDelPData.datasets[h].data[hh] = overallMonthlyDelPData.datasets[h].data[overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelPData.labels.length - 1; iii > overallMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast6MonthsDelPData.labels[iii - (overallMonthlyDelPData.labels.length - numMonths)] = overallMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast6MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast6MonthsDelPData.labels[h] === undefined)
        {
            overallMonthlyLast6MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast6MonthsDelPData.datasets.length; hh++)
            {
                overallMonthlyLast6MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculate12Months()
{
    let numMonths = 12;
    for(let h = 0; h < overallMonthlyData.datasets.length; h++)
    {
        overallMonthlyLast12MonthsData.datasets[h] = {label:overallMonthlyData.datasets[h].label, data:[], backgroundColor: overallMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast12MonthsData.datasets[h].data[hh] = overallMonthlyData.datasets[h].data[overallMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyData.labels.length - 1; iii > overallMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast12MonthsData.labels[iii - (overallMonthlyData.labels.length - numMonths)] = overallMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast12MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast12MonthsData.labels[h] === undefined)
        {
            overallMonthlyLast12MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast12MonthsData.datasets.length; hh++)
            {
                overallMonthlyLast12MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallMonthlyIndData.datasets.length; h++)
    {
        overallMonthlyLast12MonthsIndData.datasets[h] = {label:overallMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast12MonthsIndData.datasets[h].data[hh] = overallMonthlyIndData.datasets[h].data[overallMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyIndData.labels.length - 1; iii > overallMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast12MonthsIndData.labels[iii - (overallMonthlyIndData.labels.length - numMonths)] = overallMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast12MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast12MonthsIndData.labels[h] === undefined)
        {
            overallMonthlyLast12MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast12MonthsIndData.datasets.length; hh++)
            {
                overallMonthlyLast12MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallMonthlyDelData.datasets.length; h++)
    {
        overallMonthlyLast12MonthsDelData.datasets[h] = {label:overallMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast12MonthsDelData.datasets[h].data[hh] = overallMonthlyDelData.datasets[h].data[overallMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelData.labels.length - 1; iii > overallMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast12MonthsDelData.labels[iii - (overallMonthlyDelData.labels.length - numMonths)] = overallMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast12MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast12MonthsDelData.labels[h] === undefined)
        {
            overallMonthlyLast12MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast12MonthsDelData.datasets.length; hh++)
            {
                overallMonthlyLast12MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallMonthlyDelPData.datasets.length; h++)
    {
        overallMonthlyLast12MonthsDelPData.datasets[h] = {label:overallMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallMonthlyLast12MonthsDelPData.datasets[h].data[hh] = overallMonthlyDelPData.datasets[h].data[overallMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallMonthlyDelPData.labels.length - 1; iii > overallMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallMonthlyLast12MonthsDelPData.labels[iii - (overallMonthlyDelPData.labels.length - numMonths)] = overallMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallMonthlyLast12MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallMonthlyLast12MonthsDelPData.labels[h] === undefined)
        {
            overallMonthlyLast12MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallMonthlyLast12MonthsDelPData.datasets.length; hh++)
            {
                overallMonthlyLast12MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

//Voting
function calculateVoting3Months()
{
    let numMonths = 3;
    //All Delegates
    for(let h = 0; h < overallVotingMonthlyData.datasets.length; h++)
    {
        overallVotingMonthlyLast3MonthsData.datasets[h] = {label:overallVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast3MonthsData.datasets[h].data[hh] = overallVotingMonthlyData.datasets[h].data[overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyData.labels.length - 1; iii > overallVotingMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast3MonthsData.labels[iii - (overallVotingMonthlyData.labels.length - numMonths)] = overallVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast3MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast3MonthsData.labels[h] === undefined)
        {
            overallVotingMonthlyLast3MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast3MonthsData.datasets.length; hh++)
            {
                overallVotingMonthlyLast3MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallVotingMonthlyIndData.datasets.length; h++)
    {
        overallVotingMonthlyLast3MonthsIndData.datasets[h] = {label:overallVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast3MonthsIndData.datasets[h].data[hh] = overallVotingMonthlyIndData.datasets[h].data[overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyIndData.labels.length - 1; iii > overallVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast3MonthsIndData.labels[iii - (overallVotingMonthlyIndData.labels.length - numMonths)] = overallVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast3MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast3MonthsIndData.labels[h] === undefined)
        {
            overallVotingMonthlyLast3MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast3MonthsIndData.datasets.length; hh++)
            {
                overallVotingMonthlyLast3MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallVotingMonthlyDelData.datasets.length; h++)
    {
        overallVotingMonthlyLast3MonthsDelData.datasets[h] = {label:overallVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast3MonthsDelData.datasets[h].data[hh] = overallVotingMonthlyDelData.datasets[h].data[overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelData.labels.length - 1; iii > overallVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast3MonthsDelData.labels[iii - (overallVotingMonthlyDelData.labels.length - numMonths)] = overallVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast3MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast3MonthsDelData.labels[h] === undefined)
        {
            overallVotingMonthlyLast3MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast3MonthsDelData.datasets.length; hh++)
            {
                overallVotingMonthlyLast3MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallVotingMonthlyDelPData.datasets.length; h++)
    {
        overallVotingMonthlyLast3MonthsDelPData.datasets[h] = {label:overallVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast3MonthsDelPData.datasets[h].data[hh] = overallVotingMonthlyDelPData.datasets[h].data[overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelPData.labels.length - 1; iii > overallVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast3MonthsDelPData.labels[iii - (overallVotingMonthlyDelPData.labels.length - numMonths)] = overallVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast3MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast3MonthsDelPData.labels[h] === undefined)
        {
            overallVotingMonthlyLast3MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast3MonthsDelPData.datasets.length; hh++)
            {
                overallVotingMonthlyLast3MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculateVoting6Months()
{
    let numMonths = 6;
    for(let h = 0; h < overallVotingMonthlyData.datasets.length; h++)
    {
        overallVotingMonthlyLast6MonthsData.datasets[h] = {label:overallVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast6MonthsData.datasets[h].data[hh] = overallVotingMonthlyData.datasets[h].data[overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = (overallVotingMonthlyData.labels.length - 1); iii > (overallVotingMonthlyData.labels.length - (numMonths + 1)); iii--)
    {
        overallVotingMonthlyLast6MonthsData.labels[iii - (overallVotingMonthlyData.labels.length - numMonths)] = overallVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast6MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast6MonthsData.labels[h] === undefined)
        {
            overallVotingMonthlyLast6MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast6MonthsData.datasets.length; hh++)
            {
                overallVotingMonthlyLast6MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallVotingMonthlyIndData.datasets.length; h++)
    {
        overallVotingMonthlyLast6MonthsIndData.datasets[h] = {label:overallVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast6MonthsIndData.datasets[h].data[hh] = overallVotingMonthlyIndData.datasets[h].data[overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyIndData.labels.length - 1; iii > overallVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast6MonthsIndData.labels[iii - (overallVotingMonthlyIndData.labels.length - numMonths)] = overallVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast6MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast6MonthsIndData.labels[h] === undefined)
        {
            overallVotingMonthlyLast6MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast6MonthsIndData.datasets.length; hh++)
            {
                overallVotingMonthlyLast6MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallVotingMonthlyDelData.datasets.length; h++)
    {
        overallVotingMonthlyLast6MonthsDelData.datasets[h] = {label:overallVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast6MonthsDelData.datasets[h].data[hh] = overallVotingMonthlyDelData.datasets[h].data[overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelData.labels.length - 1; iii > overallVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast6MonthsDelData.labels[iii - (overallVotingMonthlyDelData.labels.length - numMonths)] = overallVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast6MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast6MonthsDelData.labels[h] === undefined)
        {
            overallVotingMonthlyLast6MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast6MonthsDelData.datasets.length; hh++)
            {
                overallVotingMonthlyLast6MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove


    //DelP Delgates
    for(let h = 0; h < overallVotingMonthlyDelPData.datasets.length; h++)
    {
        overallVotingMonthlyLast6MonthsDelPData.datasets[h] = {label:overallVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast6MonthsDelPData.datasets[h].data[hh] = overallVotingMonthlyDelPData.datasets[h].data[overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelPData.labels.length - 1; iii > overallVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast6MonthsDelPData.labels[iii - (overallVotingMonthlyDelPData.labels.length - numMonths)] = overallVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast6MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast6MonthsDelPData.labels[h] === undefined)
        {
            overallVotingMonthlyLast6MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast6MonthsDelPData.datasets.length; hh++)
            {
                overallVotingMonthlyLast6MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculateVoting12Months()
{
    let numMonths = 12;
    for(let h = 0; h < overallVotingMonthlyData.datasets.length; h++)
    {
        overallVotingMonthlyLast12MonthsData.datasets[h] = {label:overallVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast12MonthsData.datasets[h].data[hh] = overallVotingMonthlyData.datasets[h].data[overallVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyData.labels.length - 1; iii > overallVotingMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast12MonthsData.labels[iii - (overallVotingMonthlyData.labels.length - numMonths)] = overallVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast12MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast12MonthsData.labels[h] === undefined)
        {
            overallVotingMonthlyLast12MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast12MonthsData.datasets.length; hh++)
            {
                overallVotingMonthlyLast12MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallVotingMonthlyIndData.datasets.length; h++)
    {
        overallVotingMonthlyLast12MonthsIndData.datasets[h] = {label:overallVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast12MonthsIndData.datasets[h].data[hh] = overallVotingMonthlyIndData.datasets[h].data[overallVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyIndData.labels.length - 1; iii > overallVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast12MonthsIndData.labels[iii - (overallVotingMonthlyIndData.labels.length - numMonths)] = overallVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast12MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast12MonthsIndData.labels[h] === undefined)
        {
            overallVotingMonthlyLast12MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast12MonthsIndData.datasets.length; hh++)
            {
                overallVotingMonthlyLast12MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallVotingMonthlyDelData.datasets.length; h++)
    {
        overallVotingMonthlyLast12MonthsDelData.datasets[h] = {label:overallVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast12MonthsDelData.datasets[h].data[hh] = overallVotingMonthlyDelData.datasets[h].data[overallVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelData.labels.length - 1; iii > overallVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast12MonthsDelData.labels[iii - (overallVotingMonthlyDelData.labels.length - numMonths)] = overallVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast12MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast12MonthsDelData.labels[h] === undefined)
        {
            overallVotingMonthlyLast12MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast12MonthsDelData.datasets.length; hh++)
            {
                overallVotingMonthlyLast12MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallVotingMonthlyDelPData.datasets.length; h++)
    {
        overallVotingMonthlyLast12MonthsDelPData.datasets[h] = {label:overallVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallVotingMonthlyLast12MonthsDelPData.datasets[h].data[hh] = overallVotingMonthlyDelPData.datasets[h].data[overallVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallVotingMonthlyDelPData.labels.length - 1; iii > overallVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallVotingMonthlyLast12MonthsDelPData.labels[iii - (overallVotingMonthlyDelPData.labels.length - numMonths)] = overallVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallVotingMonthlyLast12MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallVotingMonthlyLast12MonthsDelPData.labels[h] === undefined)
        {
            overallVotingMonthlyLast12MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallVotingMonthlyLast12MonthsDelPData.datasets.length; hh++)
            {
                overallVotingMonthlyLast12MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}
//EndVoting

//NonVoting
function calculateNonVoting3Months()
{
    let numMonths = 3;
    //All Delegates
    for(let h = 0; h < overallNonVotingMonthlyData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast3MonthsData.datasets[h] = {label:overallNonVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast3MonthsData.datasets[h].data[hh] = overallNonVotingMonthlyData.datasets[h].data[overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyData.labels.length - 1; iii > overallNonVotingMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast3MonthsData.labels[iii - (overallNonVotingMonthlyData.labels.length - numMonths)] = overallNonVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast3MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast3MonthsData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast3MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast3MonthsData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast3MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallNonVotingMonthlyIndData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast3MonthsIndData.datasets[h] = {label:overallNonVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast3MonthsIndData.datasets[h].data[hh] = overallNonVotingMonthlyIndData.datasets[h].data[overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyIndData.labels.length - 1; iii > overallNonVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast3MonthsIndData.labels[iii - (overallNonVotingMonthlyIndData.labels.length - numMonths)] = overallNonVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast3MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast3MonthsIndData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast3MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast3MonthsIndData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast3MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallNonVotingMonthlyDelData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast3MonthsDelData.datasets[h] = {label:overallNonVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast3MonthsDelData.datasets[h].data[hh] = overallNonVotingMonthlyDelData.datasets[h].data[overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelData.labels.length - 1; iii > overallNonVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast3MonthsDelData.labels[iii - (overallNonVotingMonthlyDelData.labels.length - numMonths)] = overallNonVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast3MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast3MonthsDelData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast3MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast3MonthsDelData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast3MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallNonVotingMonthlyDelPData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast3MonthsDelPData.datasets[h] = {label:overallNonVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast3MonthsDelPData.datasets[h].data[hh] = overallNonVotingMonthlyDelPData.datasets[h].data[overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelPData.labels.length - 1; iii > overallNonVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast3MonthsDelPData.labels[iii - (overallNonVotingMonthlyDelPData.labels.length - numMonths)] = overallNonVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast3MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast3MonthsDelPData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast3MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast3MonthsDelPData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast3MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculateNonVoting6Months()
{
    let numMonths = 6;
    for(let h = 0; h < overallNonVotingMonthlyData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast6MonthsData.datasets[h] = {label:overallNonVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast6MonthsData.datasets[h].data[hh] = overallNonVotingMonthlyData.datasets[h].data[overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = (overallNonVotingMonthlyData.labels.length - 1); iii > (overallNonVotingMonthlyData.labels.length - (numMonths + 1)); iii--)
    {
        overallNonVotingMonthlyLast6MonthsData.labels[iii - (overallNonVotingMonthlyData.labels.length - numMonths)] = overallNonVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast6MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast6MonthsData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast6MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast6MonthsData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast6MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallNonVotingMonthlyIndData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast6MonthsIndData.datasets[h] = {label:overallNonVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast6MonthsIndData.datasets[h].data[hh] = overallNonVotingMonthlyIndData.datasets[h].data[overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyIndData.labels.length - 1; iii > overallNonVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast6MonthsIndData.labels[iii - (overallNonVotingMonthlyIndData.labels.length - numMonths)] = overallNonVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast6MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast6MonthsIndData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast6MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast6MonthsIndData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast6MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallNonVotingMonthlyDelData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast6MonthsDelData.datasets[h] = {label:overallNonVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast6MonthsDelData.datasets[h].data[hh] = overallNonVotingMonthlyDelData.datasets[h].data[overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelData.labels.length - 1; iii > overallNonVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast6MonthsDelData.labels[iii - (overallNonVotingMonthlyDelData.labels.length - numMonths)] = overallNonVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast6MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast6MonthsDelData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast6MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast6MonthsDelData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast6MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove


    //DelP Delgates
    for(let h = 0; h < overallNonVotingMonthlyDelPData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast6MonthsDelPData.datasets[h] = {label:overallNonVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast6MonthsDelPData.datasets[h].data[hh] = overallNonVotingMonthlyDelPData.datasets[h].data[overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelPData.labels.length - 1; iii > overallNonVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast6MonthsDelPData.labels[iii - (overallNonVotingMonthlyDelPData.labels.length - numMonths)] = overallNonVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast6MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast6MonthsDelPData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast6MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast6MonthsDelPData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast6MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}

function calculateNonVoting12Months()
{
    let numMonths = 12;
    for(let h = 0; h < overallNonVotingMonthlyData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast12MonthsData.datasets[h] = {label:overallNonVotingMonthlyData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast12MonthsData.datasets[h].data[hh] = overallNonVotingMonthlyData.datasets[h].data[overallNonVotingMonthlyData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyData.labels.length - 1; iii > overallNonVotingMonthlyData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast12MonthsData.labels[iii - (overallNonVotingMonthlyData.labels.length - numMonths)] = overallNonVotingMonthlyData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast12MonthsData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast12MonthsData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast12MonthsData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast12MonthsData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast12MonthsData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Ind Delegates
    for(let h = 0; h < overallNonVotingMonthlyIndData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast12MonthsIndData.datasets[h] = {label:overallNonVotingMonthlyIndData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyIndData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast12MonthsIndData.datasets[h].data[hh] = overallNonVotingMonthlyIndData.datasets[h].data[overallNonVotingMonthlyIndData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyIndData.labels.length - 1; iii > overallNonVotingMonthlyIndData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast12MonthsIndData.labels[iii - (overallNonVotingMonthlyIndData.labels.length - numMonths)] = overallNonVotingMonthlyIndData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast12MonthsIndData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast12MonthsIndData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast12MonthsIndData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast12MonthsIndData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast12MonthsIndData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //Del Delegates
    for(let h = 0; h < overallNonVotingMonthlyDelData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast12MonthsDelData.datasets[h] = {label:overallNonVotingMonthlyDelData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast12MonthsDelData.datasets[h].data[hh] = overallNonVotingMonthlyDelData.datasets[h].data[overallNonVotingMonthlyDelData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelData.labels.length - 1; iii > overallNonVotingMonthlyDelData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast12MonthsDelData.labels[iii - (overallNonVotingMonthlyDelData.labels.length - numMonths)] = overallNonVotingMonthlyDelData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast12MonthsDelData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast12MonthsDelData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast12MonthsDelData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast12MonthsDelData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast12MonthsDelData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove

    //DelP Delgates
    for(let h = 0; h < overallNonVotingMonthlyDelPData.datasets.length; h++)
    {
        overallNonVotingMonthlyLast12MonthsDelPData.datasets[h] = {label:overallNonVotingMonthlyDelPData.datasets[h].label, data:[], backgroundColor: overallNonVotingMonthlyDelPData.datasets[h].backgroundColor};
        for(let hh = 0; hh < numMonths; hh++)
        {
            if(overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh) >= 0)
            {
                overallNonVotingMonthlyLast12MonthsDelPData.datasets[h].data[hh] = overallNonVotingMonthlyDelPData.datasets[h].data[overallNonVotingMonthlyDelPData.datasets[h].data.length -(numMonths - hh)];
            }

        }
    }

    for(let iii = overallNonVotingMonthlyDelPData.labels.length - 1; iii > overallNonVotingMonthlyDelPData.labels.length - (numMonths + 1); iii--)
    {
        overallNonVotingMonthlyLast12MonthsDelPData.labels[iii - (overallNonVotingMonthlyDelPData.labels.length - numMonths)] = overallNonVotingMonthlyDelPData.labels[iii];
    }

    //Check and remove undefined data
    for(let h = overallNonVotingMonthlyLast12MonthsDelPData.labels.length - 1; h >= 0; h--)
    {
        if(overallNonVotingMonthlyLast12MonthsDelPData.labels[h] === undefined)
        {
            overallNonVotingMonthlyLast12MonthsDelPData.labels.splice(h, 1);
            for(let hh = 0; hh < overallNonVotingMonthlyLast12MonthsDelPData.datasets.length; hh++)
            {
                overallNonVotingMonthlyLast12MonthsDelPData.datasets[hh].data.splice(h, 1);
            }
        }
    }
    //End Check & Remove
}
//EndNonVoting

function disableThenEnable(className){
    var myElements = document.getElementsByClassName(className);
    for (let i = 0; i < myElements.length; i++)
    {
        myElements[i].disabled = true;
    }

    setTimeout(function () {
        for (let i = 0; i < myElements.length; i++)
        {
            myElements[i].disabled = false;
        }
    }, 1000);
}

function copyObject(o) { var output, v, key; output = Array.isArray(o) ? [] : {}; for (key in o) { v = o[key]; output[key] = (typeof v === "object") ? copyObject(v) : v; } return output; }


//Delegate charts

var delegateList = [];

function getActiveDelegates(){
    fetch('https://wallet.mylisk.com/api/delegates?limit=101&orderBy=rank:asc')
    .then(res => res.json())
    .then((out) => {
        getActiveDelegatesHelper(out);
    }).catch(err => console.error(err));
}

function getActiveDelegatesHelper(delegates){
    for(let i=0; i< delegates.delegates.length; i++)
    {
        delegateList.push(delegates.delegates[i].username);
    }

    delegateList.sort();
    
    document.getElementById("btnDelegateRankCharts").style.display = "block";
}

function connectDelegateRanks() {
    document.getElementById("numDaysDiv").style.display = "none";
    var selectedDelegates = [];

    for(var i = 0; i < 101; i++)
    {
        selectedDelegates.push(delegateList[i]);
    }
    var delegateStr = "[";
    for(var i = 0; i < selectedDelegates.length; i++)
    {
        delegateStr += "\"" + selectedDelegates[i] + "\"";
        if(i != selectedDelegates.length - 1)
        {
            delegateStr += ",";
        }
    }
    delegateStr += "]";
    
    var ws = new WebSocket("wss://www.liskpending.nl:9876/service");

    var daysRequested = document.getElementById("numDays").value;

    ws.onopen = function () {
        ws.send('{"delegates":' + delegateStr + ', "daysRequested":' + daysRequested + '}');
    };

    ws.onmessage = function (evt) {
        myjson = evt.data;
        ws.close();
    };

    ws.onclose = function () {
        var my1stJson = JSON.parse(myjson);

        delegateRankChartResize();
        window.onresize = delegateRankChartResize;

        //If using all data
        createDelegateRankChart(my1stJson);
    };
};

function createDelegateRankChart(chartData){
    var config = {
        type: 'line',
        data: chartData,
        options: {
            elements: { point: { radius: 0 } },
            responsive: true,
            maintainAspectRatio: false,
            title:{
                display:true
            },
            legend: {
                display: true,
                labels:{fontSize:10, boxWidth:10},
                position: 'bottom'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            scales: {
                xAxes: [{
                    display: false,
                    scaleLabel: {
                        display: true
                    }
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        suggestedMin: 1,
                        suggestedMax: 101,
                        reverse: true,
                        steps: 1,
                        stepsize: 5
                    }
                }]
            }
        }
    };

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myLine = new Chart(ctx, config);
}

/*var delRankToggle = true;

function toggleChart(){
    if(delRankToggle == true)
    {
        window.myLine.config.options.legend.display = false;
        delRankToggle = false;
    }
    else
    {
        window.myLine.config.options.legend.display = true;
        delRankToggle = true;
    }
    window.myLine.update();
}*/

function delegateRankChartResize() {
    var delegateChartDiv = document.getElementById('delegateRankChartDiv');
    var chartDivWidth = delegateChartDiv.clientWidth;

    if(chartDivWidth < 400)
    {
        delegateChartDiv.style.height = "800px";
    }
    else
    {
        delegateChartDiv.style.height = "600px";
    }
}