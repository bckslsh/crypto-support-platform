<?php
/**
 * Template Name: category
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

     <!-- Sections -->
        <section class="pb-5 alt-background">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-8 col-xl-9 mt-n5">
                        <div class="row news-list">

                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                            <div class="col-12 col-md-6 mb-4">
                                <div class="card h-100 card-raised">
                                	<div class="card-img-top" style="background-image: url('<?php the_post_thumbnail_url('large') ?>');"></div>
                                    <div class="card-body px-5 pt-5 pb-3">
                                        <div class="card-subtitle mb-2">
                                            <span class="text-info"><?php the_category( ', ' ); ?></span>
                                            <small class="text-muted">
                                                <?php echo get_the_date(); ?>
                                            </small>
                                        </div>
                                        <div class="card-author text-muted">
                                            <i class="icon ion-ios-person"></i> <?php the_author(); ?>
                                        </div>
                                        <h3 class="card-title my-4">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="card-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                    </div>
                                    <div class="card-footer px-5 pb-5 bg-white border-0">
                                        <a href="<?php the_permalink() ?>">
                                            continue to read
                                            <i class="icon ion-arrow-right-c"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                           <?php endwhile; else : ?>
                           	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                           <?php endif; ?>
                        </div>
                    </div>
            <div class="col-12 col-lg-4 col-xl-3 mt-lg-5">
                <div class="card card-raised">
                    <div class="card-header bg-white border-0">
                        <h6 class="text-uppercase primary mt-2">
                            Subscribe
                        </h6>
						<p class="lead comp"><small>Receive a notification per mail whenever there's a new post</small></p>
                    </div>
                    <div class="card-body">
						<?php echo do_shortcode( '[contact-form-7 id="279" title="Subscribe news"]' ); ?>
                    </div>
                </div>
                <div class="card card-raised mt-5">
                    <div class="card-header bg-white border-0">
                        <h6 class="text-uppercase primary mt-2">
                            More posts
                        </h6>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">

                             <!-- // Define our WP Query Parameters -->
                             <?php $the_query = new WP_Query( 'posts_per_page=10' ); ?>

                            <!-- // Start our WP Query -->
                            <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                            <li class="mb-3">
                                <a href="<?php the_permalink() ?>">
                                    <strong>
                                        <?php the_title() ?>
                                    </strong>
                                </a>
                                <div class="text-muted">
                                    <small>
                                        <?php echo get_the_date(); ?>
                                    </small>
                                </div>
                            </li>

                            <?php
                                endwhile;
                                wp_reset_postdata();
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="card card-raised mt-5">
                    <div class="card-header bg-white border-0">
                        <h6 class="text-uppercase primary mt-2">
                            Categories
                        </h6>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled">
                            <li>
                                <strong><?php wp_list_cats(); ?></strong>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>


<?php get_footer();
