<?php
/**
* The template for displaying comments
*
* This is the template that displays the area of the page that contains both the current comments
* and the comment form.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
/*
* If the current post is protected by a password and
* the visitor has not yet entered the password we will
* return early without loading the comments.
*/
if ( post_password_required() ) {
return;
}
?>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-sm-12 col-lg-9">
			<div id="comments" class="mt-5 mb-5 p-md-5">
				<?php
				// You can start editing here -- including this comment!
				if ( have_comments() ) : ?>
				<h1 class="comments-title mb-5 primary text-center"><i class="ion-ios-chatbubble pr-2"></i>
				<?php
				$comments_number = get_comments_number();
				if ( '1' === $comments_number ) {
				/* translators: %s: post title */
				printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'twentyseventeen' ), get_the_title() );
				} else {
				printf(
				/* translators: 1: number of comments, 2: post title */
				_nx(
				'%1$s Reply to &ldquo;%2$s&rdquo;',
				'%1$s Replies to &ldquo;%2$s&rdquo;',
				$comments_number,
				'comments title',
				'twentyseventeen'
				),
				number_format_i18n( $comments_number ),
				get_the_title()
				);
				}
				?>
				</h1>
				<ol class="comment-list list-unstyled">
					<?php
					wp_list_comments( array(
					'avatar_size' => 100,
					'style'       => 'ol',
					'short_ping'  => true,
					'reply_text'  => __( 'Reply', 'twentyseventeen' ),
					) );
					?>
				</ol>
				<?php
					/* If there are no comments and comments are closed, let's leave a little note, shall we?
					* But we don't want the note on pages or post types that do not support comments.
					*/
					elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) :
				?>
				
				<p>
					<?php echo __('Comments are closed', 'twentyseventeen')?>
				</p>
				
				<?php endif; ?>
				<?php
					/*
					* Adding bootstrap support to comment form,
					* and some form validation using javascript.
					*/
					
					ob_start();
					$commenter = wp_get_current_commenter();
					$req = true;
					$aria_req = ( $req ? " aria-required='true'" : '' );
					
					$comments_arg = array(
								'form'	=> array(
							'class' => 'form-horizontal'
							),
						'fields' => apply_filters( 'comment_form_default_fields', array(
																'author' 				=> '<div class="form-group">' . '<label for="author">' . __( 'Name', 'twentyseventeen' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .
														'<input id="author" name="author" class="form-control-custom" type="text" value="" size="30"' . $aria_req . ' />'.
														'<p id="d1" class="text-danger"></p>' . '</div>',
																		'email'					=> '<div class="form-group">' .'<label for="email">' . __( 'Email', 'twentyseventeen' ) . '</label> ' . ( $req ? '<span>*</span>' : '' ) .
														'<input id="email" name="email" class="form-control-custom" type="text" value="" size="30"' . $aria_req . ' />'.
														'<p id="d2" class="text-danger"></p>' . '</div>',
																		'url'					=> '')),
														'comment_field'			=> '<div class="form-group">' . '<label for="comment">' . __( 'Comment', 'twentyseventeen' ) . '</label><span>*</span>' .
														'<textarea id="comment" class="form-control-custom" name="comment" rows="3" aria-required="true"></textarea><p id="d3" class="text-danger"></p>' . '</div>',
										'comment_notes_after' 	=> '',
														'class_submit'			=> 'btn btn-primary'
				); ?>
				<?php comment_form($comments_arg);
				echo str_replace('class="comment-form"','class="comment-form" name="commentForm" onsubmit="return validateForm();"',ob_get_clean());
				?>
			</div>
		</div>
		</div><!-- #comments -->