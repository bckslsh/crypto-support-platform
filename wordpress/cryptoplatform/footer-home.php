<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
        <footer class="page-footer">
            <section class="text-center bg-lightdark">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-white text-center my-5">
                            <h1 class="text-uppercase text-white"><i class="ion-email-unread pr-3"></i>Keep up to date</h1>
                            <p class="lead">Get notified when we post new blogs and signup for our monthly newsletter.</p>
                                    <?php echo do_shortcode( '[contact-form-7 id="276" title="Subscribe news"]' ); ?>
                            <p class="small mt-n2">Note that we will not sell or give your credentials to third parties. You can unsubscribe at any time!</p>
                        </div>
                    </div>
                </div>
            </section>

            <section id="footer" class="bg-dark text-white">
                <div class="container">
                    <div class="row py-5">
                        <?php
                        get_template_part( 'template-parts/footer/footer', 'widgets' );
                        ?>
                    </div>
                </div>
            </section>
            <section class="bg-white text-black d-none" id="adspace" data-ad-id="fixedBottom" data-ad-visibility="d-none d-md-block">
				<button class="btn btn-link text-dark close-ad" data-ad-target="fixedBottom">
					<i class="ion-close-circled"></i>
				</button>
                <a href="https://moracle.network/" target="_blank" class="advertisement">
					<div class="container h-100">
	                    <div class="row align-items-center h-100">
	                        <div class="col text-center">
	                            <img alt="advertisement" src="/wp-content/themes/cryptoplatform/assets/images/mrcl.png" class="adimg">
	                        </div>
	                        <div class="col-7 text-black text-center adtext">
	                            <p class="mb-0">
									Smarter blockchain applications need smarter oracles.
								</p>
	                        </div>
	                        <div class="col text-center">
	                            <button value="LEARN MORE" class="btn btn-primary-ad text-center text-dark">
									LEARN MORE
								</button>
	                        </div>
	                    </div>
	                </div>
				</a>
            </section>			
        </footer>
		
		<!-- The popup -->
		<div id="popup">Thank you for your visit to lisk.support. This site is founded by <a href="https://forum.lisk.support/t/delegate-joo5ty/41" target="_blank">joo5ty</a> to help and promote Lisk via the community. Please consider your vote for him as a delegate.</div>
		
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="/wp-content/themes/cryptoplatform/assets/js/jquery-3.2.1.slim.min.js"></script>
        <script src="/wp-content/themes/cryptoplatform/assets/js/bootstrap-bundle.min.js"></script>
        <!-- Needed for Lisk Ticker Include -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script src="/wp-content/themes/cryptoplatform/assets/js/customjs.js"></script>
        <?php if(is_home()) : ?>
            <script src="/wp-content/themes/cryptoplatform/assets/js/particles.min.js"></script>
            <script src="/wp-content/themes/cryptoplatform/assets/js/particles-cfg.json"></script>
        <?php else : ?>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    		<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.js"></script>
    		<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>

            <script src="/wp-content/themes/cryptoplatform/assets/js/lisk-support-tools.js?v=1.5"></script>
			<script src="/wp-content/themes/cryptoplatform/assets/js/pendingv2.js?v=2.0.3"></script>
        <?php endif; ?>
        <!-- Animate on scoll JS -->
        <script src="/wp-content/themes/cryptoplatform/assets/vendor/AOS/AOS.js"></script>
        <script>
            AOS.init({
                disable: 'mobile'
            });
        </script>
        <!-- Social Share Kit JS -->
        <script type="text/javascript" src="/wp-content/themes/cryptoplatform/assets/vendor/socialsharekit/js/social-share-kit.min.js"></script>
        <script type="text/javascript">
            SocialShareKit.init();
        </script>
        <?php wp_footer(); ?>
        
		<!-- Popup Script -->
        <script type="text/javascript">		
		function popup() {
		    // Get the popup DIV
		    var x = document.getElementById("popup")
		
		    // Add the "show" class to DIV
		    x.className = "show";
		
		    // After 3 seconds, remove the show class from DIV
		    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 10000);
		}

		popup();
		</script>
		
		
    </body>

</html>
