<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-106925784-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-106925784-1');
    </script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/wp-content/themes/cryptoplatform/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/wp-content/themes/cryptoplatform/assets/vendor/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/wp-content/themes/cryptoplatform/assets/vendor/AOS/AOS.css">
	<link rel="stylesheet" href="/wp-content/themes/cryptoplatform/assets/vendor/socialsharekit/css/social-share-kit.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <!-- Global Favicon Setup -->
	<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/cryptoplatform/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/wp-content/themes/cryptoplatform/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/wp-content/themes/cryptoplatform/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/wp-content/themes/cryptoplatform/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/wp-content/themes/cryptoplatform/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="/wp-content/themes/cryptoplatform/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/wp-content/themes/cryptoplatform/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

	<script type="text/javascript">
        function googleTranslateElementInit() {
			new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'ar,de,en,es,fr,ja,nl,pl,zh-CN,zh-TW', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
		}
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>

<body <?php body_class(); ?>>


    <!-- Nav -->

    <div class="nav-topbar">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="ticker-bar">
                <div class="ticker-bar-item d-none d-md-block" data-data-key="symbol">
                    <span class="ticker-bar-value">
                        --
                    </span>
                </div>
                <div class="ticker-bar-item" data-data-key="price_usd">
                    <i class="icon ion-social-usd"></i>
                    <span class="ticker-bar-value" data-rule="dollars">
                        --
                    </span>
                </div>
                <div class="ticker-bar-item" data-data-key="price_btc">
                    <i class="icon ion-social-bitcoin"></i>
                    <span class="ticker-bar-value" data-rule="bitcoin">
                        --
                    </span>
                </div>
                <div class="ticker-bar-item d-none d-md-block" data-data-key="market_cap_usd">
                    <i class="icon ion-arrow-graph-up-right"></i>
                    <span class="ticker-bar-value">
                        --
                    </span>
                </div>
                <div class="ticker-bar-item d-none d-md-block" data-data-key="percent_change_24h">
                    <i class="icon ion-ios-time"></i>
                    <span class="ticker-bar-value">
                        --
                    </span>
                    <span>
                        %
                    </span>
                </div>
            </div>
            <div id="google_translate_element"></div>
        </div>
    </div>
    <nav class="navbar navbar-expand-md navbar-dark sticky-top flex-wrap">
        <div class="container navbar-header">
            	<a href="<?php echo home_url(); ?>" class="logo-link"><div class="logo"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 942.41 307.86"><defs><style>.cls-1{fill:#fff;}</style></defs><title>logo</title><path class="cls-1" d="M2.87 31.91H43.2v134.92h50.24v30.66H2.87zM152.64 51.06a23.59 23.59 0 0 1-40.33-16.86 23.59 23.59 0 1 1 40.33 16.86zm-36.8 146.48V65.87h40.33v131.62zM182.6 105.5q0-17.45 14.86-29.25t40.22-11.79q25.35 0 40.92 11.56t16.51 31.61h-41.32q-.71-6.84-4.72-9.32t-12-2.48q-15.8 0-15.8 9.67 0 8.73 19.34 12a157.47 157.47 0 0 1 35.62 10.14 31.12 31.12 0 0 1 13.8 11q5.54 7.67 5.54 18.75 0 18.4-15.8 30.07t-39.86 11.67q-24.06 0-40.1-11t-17.45-32.43h41.28q1.65 12 17 12 7.08 0 11.68-3.3a10 10 0 0 0 4.6-8.49q0-5.19-7.67-8.26a104.56 104.56 0 0 0-18.52-5.19q-10.85-2.12-21.82-5.31a41.61 41.61 0 0 1-18.63-11.09q-7.68-7.82-7.68-20.56zM362.79 197.49h-40.32V22.95h40.32v97.89l40.81-55h54.25l-56.36 67.22 56.37 64.39h-53.54l-41.53-52.56zM662.07 203.19H513.79a18.14 18.14 0 0 0 0 36.27h148.3a18.135 18.135 0 1 0 0-36.27zm0-175.26H513.79a18.14 18.14 0 0 0 0 36.27h148.3a18.14 18.14 0 1 0 0-36.27zM17.22 274.75a8.49 8.49 0 0 1-8.61 8.61 8.61 8.61 0 1 1 6.12-14.73 8.32 8.32 0 0 1 2.49 6.12zM23.85 247.45a14.05 14.05 0 0 1 5.8-11.39q5.8-4.6 15.7-4.6t16 4.51a15.52 15.52 0 0 1 6.44 12.3H51.66a4.52 4.52 0 0 0-1.87-3.63 9.09 9.09 0 0 0-4.7-1q-6.17 0-6.17 3.78 0 3.41 7.55 4.7a61.46 61.46 0 0 1 13.91 4 12.05 12.05 0 0 1 7.55 11.6 14 14 0 0 1-6.14 11.73q-6.17 4.56-15.56 4.56t-15.66-4.28q-6.26-4.29-6.78-12.67h16.08q.64 4.7 6.63 4.7a7.62 7.62 0 0 0 4.56-1.29 3.92 3.92 0 0 0 1.8-3.32q0-2-3-3.22a40.87 40.87 0 0 0-7.23-2q-4.24-.83-8.52-2.07a16.25 16.25 0 0 1-7.32-4.36 11.08 11.08 0 0 1-2.94-8.05zM113.36 231.98h15.75v51.38h-15.75v-7.55q-4.88 8.1-15.56 8.1-8.84 0-14.32-5.89t-5.48-16v-30h15.66v27.9q0 5 2.62 7.69a11 11 0 0 0 14.46 0q2.62-2.71 2.62-7.69zM158.67 238.7q5.25-7.27 15.06-7.28a21.73 21.73 0 0 1 16.62 7.41q6.81 7.41 6.81 18.92t-6.81 18.88a21.64 21.64 0 0 1-16.48 7.37 17.43 17.43 0 0 1-15.2-8.1v32h-15.75v-75.92h15.75zm19.06 28.36q3.41-3.27 3.41-9.35t-3.41-9.39a11 11 0 0 0-15.66 0q-3.41 3.27-3.41 9.35t3.41 9.39a11 11 0 0 0 15.66 0zM222.95 238.7q5.25-7.27 15.06-7.28a21.73 21.73 0 0 1 16.62 7.41q6.81 7.41 6.81 18.92t-6.81 18.88a21.64 21.64 0 0 1-16.48 7.37 17.43 17.43 0 0 1-15.2-8.1v32H207.2v-75.92h15.75zm19.06 28.36q3.41-3.27 3.41-9.35t-3.41-9.39a11 11 0 0 0-15.66 0q-3.41 3.27-3.41 9.35t3.41 9.39a11 11 0 0 0 15.66 0zM275.58 276.73q-7.79-7.27-7.79-19.15t7.74-19q7.74-7.14 19.15-7.14t19.11 7.12q7.69 7.14 7.69 19t-7.64 19.15q-7.65 7.28-19.06 7.28a27 27 0 0 1-19.2-7.26zm26.75-9.49q3.13-3.31 3.13-9.58t-3.22-9.53a10.38 10.38 0 0 0-7.64-3.27 10.23 10.23 0 0 0-7.6 3.27q-3.18 3.27-3.18 9.53t3.27 9.58a10.45 10.45 0 0 0 7.7 3.32 10 10 0 0 0 7.54-3.31zM347.36 231.98v9.58q5.53-10.13 14.73-10.13v16h-3.87q-5.44 0-8.15 2.58t-2.72 9v24.35h-15.74v-51.38zM392.49 270.01h5.62v13.35h-8q-8.66 0-13.4-3.82t-4.74-13.12v-21.36h-4.79v-13.08h4.79v-12.53h15.75v12.53h10.31v13.08H387.7v21.37q0 3.58 4.79 3.58zM684.43 83.51a133.74 133.74 0 0 0 21.64 136.55l.19.22q3.61 4.25 7.58 8.24a133.78 133.78 0 1 0-29.41-145.05zm39.47 135c-1.07-1.07-2.1-2.15-3.12-3.25.25-.27.49-.53.74-.79a27.87 27.87 0 0 1 45 31.46 119 119 0 0 1-42.62-27.42zM835.97 17.46a26.9 26.9 0 1 1-52-.65 120.61 120.61 0 0 1 52 .65zm-67.06 3.52a41.32 41.32 0 0 0 .54 10.55 41.12 41.12 0 0 0 81.5-9.52 119.65 119.65 0 0 1-70.78 228.12 42.08 42.08 0 0 0-68.49-45.91 119.65 119.65 0 0 1 57.23-183.24z"/><path class="cls-1" d="M853.46 148.86a32.71 32.71 0 1 0 0-46.26 32.75 32.75 0 0 0 0 46.26zm36.2-36.2a18.49 18.49 0 1 1-26.14 0 18.51 18.51 0 0 1 26.14-.01zM816.71 218.41a23.07 23.07 0 1 0 0-32.63 23.1 23.1 0 0 0 0 32.63zm22.57-22.57a8.85 8.85 0 1 1-12.51 0 8.86 8.86 0 0 1 12.51 0zM727.33 147.36a35.91 35.91 0 1 0 0-50.78 35.95 35.95 0 0 0 0 50.78zm40.73-40.73a21.69 21.69 0 1 1-30.67 0 21.71 21.71 0 0 1 30.67 0zM639.45 133.7a18.13 18.13 0 0 0-18.13-18.13H466.79l-18.13 18.13 18.13 18.13h154.53a18.14 18.14 0 0 0 18.13-18.13z"/></svg></div></a>
			<!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-right d-flex align-items-center">
                <button type="button" class="search-toggler d-block d-md-none" data-target="#search-container">
                    <i class="icon ion-search open-search"></i>
                    <i class="icon ion-close close-search d-none"></i>
                </button>
                <button type="button" class="navbar-toggler collapsed" data-target="#main-nav">
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                </button>
            </div>
            <div id="main-nav" class="navbar-slide-in">
                <button type="button" class="navbar-toggler collapsed" data-target="#main-nav">
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                </button>
                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'top',
                        'depth'             => 2,
                        'container'         => '',
                        'container_class'   => '',
                        'container_id'      => '',
                        'menu_class'        => 'nav navbar-nav ml-auto justify-content-end text-uppercase',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker())
                    );
                ?>
            </div>
	    </div>
	    <div id="search-container" class="bloodhound">
            <form action="/" method="get" class="form-inline">
                <label class="sr-only" for="search">Search</label>
                <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="typeahead hero-search-box" placeholder="Search">
                <input type="submit" alt="Search" src="<?php bloginfo( 'template_url' ); ?>" value="&#xf21f;" class="hero-search-icon">
            </form>
        </div>
	</nav>

    <div data-page="content">
        <section id="hero-single">
            <div class="container h-100">
            </div>
        </section>
    </div>
