<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(home); ?>

    <section id="latest-news" class="bg-light-blue">
        <div class="container">
            <div class="row news-list">
                <div class="col-12">

                    <div class="card super-card card-raised mb-5 mt-n5">
                        <div class="card-body pb-5 pb-md-5 px-md-5">

                            <a href="/news"><h1 class="text-uppercase primary text-center pt-5"><i class="ion-ios-paper-outline pr-3"></i>Latest Lisk news</h1></a>
                            <p class="lead text-center">Keep up to date with the latest news and blogs</p>

                            <div class="row mt-5">
                                <!-- // Define our WP Query Parameters -->
                                <?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>

                                <!-- // Start our WP Query -->
                                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                                    <div class="col-12 col-md-4 mb-3 mb-md-0">
                                        <div class="card border mb-3 h-100">
                                            <div class="card-img-top card-img-small" style="background-image: url('<?php the_post_thumbnail_url('full') ?>');"></div>
                                                <div class="card-body px-4 px-md-5 pt-5 pb-3">
                                                    <div class="card-subtitle mb-2">
                                                        <span class="text-info">
                                                            <?php the_category( ', ' ); ?>
                                                        </span>
                                                        <small class="text-muted">
                                                            - <?php echo get_the_date(); ?>
                                                        </small>
                                                    </div>
                                                    <div class="card-author text-muted">
                                                        <i class="icon ion-ios-person"></i> <?php the_author(); ?>
                                                    </div>
                                                    <h5 class="card-title my-4">
                                                        <a href="<?php the_permalink() ?>">
                                                            <?php the_title() ?>
                                                        </a>
                                                    </h5>
                                                    <div class="card-text">
                                                        <?php the_excerpt(); ?>
                                                    </div>
                                                </div>
                                                <div class="card-footer px-4 px-md-5 pb-5 bg-white border-0">
                                                    <a href="<?php the_permalink() ?>">
                                                        continue to read
                                                        <i class="icon ion-arrow-right-c"></i>
                                                    </a>
                                            </div>
                                        </div>
                                    </div>

                                <!-- // Repeat the process and reset once it hits the limit -->
                                <?php
                                endwhile;
                                wp_reset_postdata();
                                ?>
                            </div>
                            <div class="text-md-center">
                                <a href="/news" class="btn mt-md-5">
                                    read all Lisk news <i class="icon ion-arrow-right-c"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>


    <section id="get-started" class="bg-alt">
        <div class="container">
            <div class="row">
                <div class="col-12 text-secondary text-center my-5">
					<a href="knowledge-base/">
					    <h1 class="text-uppercase primary"><i class="ion-ios-book-outline pr-3"></i>LISK KNOWLEDGE BASE</h1>
					</a>
                    <p class="lead text-center">all you need to know to get started with the Lisk blockchain platform</p>
                    <div class="row">

                        <?php
                            $args = array( 'post_type' => 'knowledgebase', 'order' => 'ASC', 'posts_per_age' => '3');
                            $loop = new WP_Query( $args );
                            $cat_i = 1;

                            while ( $loop->have_posts() ) : $loop->the_post();
                        ?>

                            <div class="col-12 col-md-4 pt-5 text-secondary text-left">
                                <h5 class="text-uppercase">
                                    <?php echo the_title(); ?>
                                </h5>
                                <span class="separator"></span>
                                <?php if( have_rows('questions') ): ?>
                                    <div id="kb-cat-<?php echo $cat_i; ?>" role="tablist">
                                        <?php $i=1; while ( have_rows('questions') && $i < 6) : the_row(); ?>
                                            <div class="card border-top-0 border-left-0 border-right-0 bg-alt">
                                                <div class="card-header border-0 px-0 bg-alt" role="tab" id="heading-<?php echo the_sub_field('cat') + '-' + $i; ?>">
                                                    <span class="mb-0">
                                                        <a data-toggle="collapse" href="#collapse-<?php echo the_sub_field('cat') + '-' + $i; ?>" aria-expanded="false" aria-controls="<?php echo the_sub_field('cat') + '-' + $i; ?>">
                                                            <i class="icon ion-document align-middle pr-2" aria-hidden="true"></i>
                                                            <?php the_sub_field('question') ?>
                                                        </a>
                                                    </span>
                                                </div>
                                                <div id="collapse-<?php echo the_sub_field('cat') + '-' + $i; ?>" class="collapse" role="tabpanel" data-parent="#kb-cat-<?php echo $cat_i; ?>" aria-labelledby="heading-<?php echo the_sub_field('cat') + '-' + $i; ?>">
                                                    <div class="card-body px-0 pt-0">
                                                        <?php the_sub_field('answer') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $i++; endwhile; ?>
                                    </div>

                                <?php else:  ?>
                                    <h5>
                                        no questions here
                                    </h5>
                                <?php endif; ?>
                                <div class="mt-2">
                                    <a href="/knowledge-base/#kb-cat-<?php echo $cat_i ?>">                                    
	                                    View all
                                        <i class="icon ion-arrow-right-c"></i>
                                    </a>
                                </div>
                            </div>
                        <?php
                            $cat_i++;
                            endwhile;
                            wp_reset_postdata();
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(home);
