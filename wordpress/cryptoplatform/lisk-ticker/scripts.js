var datakey = 'usd';
var lastkey = 'month';
var zerokey = 'notzeroed';
var currencykey = '$';
var myjson = '';
var mydata;
var currentPrice = {};

window.onload = function() {
	connectPrice();
}

function connectPrice() {
    var ws = new WebSocket("wss://www.liskpending.nl:8765/service");

    ws.onopen = function () {
        ws.send('currentPrice');
    };

    ws.onmessage = function (evt) {
        myjson = evt.data;
        ws.close();
    };

    ws.onclose = function () {
        mydata = JSON.parse(myjson);
        currentPrice.usd = Number(mydata.hourUSD[mydata.hourUSD.length - 1]).toLocaleString(
            undefined, // use a string like 'en-US' to override browser locale
            { minimumFractionDigits: 2 }
        );
        currentPrice.btc = mydata.hourBTC[mydata.hourBTC.length - 1];
        currentPrice.mrk = Number(mydata.hourMRK[mydata.hourMRK.length - 1]).toLocaleString(
            undefined, // use a string like 'en-US' to override browser locale
            { minimumFractionDigits: 0 }
        );

        var test2 = Number(currentPrice.mrk);
        var testvalue = test2.toLocaleString(
            undefined, // use a string like 'en-US' to override browser locale
            { minimumFractionDigits: 0 }
          );

        if(datakey =='usd')
        {
            LiskPriceHead.innerHTML = "Lisk Price - USD: $" + currentPrice.usd;
            LiskPriceSub.innerHTML = "BTC: \u0243" + currentPrice.btc + " &nbsp; Mrk Cap: $" + currentPrice.mrk;
        }
        else if(datakey == 'btc')
        {
            LiskPriceHead.innerHTML = "Lisk Price - BTC: \u0243" + currentPrice.btc;
            LiskPriceSub.innerHTML = "USD: $" + currentPrice.usd + " &nbsp; Mrk Cap: $" + currentPrice.mrk;
        }
        else if(datakey == 'mc')
        {
            LiskPriceHead.innerHTML = "Lisk Price - Mrk Cap: $" + currentPrice.mrk;
            LiskPriceSub.innerHTML = "USD: $" + currentPrice.usd + " &nbsp; BTC: \u0243" + currentPrice.btc;
        }
        //LiskPriceSub.innerHTML = "USD: $" + currentPrice.usd + " &nbsp; BTC: \u0243" + currentPrice.btc + " &nbsp; Mrk Cap: $" + currentPrice.mrk;
        createChart();
    };
};

function changeData(){

    datakey = document.getElementById('dataDropDown').value;

    if(datakey == 'btc')
    {
        currencykey = '\u0243';
    }
    else
    {
        currencykey = '$';
    }

    if(datakey =='usd')
    {
        LiskPriceHead.innerHTML = "Lisk Price - USD: $" + currentPrice.usd;
        LiskPriceSub.innerHTML = "BTC: \u0243" + currentPrice.btc + " &nbsp; Mrk Cap: $" + currentPrice.mrk;
    }
    else if(datakey == 'btc')
    {
        LiskPriceHead.innerHTML = "Lisk Price - BTC: \u0243" + currentPrice.btc;
        LiskPriceSub.innerHTML = "USD: $" + currentPrice.usd + " &nbsp; Mrk Cap: $" + currentPrice.mrk;
    }
    else if(datakey == 'mc')
    {
        LiskPriceHead.innerHTML = "Lisk Price - Mrk Cap: $" + currentPrice.mrk;
        LiskPriceSub.innerHTML = "USD: $" + currentPrice.usd + " &nbsp; BTC: \u0243" + currentPrice.btc;
    }

    chartChange();
}

function changeLast(){
    lastkey = document.getElementById('lastDropDown').value;
    chartChange();
}

function changeZero(){
    zerokey = document.getElementById('zeroedDropDown').value;
    chartChange();
}

function chartChange(){
    var data = window.chart.config.data;
    data.datasets[0].label = currencykey;

    if(datakey == 'usd')
    {
        if(lastkey == 'hour')
        {
            data.datasets[0].data = mydata.hourUSD;
            data.labels = mydata.hourLabels;
        }
        else if(lastkey == 'day')
        {
            data.datasets[0].data = mydata.dayUSD;
            data.labels = mydata.dayLabels;
        }
        else if(lastkey == 'week')
        {
            data.datasets[0].data = mydata.weekUSD;
            data.labels = mydata.weekLabels;
        }
		else if(lastkey == 'month')
        {
            data.datasets[0].data = mydata.monthUSD;
            data.labels = mydata.monthLabels;
        }
    }
    else if(datakey == 'btc')
    {
        if(lastkey == 'hour')
        {
            data.datasets[0].data = mydata.hourBTC;
            data.labels = mydata.hourLabels;
        }
        else if(lastkey == 'day')
        {
            data.datasets[0].data = mydata.dayBTC;
            data.labels = mydata.dayLabels;
        }
        else if(lastkey == 'week')
        {
            data.datasets[0].data = mydata.weekBTC;
            data.labels = mydata.weekLabels;
        }
		else if(lastkey == 'month')
        {
            data.datasets[0].data = mydata.monthBTC;
            data.labels = mydata.monthLabels;
        }
    }
    else if(datakey == 'mc')
    {
        if(lastkey == 'hour')
        {
            data.datasets[0].data = mydata.hourMRK;
            data.labels = mydata.hourLabels;
        }
        else if(lastkey == 'day')
        {
            data.datasets[0].data = mydata.dayMRK;
            data.labels = mydata.dayLabels;
        }
        else if(lastkey == 'week')
        {
            data.datasets[0].data = mydata.weekMRK;
            data.labels = mydata.weekLabels;
        }
		else if(lastkey == 'month')
        {
            data.datasets[0].data = mydata.monthMRK;
            data.labels = mydata.monthLabels;
        }
    }

    var options = window.chart.config.options;
    if(zerokey == 'zeroed')
    {
        options.scales.yAxes[0].ticks.beginAtZero = true;
    }
    else
    {
        options.scales.yAxes[0].ticks.beginAtZero = false;
    }
    //data.datasets[1].data = ind_dataset;
    //data.labels = chart_labels;
    window.chart.update();
}

function createChart()
{
    Chart.defaults.global.pointHitDetectionRadius = 1;

    var customTooltips = function(tooltip) {
	    // Tooltip Element
	    var tooltipEl = document.getElementById('chartjs-tooltip');

	    if (!tooltipEl) {
	        tooltipEl = document.createElement('div');
	        tooltipEl.id = 'chartjs-tooltip';
	        tooltipEl.innerHTML = "<table></table>"
	        this._chart.canvas.parentNode.parentNode.parentNode.appendChild(tooltipEl);
	    }

	    // Hide if no tooltip
	    if (tooltip.opacity === 0) {
	        tooltipEl.style.opacity = 0;
	        LiskPriceTooltipSub.innerHTML ="";
	        return;
	    }

	    // Set caret Position
	    tooltipEl.classList.remove('above', 'below', 'no-transform');
	    if (tooltip.yAlign) {
	        tooltipEl.classList.add(tooltip.yAlign);
	    } else {
	        tooltipEl.classList.add('no-transform');
	    }

	    function getBody(bodyItem) {
	        return bodyItem.lines;
	    }

	    // Set Text
		    if (tooltip.body) {
		    var titleLines = tooltip.title || [];
		    var bodyLines = tooltip.body.map(getBody);

		    var titlePH;
		    titleLines.forEach(function(title) {
		        titlePH = title;
		    });

		    var bodystr;
		    var bodytest;
		    var span;

		    bodyLines.forEach(function(body, i) {
		        var colors = tooltip.labelColors[i];
		        var style = 'background:' + colors.backgroundColor;
		        style += '; border-color:' + colors.borderColor;
		        style += '; border-width: 2px';
		        span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
		        bodystr = "" + body;
		        bodystr = bodystr.replace(": ", "");

		        if(datakey == 'usd')
		        {
		            bodytest = Number(bodystr.substring(1)).toLocaleString(
		                undefined, // use a string like 'en-US' to override browser locale
		                { minimumFractionDigits: 2 }
		        	);
		        }
		        else if(datakey == 'btc')
		        {
		            bodytest = Number(bodystr.substring(1)).toLocaleString(
		                undefined, // use a string like 'en-US' to override browser locale
		                { minimumFractionDigits: 8 }
		        	);
		        }
		        else if(datakey == 'mc')
		        {
		            bodytest = Number(bodystr.substring(1)).toLocaleString(
		                undefined, // use a string like 'en-US' to override browser locale
		                { minimumFractionDigits: 0 }
		        	);
		        }

		    });
	    }

	    LiskPriceTooltipSub.innerHTML = titlePH + ": " + bodystr.substring(0, 1) + bodytest;
	};

	var canvas = document.getElementById("coin-ticker");

	// Apply multiply blend when drawing datasets
	var multiply = {
	  beforeDatasetsDraw: function(chart, options, el) {
	    chart.ctx.globalCompositeOperation = 'multiply';
	  },
	  afterDatasetsDraw: function(chart, options) {
	    chart.ctx.globalCompositeOperation = 'source-over';
	  },
	};

	// Gradient color - this week
	var gradient = canvas.getContext('2d').createLinearGradient(0, 0, 0, 150);
	gradient.addColorStop(0, '#23074d');
	gradient.addColorStop(1, '#26D0CE');

	var config = {
	    type: 'line',
	    data: {
	        //labels: ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
	        labels: mydata.monthLabels,
	        datasets: [
	          {
	              label: '$',
	              data: mydata.monthUSD,
	              backgroundColor: gradient,
	              //borderColor: 'transparent',
	              pointBackgroundColor: '#000000',
	              pointBorderColor: '#FFFFFF',
	              pointHoverRadius: 3,
	              lineTension: 0.40,
	          }
	        ]
	    },
	    options: {
	    elements: {
	        point: {
	          radius: 0,
	          hitRadius: 5,
	            hoverRadius: 5
	          }
	        },

		tooltips: {
			enabled: false,
			mode: 'index',
			intersect: false,
			custom: customTooltips
		},

	    legend: {
	        display: false,
	        },
	        scales: {
	            xAxes: [{
	            display: false,
	            }],
	            yAxes: [{
	            display: false,
	                ticks: {
	                beginAtZero: false,
	              },
	            }]
	        }
	    },
	    plugins: [multiply],
	};

	window.chart = new Chart(canvas, config);
}
