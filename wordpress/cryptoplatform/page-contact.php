<?php
/**
 * Template Name: Contact
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */


get_header(); ?>
    <!-- Sections -->
    <section class="pb-5 alt-background mt-n5">
        <div class="container">
            <div class="row">
				<div class="col-12">
                    <div class="card super-card card-raised mb-5">
                        <div class="card-body pb-2 pb-md-5 px-md-5">
							<p class="lead comp mt-3"><small>Please fill in all the fields below.</small></p>
								<?php the_field('contact_form'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>		
</section>
<?php get_footer();