<?php
/**
* Template Name: Delegate ranks
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
get_header(); ?>
    <style>
    canvas{
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
<script type="text/javascript">
	jQuery(window).load(function () {
  		getActiveDelegates()
	});
</script>
<!-- Sections -->
<section class="pb-5 alt-background mt-n5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card super-card card-raised mb-5">
					<div class="card-body pb-2 pb-md-5 px-md-5">
						<div id="numDaysDiv">
							<p>Enter timespan (in days): <br />
							</p>
							<input class="form-control-custom" type="number" name="numDays" id="numDays" autocomplete="on"><br /><br />
							<button class="btn btn-primary" id="btnDelegateRankCharts" onclick="connectDelegateRanks()" style="display:none;">Start</button></div><br>
							<div id="delegateRankChartDiv" class="chart-container" style="position: relative; ">
								<canvas id="canvas"></canvas>
							</div>
					</div>
				</div>
			</div>
		</div>
</div>
</section>
<?php get_footer();