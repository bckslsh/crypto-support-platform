<?php
/**
 * Template Name: Upcoming events
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Sections -->
<section class="pb-5 alt-background mt-n5">
    <div class="container">
		<div class="row events">
			<div class="col-md-12">
				<ul class="timeline">
					<?php $args = array( 'post_type' => 'upcomingevents', 'posts_per_page' => 20, 'meta_key' => 'event_date',
						'orderby'	=> 'meta_value_num', 'order' => 'ASC');
					$loop = new WP_Query( $args );
					while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
					<li class="timeline-item">
						<div class="timeline-badge"></div>
						<div
						    class="card h-100 card-raised timeline-panel"
						    <?php if($loop->current_post % 2 == 0) {?>
						        data-aos="fade-right"
						    <?php } else { ?>
						        data-aos="fade-left"
						    <?php } ?>
						    data-aos-anchor-placement="center-bottom"
						    data-aos-easing="ease-in-out-cubic"
						    data-aos-duration="600"
						    data-aos-once="true"
						>
	                    	<div class="card-img-top" style="background-image: url('<?php if($image = get_field('event_image')): echo $image['sizes']['large']; endif; ?>');"></div>
	                        <div class="card-body px-5 pt-5 pb-3">
	                            <div class="card-subtitle mb-2">
									<i class="ion-ios-calendar-outline pr-2"></i>
										<small class="text-muted">
											<?php 
											// get raw date
											$date = get_field('event_date', false, false);


											// make date object
											$date = new DateTime($date);

											?>
											<?php echo $date->format('j M Y'); ?>
										</small>
	                            </div>
	                            <h1 class="card-title my-4 primary">
	                                <a href="<?php the_field('event_url'); ?>">
	                                    <?php the_title() ?>
	                                </a>
	                            </h1>
									Validation:
								<?=function_exists('thumbs_rating_getlink') ? thumbs_rating_getlink() : ''?>
								<p class="card-text">
	                                <?php the_content(); ?>
	                            </p>
	                        </div>
	                    </div>
                    </li>
					<?php endwhile; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
    <?php get_footer();