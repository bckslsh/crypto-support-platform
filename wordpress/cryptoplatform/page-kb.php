<?php
/**
 * Template Name: Knowledge Base
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
    <!-- Sections -->
    <section class="alt-background">
        <div class="container">
            <div class="row">
                <div class="d-none d-md-block col-md-3 pt-5 pb-5">
                    <div id="sticky-menu">
                        <ul class="nav nav-pills flex-column pt-5" id="sticky-menu-menu">
                            <?php
                                $args = array( 'post_type' => 'knowledgebase', 'order' => 'ASC');
                                $loop = new WP_Query( $args );
                                $cat_i = 1;

                                while ( $loop->have_posts() ) : $loop->the_post();
                            	?>
                                <li class="nav-item primary">
                                    <a href="#kb-<?php the_field('kb_slug') ?>" class="nav-link">
                                        <h6 class="primary"><?php echo the_title(); ?></h6>
                                    </a>
                                    <?php if( have_rows('questions') ): ?>
                                        <ul class="nav nav-pills flex-column">
                                            <?php $i=1; while ( have_rows('questions') ) : the_row(); ?>
                                                <li class="nav-item primary">
                                                    <a href="#kb-<?php the_field('kb_slug') ?>-<?php echo $i; ?>" class="nav-link ml-3">
                                                        <?php the_sub_field('question') ?>
                                                    </a>
                                                </li>
                                            <?php $i++; endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            <?php
                                $cat_i++;
                                endwhile;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-9 mt-n5">
					<?php
						$args = array( 'post_type' => 'knowledgebase', 'order' => 'ASC');
						$loop = new WP_Query( $args );
						$cat_i = 1;

						while ( $loop->have_posts() ) : $loop->the_post();
					?>
	                    <div class="card card-raised mb-5">
	                        <div class="card-header bg-white border-0 text-center mt-3">
	                            <h1 class="text-uppercase primary mt-2 scrollspy-title" id="kb-<?php the_field('kb_slug') ?>">
	                                <?php echo the_title(); ?>
	                            </h1>
								<p class="lead comp ml-4"><small><?php the_field('kb_subhead'); ?></small></p>
	                        </div>
	                        <div class="card-body mx-md-5">
	                            <?php if( have_rows('questions') ): ?>
	                                <div role="tablist" id="kb-cat-<?php the_field('kb_slug') ?>-parent">
	                                    <?php $i=1; while ( have_rows('questions') ) : the_row(); ?>
	                                        <div class="card border-0">
	                                            <div class="card-header bg-white border-0 px-0" role="tab" id="heading-<?php echo the_sub_field('cat') + '-' + $i; ?>">
	                                                <h6 class="primary mb-0 pr-2 scrollspy-title" id="kb-<?php the_field('kb_slug') ?>-<?php echo $i; ?>">
	                                                    <a class="primary" data-toggle="collapse" href="#collapse-<?php echo the_sub_field('cat') + '-' + $i; ?>" aria-expanded="false" aria-controls="<?php echo the_sub_field('cat') + '-' + $i; ?>">
															<i class="icon ion-document pr-2"></i>
	                                                        <?php the_sub_field('question') ?>
	                                                    </a>
	                                                </h6>
	                                            </div>
	                                            <div id="collapse-<?php echo the_sub_field('cat') + '-' + $i; ?>" class="collapse kb-collapse" role="tabpanel" data-parent="#kb-cat-<?php echo $cat_i; ?>-parent" aria-labelledby="heading-<?php echo the_sub_field('cat') + '-' + $i; ?>">
	                                                <div class="card-body px-0 pt-0">
														<?php the_sub_field('answer') ?>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    <?php $i++; endwhile; ?>
	                                </div>

								<?php else:  ?>
									<h5>
										no questions here
									</h5>
	                            <?php endif; ?>
	                        </div>
	                    </div>
					<?php
						$cat_i++;
						endwhile;
					?>
                </div>

            </div>
        </div>
    </section>
    </div>
    <?php get_footer();
