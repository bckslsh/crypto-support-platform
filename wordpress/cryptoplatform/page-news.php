<?php
/**
 * Template Name: news
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

    <!-- Sections -->
    <section class="pb-5 alt-background">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 col-xl-9">
                    <div class="row news-list mt-n5">
                        
                        <?php
                        global $paged; 

                        // If post/page isn't paginated
                        if ($paged === 0) {?>
                        
                        <!-- // Define our WP Query Parameters -->
                        <?php $the_query = new WP_Query( 'posts_per_page=1' ); ?>
                
                        <!-- // Start our WP Query -->
                        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                            <div class="col-12">
                                <div class="card super-card card-raised mb-4">
                                    <div class="card-img-top" style="background-image: url('<?php the_post_thumbnail_url('full') ?>');"></div>
                                    <div class="card-body px-5 pt-5 pb-3">
                                        <div class="card-subtitle mb-2">
                                            <span class="text-info"><?php the_category( ', ' ); ?></span>
                                            <small class="text-muted">
                                                <?php echo get_the_date(); ?>
                                            </small>
                                        </div>
                                        <div class="card-author text-muted"><i class="icon ion-ios-person"></i> <?php the_author(); ?>
                                        </div>
                                        <h3 class="card-title my-4">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="card-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                    </div>
                                    <div class="card-footer px-5 pb-5 bg-white border-0">
                                        <a href="<?php the_permalink() ?>">
                                            continue to read
                                            <i class="icon ion-arrow-right-c"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        <!-- // Repeat the process and reset once it hits the limit -->
                        <?php endwhile;
                            wp_reset_postdata();
                        ?>

                        <?php
                            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                            $query = new WP_Query( array(
                                'posts_per_page' => 8,
                                'offset' => 1,
                                'paged' => $paged
                            ) );
                        ?>	
                        
                        <?php if ( $query->have_posts() ) : ?>
                        
                        <!-- begin loop -->
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>					
                            <div class="col-12 col-md-6 my-3">
                                <div class="card h-100 card-raised">
                                    <div class="card-img-top" style="background-image: url('<?php the_post_thumbnail_url('large') ?>');"></div>
                                    <div class="card-body px-5 pt-5 pb-3">
                                        <div class="card-subtitle mb-2">
                                            <span class="text-info"><?php the_category( ', ' ); ?></span>
                                            <small class="text-muted">
                                                <?php echo get_the_date(); ?>
                                            </small>
                                        </div>
                                        <div class="card-author text-muted">
                                            <i class="icon ion-ios-person"></i> <?php the_author(); ?>
                                        </div>
                                        <h3 class="card-title my-4">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title() ?>
                                            </a>
                                        </h3>
                                        <p class="card-text">
                                            <?php the_excerpt(); ?>
                                        </p>
                                    </div>
                                    <div class="card-footer px-5 pb-5 bg-white border-0">
                                        <a href="<?php the_permalink() ?>">
                                            continue to read
                                            <i class="icon ion-arrow-right-c"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>					
                        <?php endwhile; ?>
                        <!-- end loop -->

                         <div class="col-12 my-3">
                            <div class="pagination">
                                <?php 
                                    $pages = paginate_links( array(
                                        'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                        'total'        => $query->max_num_pages,
                                        'current'      => max( 1, get_query_var( 'paged' ) ),
                                        'format'       => '?paged=%#%',
                                        'show_all'     => false,
                                        'type'         => 'plain',
                                        'end_size'     => 2,
                                        'mid_size'     => 1,
                                        'prev_next'    => true,
                                        'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                        'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                        'add_args'     => false,
                                        'add_fragment' => '',
                                    ) );
                                    echo($pages);
                                ?>
                            </div>
                        </div>
                        
                        <?php wp_reset_postdata(); ?>					
                        <?php else : ?>
                            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                        <?php endif; ?>

                        <?php
                        }

                        // If post/page is paginated
                        else { ?>						
                            <!-- query -->
                            <?php
                                $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                                $query = new WP_Query( array(
                                    'posts_per_page' => 8,
                                    'paged' => $paged
                                ) );
                            ?>					
                            <?php if ( $query->have_posts() ) : ?>					
                            <!-- begin loop -->
                            <?php while ( $query->have_posts() ) : $query->the_post(); ?>					
                                <div class="col-12 col-md-6 my-3">
                                        <div class="card h-100 card-raised">
                                            <div class="card-img-top" style="background-image: url('<?php the_post_thumbnail_url('large') ?>');"></div>
                                            <div class="card-body px-5 pt-5 pb-3">
                                                <div class="card-subtitle mb-2">
                                                    <span class="text-info"><?php the_category( ', ' ); ?></span>
                                                    <small class="text-muted">
                                                        <?php echo get_the_date(); ?>
                                                    </small>
                                                </div>
                                                <div class="card-author text-muted">
                                                    <i class="icon ion-ios-person"></i> <?php the_author(); ?>
                                                </div>
                                                <h3 class="card-title my-4">
                                                    <a href="<?php the_permalink() ?>">
                                                        <?php the_title() ?>
                                                    </a>
                                                </h3>
                                                <p class="card-text">
                                                    <?php the_excerpt(); ?>
                                                </p>
                                            </div>
                                            <div class="card-footer px-5 pb-5 bg-white border-0">
                                                <a href="<?php the_permalink() ?>">
                                                    continue to read
                                                    <i class="icon ion-arrow-right-c"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>					
                            <?php endwhile; ?>
                            <!-- end loop -->

                            <div class="col-12 my-3">
                                <div class="pagination">
                                    <?php 
                                        echo paginate_links( array(
                                            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                                            'total'        => $query->max_num_pages,
                                            'current'      => max( 1, get_query_var( 'paged' ) ),
                                            'format'       => '?paged=%#%',
                                            'show_all'     => false,
                                            'type'         => 'plain',
                                            'end_size'     => 2,
                                            'mid_size'     => 1,
                                            'prev_next'    => true,
                                            'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
                                            'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
                                            'add_args'     => false,
                                            'add_fragment' => '',
                                        ) );
                                    ?>
                                </div>
                            </div>			
                            <?php wp_reset_postdata(); ?>					
                            <?php else : ?>
                                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                            <?php endif; ?>
                            <!-- end query -->
                        <?php } ?>

                    </div>
                </div>			
                <div class="col-12 col-lg-4 col-xl-3 mt-lg-5">
                <div class="card card-raised mb-5">
                    <div class="card-body bloodhound">
                        <form action="/" method="get" class="form-inline">
                            <label class="sr-only" for="search">Search</label>
                            <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" class="typeahead hero-search-box" placeholder="Search">
                            <input type="submit" alt="Search" src="<?php bloginfo( 'template_url' ); ?>" value="&#xf4a5;" class="hero-search-icon">
                        </form>
                    </div>
                </div>
                <div class="AdSidebar card card-raised mb-5" data-ad-id="sidebarAdd" data-ad-visibility="d-none d-md-block">
                        <button class="btn btn-link text-dark close-ad" data-ad-target="sidebarAdd">
                            <i class="ion-close-circled"></i>
                        </button>
                        <div class="card-body">
                            <a href="https://moracle.network/" target="_blank" class="advertisement">
                                <div class="d-flex align-items-center flex-column">
                                    <div class="text-center">
                                        <img src="/wp-content/themes/cryptoplatform/assets/images/mrcl.png" class="adimg">
                                    </div>
                                    <div class="text-black text-center adtext mt-3 mb-4">
                                        <p class="mb-0">
                                            Smarter blockchain applications need smarter oracles.
                                        </p>
                                    </div>
                                    <div class="text-center">
                                        <button value="LEARN MORE" class="btn btn-primary-ad text-center text-dark">
                                            LEARN MORE
                                        </button>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>								
                    <div class="card card-raised">
                        <div class="card-header bg-white border-0">
                            <h6 class="text-uppercase primary mt-2">
                                <i class="ion-email-unread pr-2"></i>Subscribe
                            </h6><br />
                            <p class="lead comp"><small>Receive a notification per mail whenever there's a new post</small></p>
                        </div>
                        <div class="card-body">
                            <?php echo do_shortcode( '[contact-form-7 id="279" title="Subscribe news"]' ); ?>
                        </div>
                    </div>				
                    <div class="card card-raised mt-5">
                        <div class="card-header bg-white border-0">
                            <h6 class="text-uppercase primary mt-2">
                                <i class="ion-ios-paper-outline pr-2"></i>More posts
                            </h6>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled">

                                <!-- // Define our WP Query Parameters -->
                                <?php
	                                $the_query = new WP_Query( array(
                                    'posts_per_page' => 5,
                                    'offset' => 9
                                ) );
                                ?>
              
                                
                                <!-- // Start our WP Query -->
                                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                                <li class="mb-3">
                                    <a href="<?php the_permalink() ?>">
                                            <?php the_title() ?>
                                    </a>
                                    <div class="text-muted">
                                        <small>
                                            <?php echo get_the_date(); ?>
                                        </small>
                                    </div>
                                </li>

                                <?php 
                                    endwhile;
                                    wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="card card-raised mt-5">
                        <div class="card-header bg-white border-0">
                            <h6 class="text-uppercase primary mt-2">
                                <i class="ion-android-list pr-2"></i>Categories
                            </h6>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled">
                                <li>
                                    <?php wp_list_cats(); ?>
                                </li>
                            </ul>
                        </div>
                    </div>				
                </div>
            </div>
        </div>
    </section>

<?php get_footer();
