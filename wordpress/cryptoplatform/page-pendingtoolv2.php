<?php
/**
* Template Name: Lisk pendingv2
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
get_header(); ?>
<!-- Sections -->
<section class="pb-5 alt-background mt-n5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card super-card card-raised mb-5">
					<div class="card-body pb-2 pb-md-5 px-md-5">
						<p class="lead comp mt-3">
							<iframe name="myiframe" style="display:none" src="https://www.lisk.support"></iframe>
							<form target="myiframe" action="https://www.lisk.support">
								<div id="LSKAddress"><p>Please enter your Lisk Address</p>
								<input type="text" id="LSKAddressBox" autocomplete="on" class="form-control-custom" />
							</div><br />
							<button class="btn btn-md btn-primary" id="startButton" type="submit" onclick="connect()">Submit</button>
							<div id="InvalidAddress" style="display:none">
								<p>Invalid Address</p>
							</div>
							<div id="Calculating" style="display:none">Calculating... This may take a minute or two...<br />
								<h5 id="percentPara" class="primary" style="display:none"></h5><div class="spinner-grow"></div>
							</div>
							<button class="btn btn-md btn-primary" id="reloadButton" style="display:none" type="button" onclick="reload()">
							Enter a New Address
							</button><br />
							<h5 id="totalPendingPara" class="primary" style="display:none"></h5>
						<table class="table table-responsive" id="tbl" style="display:none"></table>
						<h5 id="noToolPara" class="primary" style="display:none"></h5>
						<p id="noToolList" style="display:none"></p>
					</form>
					<div id="accordion1" class="accordion">
						<div id="pendingDropdowns" class="card m-b-0"></div>
					</div>
					<br>
					<p id="nonForgersPara">Forgers who give individual rewards, but have no online tool we can use to calculate pending payouts: </p>
					<div id="accordion2" class="accordion">
						<div id="nonPendingDropdowns" class="card m-b-0"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<?php get_footer();