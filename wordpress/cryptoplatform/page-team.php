<?php
/**
 * Template Name: Team
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<!-- Sections -->
<section class="pb-5 alt-background mt-n5">
    <div class="container">
        <div class="row">
            <?php if( have_rows('team_member') ): ?>
                <?php while( have_rows('team_member') ): the_row(); ?>
                    <div class="col-12 col-md-4 mb-4">
                        <div class="card card-raised card-raised-hover h-100">
                            <div class="card-body team">
                                <img class="rounded-circle mb-4 team-image" src="<?php echo get_sub_field('team_member_image'); ?>">
                                <h4>
                                    <?php echo get_sub_field('team_member_name'); ?>
                                </h4>
                                <p>
                                    <span class="emphasis">
                                        <?php echo get_sub_field('team_member_role'); ?>
                                    </span>
                                </p>
                                <p>
                                    <?php echo get_sub_field('team_member_description'); ?>
                                </p>
                                <ul class="list-inline">
                                    <?php if( get_sub_field('team_member_linkedin') ): ?>
                                        <li class="list-inline-item">
                                            <a href="<?php echo get_sub_field('team_member_linkedin'); ?>" target="_blank" title="LinkedIn" class="social-circle">
                                                <i class="icon ion-social-linkedin"></i>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('team_member_twitter') ): ?>
                                        <li class="list-inline-item">
                                            <a href="<?php echo get_sub_field('team_member_twitter'); ?>" target="_blank" title="Twitter" class="social-circle">
                                                <i class="icon ion-social-twitter"></i>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('team_member_facebook') ): ?>
                                        <li class="list-inline-item">
                                            <a href="<?php echo get_sub_field('team_member_facebook'); ?>" target="_blank" title="Facebook" class="social-circle">
                                                <i class="icon ion-social-facebook"></i>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('team_member_instagram') ): ?>
                                        <li class="list-inline-item">
                                            <a href="<?php echo get_sub_field('team_member_instagram'); ?>" target="_blank" title="Instagram" class="social-circle">
                                                <i class="icon ion-social-instagram"></i>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('team_member_ext_link') ): ?>
                                        <li class="list-inline-item">
                                            <a href="<?php echo get_sub_field('team_member_ext_link'); ?>" target="_blank" title="External link" class="social-circle">
                                                <i class="icon ion-link"></i>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer();