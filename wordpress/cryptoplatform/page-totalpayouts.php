<?php
/**
* Template Name: Total payouts
*
* This is the most generic template file in a WordPress theme
* and one of the two required files for a theme (the other being style.css).
* It is used to display a page when nothing more specific matches a query.
* E.g., it puts together the home page when no home.php file exists.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package WordPress
* @subpackage Twenty_Seventeen
* @since 1.0
* @version 1.0
*/
get_header(); ?>
<!-- Sections -->
<section class="pb-5 alt-background mt-n5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card super-card card-raised mb-5">
					<div class="card-body pb-2 pb-md-5 px-md-5">
						<p class="lead comp mt-3">
							<button class="btn btn-md btn-primary" id="reloadButton" style="display:none" type="button" onclick="reload()">ENTER A NEW ADDRESS</button>
							<div onload="overallMonthlyChart()">
								<iframe name="myiframe" style="display:none" src="https://www.lisk.support"></iframe>
								<div align="center" id='totalPayoutInfo' style="display:none">
										<p class="overallPayoutWindow" id="totalPayouts">
											<div class="overallPayoutWindow" id="alldelegates"></div>
											<div class="overallPayoutWindow" id="voted"></div>
											<div class="overallPayoutWindow" id="notvoted"></div>
										</p>

										<div id="overallMonthlyChartDiv" class="totalPayoutChartDiv" align="center"><div id="omcd" class="totalPayoutChartDiv"><canvas id="overallMonthlyChart"></canvas></div></div>
										<div id="overallChartDiv" class="totalPayoutChartDiv" align="center"><canvas id="overallChart"></canvas></div>
										<div id="delegateChartDiv" class="totalPayoutChartDiv" align="center"><canvas id="delegateChart"></canvas></div>
										<br>
										<p id="MyButtons">
											<button id="overallMonthlyBtn" class="button DataTypes" type="button" onclick="overallMonthlyChart()">Overall - By Month</button>
											<button id="overallBtn" class="button DataTypes" type="button" onclick="overallChart()">Overall - By Delegate</button>
											<button id="indDelegatesBtn" class="button DataTypes" type="button" onclick="IndDelegates()">Show/Hide Individual Delegates</button>
										</p>
										<br>
									<div id="MonthlyButtons">
										Pools: <button id='OverallAllMonthlyChart' class='button MonthlyButton MonthlyPools' type='button' onclick="OverallAllMonthlyChart()">All</button>
										<button id='OverallDelMonthlyChart' class='button MonthlyButton MonthlyPools' type='button' onclick="OverallDelMonthlyChart()">Groups - Without Ind GDT</button>
										<button id='OverallDelPMonthlyChart' class='button MonthlyButton MonthlyPools' type='button' onclick="OverallDelPMonthlyChart()">Groups - With Ind GDT</button>
										<button id='OverallIndMonthlyChart' class='button MonthlyButton MonthlyPools' type='button' onclick="OverallIndMonthlyChart()">Individual</button><br>
										Chart Type: <button id='OverallBarMonthlyChart' class='button MonthlyButton MonthlyChartType MonthlyType' type='button' onclick="OverallBarMonthlyChart()">Bar</button>
										<button id='OverallHBarMonthlyChart' class='button MonthlyButton MonthlyChartType MonthlyType' type='button' onclick="OverallHBarMonthlyChart()">Horizontal</button><br>
										Stacked Data: <button id='OverallStackedMonthlyChart' class='button MonthlyButton MonthlyChartType Stacked' type='button' onclick="OverallStackedMonthlyChart()">Stacked</button>
										<button id='OverallNonStackedMonthlyChart' class='button MonthlyButton MonthlyChartType Stacked' type='button' onclick="OverallNonStackedMonthlyChart()">Non-Stacked</button><br>
										Data Range: <button id='OverallRangeAll' class='button MonthlyButton MonthlyDataRange' type='button' onclick="OverallRangeAll()">All</button>
										<button id='OverallRange12Months' class='button MonthlyButton MonthlyDataRange' type='button' onclick="OverallRange12Months()">Last Year</button>
										<button id='OverallRange6Months' class='button MonthlyButton MonthlyDataRange' type='button' onclick="OverallRange6Months()">Last 6 Months</button>
										<button id='OverallRange3Months' class='button MonthlyButton MonthlyDataRange' type='button' onclick="OverallRange3Months()">Last 3 Months</button><br>
										Voting: <button id='MonthlyOnlyVoting' class='button MonthlyButton MonthlyVoting' type='button' onclick="MonthlyOnlyVoting()">Currently Voting</button>
										<button id='MonthlyNotVoting' class='button MonthlyButton MonthlyVoting' type='button' onclick="MonthlyNotVoting()">Not Currently Voting</button>
										<button id='MonthlyBothVoting' class='button MonthlyButton MonthlyVoting' type='button' onclick="MonthlyBothVoting()">Both</button>
									</div>
									
									<div id="OverallButtons" style="display:none">
										Pools: <button id='OverallAllChart' class='button Pools' type='button' onclick="OverallAllChart()">All</button>
										<button id='OverallDelChart' class='button Pools' type='button' onclick="OverallDelChart()">Groups - Without Ind GDT</button>
										<button id='OverallDelPChart' class='button Pools' type='button' onclick="OverallDelPChart()">Groups - With Ind GDT</button>
										<button id='OverallIndChart' class='button Pools' type='button' onclick="OverallIndChart()">Individual</button><br>
										Chart Type: <button id='OverallPieChart' class='button ChartTypes' type='button' onclick="OverallPieChart()">Pie</button>
										<button id='OverallDonChart' class='button ChartTypes' type='button' onclick="OverallDonChart()">Doughnut</button>
										<button id='OverallBarChart' class='button ChartTypes' type='button' onclick="OverallBarChart()">Bar</button>
										<button id='OverallHBarChart' class='button ChartTypes' type='button' onclick="OverallHBarChart()">Horizontal</button><br>
										Data Range: <button id='OverallRangeAll2' class='button DataRange' type='button' onclick="OverallRangeAll2()">All</button>
										<button id='OverallRangeLast365Days' class='button DataRange' type='button' onclick="OverallRangeLast365Days()">Last 365 Days</button>
										<button id='OverallRangeLast180Days' class='button DataRange' type='button' onclick="OverallRangeLast180Days()">Last 180 Days</button>
										<button id='OverallRangeLast90Days' class='button DataRange' type='button' onclick="OverallRangeLast90Days()">Last 90 Days</button>
										<button id='OverallRangeLast60Days' class='button DataRange' type='button' onclick="OverallRangeLast60Days()">Last 60 Days</button>
										<button id='OverallRangeLast30Days' class='button DataRange' type='button' onclick="OverallRangeLast30Days()">Last 30 Days</button><br>
										Voting: <button id='OverallOnlyVoting' class='button OverallVoting' type='button' onclick="OverallOnlyVoting()">Currently Voting</button>
										<button id='OverallNotVoting' class='button OverallVoting' type='button' onclick="OverallNotVoting()">Not Currently Voting</button>
										<button id='OverallBothVoting' class='button OverallVoting' type='button' onclick="OverallBothVoting()">Both</button>
									</div>
									<br>
									<div id='delegateButtons' style="display:none">
										<div id='votingDelegates'>Delegates currently voting for:<br><br></div>
										<div id='nonvotingDelegates'><br>Delegates currently NOT voting for:<br><br></div><br>
									</div>
								<table id='delegateTable' class="table-responsive"></table>
								<div id='overallTableDiv'>Most Recent 20 Transactions:<br>
								<table id='overallTable' class="table-responsive"></table>
							</div>
							
						</div>
						<form target="myiframe" action="https://www.lisk.support">
							<div id="LSKAddress"><p>Please enter your Lisk Address</p>
								<input type="text" id="LSKAddressBox" autocomplete="on" class="form-control-custom"/></div><br />
							<button class="btn btn-md btn-primary" id="startButton" type="submit" onclick="connectnow()">Submit</button>
							<br>
							<p id='Main'></p>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<?php get_footer();