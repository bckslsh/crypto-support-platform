<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<section class="pb-5 alt-background">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-8 col-xl-9">
				<div class="row news-list">
					<div class="col-12">
						<div class="card super-card card-raised mt-n5 mb-4">
							<div class="card-body px-md-5 pt-md-5 pb-md-3">
								<h1 class="primary">
									<?php printf( __( 'Search results', 'twentyseventeen' )); ?>
								</h1>
								<?php if ( have_posts() ) : ?>
									<h2 class="h5 text-muted">
										<?php _e('Results for <strong>' . get_search_query() . '</strong>', 'twentyseventeen'); ?>
									</h2>
								<?php else : ?>
									<h2 class="h5 text-muted">
										<?php _e( 'Nothing Found', 'twentyseventeen' ); ?>
									</h2>
								<?php endif; ?>

								<?php
									get_search_form();
								?>
							</div>
						</div>
					</div>

					<?php
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $query = new WP_Query( array(
                            'posts_per_page' => 8,
                            'paged' => $paged
                        ) );
                    ?>	

					<?php
						if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						// get_template_part( 'template-parts/post/content', 'excerpt' );
					?>

					<div class="col-12 col-md-6 my-3">
						<div class="card h-100 card-raised">
						<div class="card-img-top" style="background-image: url('<?php the_post_thumbnail_url('large') ?>');"></div>
							<div class="card-body px-5 pt-5 pb-3">
								<div class="card-subtitle mb-2">
									<span class="text-primary"><?php the_category( ', ' ); ?></span>
									<small class="text-muted">
										<?php echo get_the_date(); ?>
									</small>
								</div>
								<div class="card-author text-muted">
									<i class="icon ion-ios-person-outline"></i> <?php the_author(); ?>
								</div>
								<h3 class="card-title my-4">
									<a href="<?php the_permalink() ?>">
										<?php the_title() ?>
									</a>
								</h3>
								<p class="card-text">
									<?php the_excerpt(); ?>
								</p>
							</div>
							<div class="card-footer px-5 pb-5 bg-white border-0">
								<a href="<?php the_permalink() ?>">
									continue to read
									<i class="icon ion-arrow-right-c"></i>
								</a>
							</div>
						</div>
					</div>

					<?php
					endwhile; // End of the loop.
					?>



					<div class="col-12 my-3">
						<div class="pagination">
							<?php
									$pages = paginate_links( array(
									'show_all'     => false,
									'type'         => 'plain',
									'end_size'     => 2,
									'mid_size'     => 1,
									'prev_next'    => true,
									'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer Posts', 'text-domain' ) ),
									'next_text'    => sprintf( '%1$s <i></i>', __( 'Older Posts', 'text-domain' ) ),
									'add_args'     => false,
									'add_fragment' => '',
								) );
								echo($pages);
							?>
						</div>
					</div>
					
					<?php
						else : ?>

					<div class="col-12 mb-4">
						<div class="card card-raised">
							<div class="card-body">
								<p class="text-muted lead text-center my-3">
									<?php
										_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyseventeen' );
									?>
								</p>
							</div>
						</div>
					</div>

					
					<?php
					endif;
					?>
				</div>
			</div>
			<div class="col-12 col-lg-4 col-xl-3 mt-lg-5">
				<div class="card card-raised">
					<div class="card-header bg-white border-0">
						<h6 class="text-uppercase primary mt-2">
							<i class="ion-ios-paper-outline pr-2"></i>More posts
						</h6>
					</div>
					<div class="card-body">
						<ul class="list-unstyled">

							<!-- // Define our WP Query Parameters -->
							<?php $the_query = new WP_Query( 'posts_per_page=10' ); ?>

							<!-- // Start our WP Query -->
							<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

							<li class="mb-3">
								<a href="<?php the_permalink() ?>">
										<?php the_title() ?>
								</a>
								<div class="text-muted">
									<small>
										<?php echo get_the_date(); ?>
									</small>
								</div>
							</li>

							<?php
								endwhile;
								wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>       
	</div>
</section>

<?php get_footer();
