<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="form mt-4" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="form-group">
		<label for="<?php echo $unique_id; ?>" class="sr-only">
			<?php echo _x( 'Search for:', 'label', 'twentyseventeen' ); ?>
		</label>
		<div class="input-group input-group-lg bloodhound">
			<input type="search" id="<?php echo $unique_id; ?>" class="typeahead form-control-custom" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'twentyseventeen' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		</div>
	</div>
</form>
