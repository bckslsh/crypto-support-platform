<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(singlepost); ?>
<div class="container single-head mt-n5 has-feat-img">
	<main id="main" class="site-main" role="main">
		<div class="row justify-content-center">
			<div class="col-sm-12 col-lg-10">
				<?php
				/* Start the Loop */
	
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					the_post_thumbnail('full', ['class' => 'img-fluid header-img']);
				}
				?>
			</div>
		</div>
	    <div class="row justify-content-center">
	        <div class="col-md-10 col-lg-8">	
			<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

			
					get_template_part( 'template-parts/post/content', get_post_format() );
			?>
	        </div>

	    </div>
		<?php
		// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			endwhile; // End of the loop.
		?>
    </main><!-- #main -->
</div>
<?php get_footer();
