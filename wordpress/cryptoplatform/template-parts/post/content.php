<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php
			if ( is_sticky() && is_home() ) :
				echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
			endif;
			?>
			<header class="entry-header mt-5">
				<?php
				if ( is_single() ) {
					the_title( '<h2 class="entry-title text-center">', '</h2>' );
				} elseif ( is_front_page() && is_home() ) {
					the_title( '<h3 class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
				} else {
					the_title( '<h2 class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
				?>
			</header><!-- .entry-header -->
		
			<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
				<div class="post-thumbnail">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
					</a>
				</div><!-- .post-thumbnail -->
			
			
			<?php endif; ?>
			
			<div id="floating_social_bar">
				<div class="ssk-sticky ssk-left ssk-float ssk-round" data-url="<?php $current_url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
                    <a href="//www.reddit.com/submit" onclick="window.location = '//www.reddit.com/submit?url=' + encodeURIComponent(window.location); return false" class="ssk ssk-reddit"></a>		
                    <a href="" class="ssk ssk-facebook mt-3"></a>
                    <a href="" class="ssk ssk-twitter mt-3"></a>
                    <a href="" class="ssk ssk-google-plus mt-3"></a>
                    <a href="" class="ssk ssk-email mt-3"></a>
                </div>				
			</div>			
			
				<div class="lead text-center mb-5">
					By <strong><?php the_author_posts_link(); ?></strong> on <?php the_time('F jS, Y'); ?> in <strong><?php the_category(', '); ?></strong> <?php edit_post_link(__('{Edit}'), ''); ?>
				</div>
				<div class="ssk-group ssk-round text-center mb-5" data-url="<?php $current_url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>">
                    <a href="//www.reddit.com/submit" onclick="window.location = '//www.reddit.com/submit?url=' + encodeURIComponent(window.location); return false" class="ssk ssk-reddit mr-3"></a>		
                    <a href="" class="ssk ssk-facebook mr-3"></a>
                    <a href="" class="ssk ssk-twitter mr-3"></a>
                    <a href="" class="ssk ssk-google-plus mr-3"></a>
                    <a href="" class="ssk ssk-email mr-3"></a>
                </div>
				<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
					get_the_title()
				) );
		
				wp_link_pages( array(
					'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				) );
				?>
			</div><!-- .entry-content -->
	
			<?php
			if ( is_single() ) {
				twentyseventeen_entry_footer();
			}
			?>
		</article><!-- #post-## -->


